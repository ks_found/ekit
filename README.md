# EKit

## First import of an HTML editor

This repository contains the EKit HTML editor (https://sourceforge.net/projects/ekit/) with several changes:

1. Can be compiled on JDK17 
2. Includes JEX equation  editor
3. A spell checker (need to be fixed) 

This editor runs on Unix, Mac OS X, Windows.

## Features

1. Cut, Copy, & Paste
2. Bold, Italic, Underline
3. Superscript & Subscript
4. Text Styles & Colors
5. Anchor Insertion
6. Table Editing, table and cell editing 
7. Form Support
8. Image Insertion
9. Find & Replace
10. CSS Support
11. Graphical Toolbar
12. Source Edit Window
13. Internationalization support & many language files
14. Spell checking via the Jazzy open source spellchecker
15. Font selector dialog lets you use and view system fonts in your HTML page
16. Word processor style bullet lists
17. Better table handling and new table row and column menus
18. Load and save documents in web-safe Base64 encoding
19. Multiple toolbars
20. Unicode character insertion tools supports dingbats, math symbols, extended alphabets, and more
21. Paragraph or Break line terminators


To be added:

ZWI export (with signature)


Work in progress

S.Chekanov

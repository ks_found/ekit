#!/bin/bash

# set here home directory where the jehep.jar file is located 
JEHEP_HOME=`pwd`


################## do not edit ###############################
JAVA_HEAP_SIZE=256 
CLASSPATH=$JEHEP_HOME"/ekit.jar":$CLASSPATH


# Add in your .jar files first
for i in $JEHEP_HOME/lib/*.jar
do
      CLASSPATH=$CLASSPATH:"$i"
done

 # Add in your .jar files first
for i in $JEHEP_HOME/lib/*/*.jar
do
      CLASSPATH=$CLASSPATH:"$i"
done


# convert the unix path to windows
if [ "$OSTYPE" = "cygwin32" ] || [ "$OSTYPE" = "cygwin" ] ; then
   CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
fi


if [ -z $JAVA_HOME ]; then
        java -mx${JAVA_HEAP_SIZE}m -cp $CLASSPATH -Djehep.home=$JEHEP_HOME com.hexidec.ekit.Ekit  $1 &
else
        $JAVA_HOME/bin/java -mx${JAVA_HEAP_SIZE}m -cp $CLASSPATH -Djehep.home=$JEHEP_HOME com.hexidec.ekit.Ekit $1 &
fi

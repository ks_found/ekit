package jex;

import java.awt.*;
import java.util.*;

import jex.Jexmath;

public class Jexmml{

        static private Hashtable mmlDic;
        static private Hashtable dicMML;
        static private Hashtable tokens;
        static private Hashtable pairs;
        static private Hashtable h;

	static public Integer mmlMacro(String x){
            return (Integer) mmlDic.get(x);
        }
        
        static public String tokenReplace(String x){
            return (String) dicMML.get(x);
        }
        
        static public String tokenType(String x){
            return (String) tokens.get(x);
        }
        
        static public String getLeft(String x){
            return (String) pairs.get(x);
        }
        
        static public Integer getType(String x){
            return (Integer) h.get(x);
        }
        
        static public void init(){
        
        tokens = new Hashtable();
      
        tokens.put("mover", "OverBox");
        tokens.put("munder", "UnderBox");
	tokens.put("msubsup", "SubSupBox");
	tokens.put("mrow", "{");
	tokens.put("mfracext", "DivisionBox");
	tokens.put("mfrac", "DivisionBox");
	tokens.put("mspace", "mspace");

        //tokens.put("mroot", "RootBox");
	//tokens.put("msub", "???");
	//tokens.put("msup", "???"); 


	tokens.put("/mtr", "\\\\");
	tokens.put("/mtd", "&");
	tokens.put("/mrow", "}");
	tokens.put("/mo", "}");
	tokens.put("/mtable", "\\end");
        
        pairs = new Hashtable();
        
        pairs.put("}", "{");
        pairs.put("\\rightgrp", "\\leftgrp");
        pairs.put("]", "[");
        pairs.put("&", "array");
        pairs.put("\\\\", "array");
        pairs.put("\\end", "array");
        
        h = new Hashtable();
        
        h.put("&", new Integer(1));
        h.put("<", new Integer(1));
	h.put(" ", new Integer(0));
	h.put("\r", new Integer(0));
	h.put("\n", new Integer(0));
	h.put("\t", new Integer(0));
        
        mmlDic = new Hashtable();
        dicMML = new Hashtable();
        
 
        mmlDic.put("lt", new Integer('<'));
        mmlDic.put("gt", new Integer('>'));
       
               
        for(Enumeration e = mmlDic.keys(); e.hasMoreElements();){
            Object ob = e.nextElement();
            dicMML.put(mmlDic.get(ob), ob);
        }

       
    }
}

class MMLParser{

    MMLTokenParser tp;
    Stack stack;
    
    public MMLParser(String x){
        tp = new MMLTokenParser(x);
        stack = new Stack();
    }
    
    private void pushClosed(MathBox mb){
        if(stack.isEmpty()) return;
        MathBox sb = (MathBox) stack.pop();
        if(sb.isClosed()){
            MathBox sb2 = (MathBox) stack.pop();
            sb2 = sb2.build("add", sb);
            stack.push(sb2);
            stack.push(mb);
            //System.out.println("add to close: " + sb.toMML());
        }
        else{
            if(sb.isRow() & (!sb.isMathrm())){
                stack.push(sb);
                stack.push(mb);
                //System.out.println("open row push push");
            }
            else{
                sb = sb.build("add", mb);
                if(sb.isClosed()) pushClosed(sb);
                else stack.push(sb);
                //System.out.println("add: " + mb.toMML());
            }
        }
    }

    private boolean peekFence(){
	if(stack.isEmpty()) return false;
	MathBox sb0 = (MathBox) stack.pop();
	if(stack.isEmpty() || !(sb0.isRow() && sb0.isClosed())){
		stack.push(sb0);
		return false;
	}
	MathBox sb = (MathBox) stack.pop();
	boolean answer = false;
	if(sb.isRow() && !sb.isClosed() && (sb.buildState.equals("\\leftgrp"))) answer = true;
	stack.push(sb);
	stack.push(sb0);
	return answer;
    }
    
    private void pushCloser(String z){
	if(stack.isEmpty()) return;
        MathBox sb = (MathBox) stack.pop();
        if(sb.isClosed()){
            MathBox sb2 = (MathBox) stack.pop();
            sb = sb2.build("add", sb);
        }
        sb = sb.build(z, null);
        if(sb.isClosed()) pushClosed(sb);
        else stack.push(sb);
    }
      
    public RowBox parse(){
        RowBox rb = new RowBox();
        rb.build("open", null);
        stack.push(rb);
        
        String z;
        boolean iter = true;
        while(iter){
            z = tp.nextToken();
            if(z != "") {
		String b = z.substring(0,1);
		z = z.substring(1);
		char zz = 'a';
		boolean isChar = false;
		if(z.length() == 0) {
			zz = b.charAt(0);
			isChar = true;
		}
		if(b.equals("&")) {
			isChar = true;
			b = z.substring(0,1);
			if(b.equals("#")) zz = (char) Integer.parseInt(z.substring(1), 10);
			else {
				Integer entity = Jexmml.mmlMacro(z);
				if(entity == null) isChar = false;
				else zz = (char) entity.intValue();
			}
		}
		else if(b.equals("<")) {
			tp.getProps(z);
			//System.out.println(tp.tag);
			String ttype = Jexmml.tokenType(tp.tag);
			boolean fence = false;
			for(int ii = 0; ii < tp.nprops; ii++){
				//System.out.println(tp.names[ii] + "=" + tp.props[ii]);
				if(tp.names[ii].equals("fence") && tp.props[ii].equals("true")) fence = true;
			}
                        MathBox mb = null;
			if(tp.tag.equals("mtable")) {
				mb = new MatrixBox();
                        	mb.build("mtable", null);
				String rowalign = "bottom";
				String colalign = "";
				ttype = "array";
				for(int ii = 0; ii < tp.nprops; ii++){
					if(tp.names[ii].equals("rowalign")) rowalign = tp.props[ii];
					if(tp.names[ii].equals("colalign")) colalign = tp.props[ii];
					if(tp.names[ii].equals("columnspacing") && tp.props[ii].equals("0.0em")) ttype = "align";
				}
                        	mb.build(ttype, null); //need to fix this to check characteristics for align
                        	mb.build(tp.getAlign(rowalign), null);
                        	stack.push(mb.build(tp.getAlign(colalign), null));
			}
			else if(tp.tag.equals("mspace")){
				String width = null;
				for(int ii = 0; ii < tp.nprops; ii++){
					if(tp.names[ii].equals("width")) width = tp.props[ii];
				}
				if(width != null){
					ttype = "\\:";
					if(width.equals("0.5em")) ttype = "\\,";
					if(width.equals("1.5em")) ttype = "\\;";
					mb = rb.wrapBox(new SpaceBox(ttype));
                                	mb = mb.build("close", null);
                                	pushClosed(mb);
				}
			}
			else {
				boolean test = false;
				if(fence) test = peekFence();
				if(fence && !test) {
					mb = new FenceBox();
					mb.setLang("mml");
					mb.build("\\leftgrp", null);
					stack.push(mb);
				}
				if(fence && test) pushCloser("\\rightgrp");
                        	if(ttype != null && ttype.equals("OverBox")) mb = new OverBox();
                        	if(ttype != null && ttype.equals("UnderBox")) mb = new UnderBox();
				if(ttype != null && ttype.equals("SubSupBox")) mb = new SubSupBox();
				if(ttype != null && ttype.equals("DivisionBox")) mb = new DivisionBox();
                        	//if(ttype != null && ttype.equals("RootBox")) mb = new RootBox();
				if(tp.tag.equals("mfrac")) {
					DivisionBox db = (DivisionBox) mb;
					db.isExt = false;
				}
                        	if(tp.tag.equals("mrow")) mb = new RowBox();
				if(tp.tag.equals("mo")){
					mb = new RowBox();
					if(!fence) {
						RowBox rt = (RowBox) mb;
						rt.bMathrm = true;
					}
					ttype = "{";
				}
                        	if(mb != null) {
					mb.setLang("mml");
                            		mb = mb.build(ttype, null);
                            		stack.push(mb);
                        	}
				else {
					if((ttype != null) && tp.tag.substring(0,1).equals("/")) pushCloser(ttype);
				}
			}
			if(tp.tag.equals("mtr") || tp.tag.equals("mtd")) pushCloser(tp.tag);
		}
		if(isChar){
                    MathBox rn = rb.wrapBox(new CharBox(zz));
                    rn = rn.build("close", null);
                    pushClosed(rn);
		}
		

            }
            else {
                pushClosed(new EmptyBox());
                iter = false;
            }
        }
        rb.validate();
        return rb;
    }
}

class MMLTokenParser{
	int pos = 0;
	String x;
	public String tag;
	public String names[] = new String[100];
	public String props[] = new String[100];
	public int nprops = 0;
	public MMLTokenParser(String x){
		this.x = x;
	}

	public String getAlign(String a){
		a = a.trim();
		String b = "";
		int pos;
		do {
			pos = a.indexOf(' ');
			b = b + a.substring(0,1);
			if(pos > 0) a = a.substring(pos).trim();
			else a = "";
			pos = a.length();
		}
		while (pos > 0);
		return b;
	}
        
        public void getProps(String a){
	    a = a.replaceAll("math:", "");
	    int pos = a.indexOf(' ');
	    nprops = 0;
	    if(pos < 0){
		tag = a.trim();
		return;
	    }
	    tag = a.substring(0, pos);
	    while(pos > 0){
		a = a.substring(pos + 1);
		pos = a.indexOf('=');
		if(pos > 0){
			names[nprops] = a.substring(0, pos).trim();
			pos = a.indexOf('"');
			if(pos > 0){
				a = a.substring(pos + 1);
				pos = a.indexOf('"');
				if(pos > 0){
					props[nprops] = a.substring(0, pos).trim();
					a = a.substring(pos);
					pos = a.indexOf(' ');
				}
			}
			nprops++;
		}
	    } 
        }
        


	public String nextToken(){
		int state = 0;
		String a = "";
		String b = "";
		while(state == 0){
                        if(pos == x.length()){			
                            x = "";
                            pos = 0;
                            return "";
                        }
			String y = x.substring(pos, pos + 1);
			Integer n = Jexmml.getType(y);
			if(n == null){
				pos++;
				a = y;
				state = 1;
			}
			else {
				int nn = n.intValue();
				if (nn == 1) {
					String delim = ";";
					if(y.equals("<")) delim = ">";
					int m = x.indexOf(delim);
					if(m <= 0) pos++;
					else{
						b = x.substring(pos, pos + 1);
						a = x.substring(pos + 1, m);
						a = b + a.trim();
						pos = m + 1;
						state = 1;
					}
				}
				else pos++;
			}
		}
                x = x.substring(pos);
                pos = 0;
		return a;
	}
}

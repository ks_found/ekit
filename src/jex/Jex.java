package jex;

import javax.swing.*;
import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.util.*;
import java.lang.Math.*;
import java.lang.Number.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.*;


import jex.Jextex;
import jex.Jexmml;
import jex.Jexkey;
import jex.Jexmath;
import jex.Jexfont;
import jex.Jexconf;
import jex.Jexwmf;
import jex.Jexmt;

// import Ekit
import com.hexidec.ekit.EkitCore;

interface Office {
	public abstract void grabEq();
	public abstract boolean queryExit();
	public abstract void setJex(Jex jex);
	public abstract void installOffice();
	public abstract void installFonts();
	public abstract void installJavaFonts();
	public abstract void convertToTex();
	public abstract void convertFromTex();
	public abstract void updateEquations(DefaultContext d, boolean embedGraphics);
}

public class Jex extends JFrame{

    static Dimension frameSize = new Dimension(500, 300);
    static Dimension canvasSize = new Dimension(1200, 1600);

    private static int countEQ=0; 
    static ImageIcon editcopy = null;
    static ImageIcon editcut;
    static ImageIcon editpaste;
    static ImageIcon fileopen;
    static ImageIcon filesave;
    static ImageIcon preview;
    static ImageIcon redo;
    static ImageIcon undo;
    static ImageIcon viewmagfit;
    static ImageIcon viewmagminus;
    static ImageIcon viewmagplus;
 
    //hack to show what the 256 font glyphs look like
    static boolean showFont = false;
    static String fontName = "Euclid Symbol";
    //static Dimension canvasSize = new Dimension(1200, 12000);

    public static int nWins = 0;
    public static Office officeclass;
    public static boolean immediateShutdown = false;
    
    public  JexPane jexPane;
    JLabel statusBar;
    public JFrame gJexFrame;
    JexDialog activeD = null;
    Toolkit toolkit;
    String initEqn = "{}";
    JButton previewButton;
    Office office;
    private static EkitCore mEkitCore; 


	
    public static void main(String[] args) {
        initJ(args);
    }
  
 
    public static void startjex(final EkitCore m) {
        mEkitCore=m;
        Jexconf.init();
        Jextex.init();
        Jexmml.init();
        Jexfont.init();
        Jexkey.init();
        JFrame jexFrame = new JFrame("Jex");
        final Jex controller = new Jex();
        controller.initEqn = "{}";
        JexPane jp = controller.buildUI(jexFrame);
        jexFrame.setSize(frameSize);
        jp.setPreferredSize(canvasSize);
        jexFrame.setVisible(true);
        jp.requestFocus();
        // return newJex(Jexconf.getProp("default"));
    }




     
    public static Jex initJ(String[] args) {
	Jexconf.init();
        Jextex.init();
	Jexmml.init();
        Jexfont.init();
        Jexkey.init();
	if( args.length > 0) {
		for(int ii = 0; ii < args.length; ii++){
			if(!args[ii].substring(0,1).equals("-")){
				Jexconf.setFile(new File(args[ii])); 
				Jexconf.readConf(); 
			}
		}
	}     
        return newJex(Jexconf.getProp("default"));
    }

    public void checkClose(){
	closeThread ct = new closeThread();
	ct.start();
    }
    
    public static Jex newJex(String eqn){
        JFrame jexFrame = new JFrame("Jex");
        final Jex controller = new Jex();
        controller.initEqn = eqn;
        JexPane jp = controller.buildUI(jexFrame);
        jexFrame.setSize(frameSize);  
        jp.setPreferredSize(canvasSize);
        jexFrame.setVisible(true);
        jp.requestFocus();
	nWins++;
        return controller;
    }
    
    public void setMathDisplay(MathDisplay z){
        jexPane.d.md = z;
    }

    public void setOffice(Office office){
	this.office = office;
    }
    
    public JexPane buildUI(JFrame jf){
	jf.addWindowListener(new JexClosing());	
	jf.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        buildMenu(jf);
        gJexFrame = jf;
	toolkit = jf.getToolkit();
        return buildContainer(jf.getContentPane());
    }

    public void setTitle(String t){
	gJexFrame.setTitle("Jex - " + t);
    }
    
//UI Definition    
    
    public JMenuItem bmi(String action){
        JMenuItem item;
        item = new JMenuItem(action);
        item.addActionListener(new JexActionListener(action));
        return item;
    }
	
    public JButton bjb(String action){
        JButton item;
        item = new JButton(action);
        item.addActionListener(new JexActionListener(action, item));
        return item;
    }

    public JButton bjb(String action, Icon icon){
        JButton item;
        item = new JButton(icon);
        item.addActionListener(new JexActionListener(action, item));
	item.setToolTipText(action);
        return item;
    }
	
    public void buildMenu(JFrame jf){			
        JMenuBar jmb = new JMenuBar();
        JMenu file = new JMenu("File");
        //file.add(bmi("Grab"));
        file.add(bmi("New"));
        //file.add(bmi("Open"));
        //file.addSeparator();
        //file.add(bmi("Close"));
        //file.addSeparator();
        file.add(bmi("Save"));

	file.add(bmi("Save Configuration As"));

	JMenu export = new JMenu("Export");
        export.add(bmi("Ekit"));
        export.add(bmi("png"));
	export.add(bmi("jpg"));
	export.add(bmi("wmf"));
	export.add(bmi("txt"));
	export.add(bmi("mml"));

	JMenu docs = new JMenu("Entire Document");
// 	docs.add(bmi("Export TeX"));
	docs.add(bmi("Import TeX"));
//	docs.add(bmi("Update Equations"));
//	docs.add(bmi("Embed All Graphics"));

	file.add(export);
	file.add(docs);

	file.addSeparator();
        file.add(bmi("Exit"));
    	jmb.add(file);
        
        JMenu edit = new JMenu("Edit");
        edit.add(bmi("Undo"));
        edit.add(bmi("Redo"));
        edit.addSeparator();
        edit.add(bmi("Cut"));
        edit.add(bmi("Copy"));
	edit.add(bmi("Copy mml"));
        edit.addSeparator();
        edit.add(bmi("Paste"));
	edit.addSeparator();
	edit.add(bmi("Clear"));
	edit.add(bmi("Select All"));
        jmb.add(edit);
        
        JMenu view = new JMenu("View");
        view.add(bmi("Zoom Normal"));
        view.addSeparator();
        view.add(bmi("Zoom In"));
        view.add(bmi("Zoom Out"));
	view.addSeparator();
	view.add(bmi("Highlight Levels"));
	//view.addSeparator();
	//view.add(bmi("Set As Default"));
        jmb.add(view);
        
        JMenu insert = new JMenu("Insert");
        
        JMenu space = new JMenu("Space");
        space.add(bmi("Thick"));
        space.add(bmi("Medium"));
        space.add(bmi("Thin"));
        insert.add(space);
        
        insert.addSeparator();
        
        insert.add(bmi("Fence"));
        insert.add(bmi("Layout"));
        insert.add(bmi("Matrix"));
        insert.add(bmi("Symbols"));
        insert.add(bmi("greek"));
        insert.add(bmi("Greek"));
        insert.add(bmi("User"));
	insert.addSeparator();
	insert.add(bmi("Toggle Math/Text"));


        jmb.add(insert);
        
        //JMenu size = new JMenu("Size");
        //jmb.add(size);
        
        JMenu tools = new JMenu("Tools");

	JMenu install = new JMenu("Install");
	//install.add(bmi("Install All"));
	// install.add(bmi("Install Office Fonts"));
	install.add(bmi("Install Java Fonts"));
	// install.add(bmi("Install for Office"));
	// install.add(bmi("Immediate Shutdown"));
	tools.add(install);
	tools.addSeparator();
	tools.add(bmi("test1"));
	tools.add(bmi("test2"));
	//tools.add(bmi("test import mt"));
	//tools.add(bmi("testmml"));
        jmb.add(tools);
        
        JMenu matrix = new JMenu("Matrix");
        matrix.add(bmi("Matrix"));
        jmb.add(matrix);
        matrix.addSeparator();
        matrix.add(bmi("Add Row Above"));
        matrix.add(bmi("Add Row Below"));
        matrix.addSeparator();
        matrix.add(bmi("Add Column Left"));
        matrix.add(bmi("Add Column Right"));
        matrix.addSeparator();
        matrix.add(bmi("Delete Row"));
        matrix.add(bmi("Delete Column"));
        
        JMenu help = new JMenu("Help");
	//help.add(bmi("Overview"));
	help.add(bmi("Readme"));
	//help.add(bmi("Supported Tex"));
	help.add(bmi("About"));

        jmb.add(help);
        
        jf.setJMenuBar(jmb);
    }
	
    private void loadIcons(){
	if(editcopy != null) return;
	editcopy = new ImageIcon(Jex.class.getResource("images/editcopy.png"));
	editcut = new ImageIcon(Jex.class.getResource("images/editcut.png"));
	editpaste = new ImageIcon(Jex.class.getResource("images/editpaste.png"));
	fileopen = new ImageIcon(Jex.class.getResource("images/fileopen.png"));
	filesave = new ImageIcon(Jex.class.getResource("images/filesave.png"));
	preview = new ImageIcon(Jex.class.getResource("images/preview.png"));
	redo = new ImageIcon(Jex.class.getResource("images/redo.png"));
	undo = new ImageIcon(Jex.class.getResource("images/undo.png"));
	viewmagfit = new ImageIcon(Jex.class.getResource("images/viewmagfit.png"));
	viewmagminus = new ImageIcon(Jex.class.getResource("images/viewmagminus.png"));
	viewmagplus = new ImageIcon(Jex.class.getResource("images/viewmagplus.png"));
    }
	
    public JexPane buildContainer(Container container){
    
        JPanel x = new JPanel(new BorderLayout());
        container.add(x, BorderLayout.NORTH);

	loadIcons();
	
        JToolBar jtb = new JToolBar();
        //jtb.add(bjb("Grab", fileopen));
        jtb.add(bjb("Save", filesave));
        jtb.add(bjb("Cut", editcut));
        jtb.add(bjb("Copy", editcopy));
        jtb.add(bjb("Paste", editpaste));
    	jtb.add(bjb("Undo", undo));	
        jtb.add(bjb("Redo", redo));
        jtb.add(bjb("Zoom Out", viewmagminus));
        jtb.add(bjb("Zoom Normal", viewmagfit));
        jtb.add(bjb("Zoom In", viewmagplus));
	previewButton = bjb("Preview", preview);
        jtb.add(previewButton);
	previewButton.setText("Preview");
	previewButton.setToolTipText("");
        x.add(jtb, BorderLayout.NORTH);
        
        jtb = new JToolBar();        
        jtb.add(bjb("Fence"));
        jtb.add(bjb("Layout"));
        jtb.add(bjb("Matrix"));
        jtb.add(bjb("Symbols"));
        jtb.add(bjb("greek"));
        jtb.add(bjb("Greek"));
        jtb.add(bjb("User"));
        container.add(jtb, BorderLayout.WEST);
        x.add(jtb, BorderLayout.SOUTH);
		
        jexPane = new JexPane();
        jexPane.addMouseListener(new JexMouseListener());
        jexPane.addMouseMotionListener(new JexMouseMotionListener());
        jexPane.addKeyListener(new Jexkey(jexPane));
        jexPane.setup();
		
        JScrollPane sp = new JScrollPane(jexPane);
        JScrollBar sb = sp.getHorizontalScrollBar();
        JScrollBar sv = sp.getVerticalScrollBar();
        sv.setUnitIncrement(10);
        sb.setUnitIncrement(100);
        sp.setInputMap(javax.swing.JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, null);
        container.add(sp, BorderLayout.CENTER);
		
        statusBar = new JLabel("Edit");
        container.add(statusBar, BorderLayout.SOUTH);

	jexPane.setStatus();
		
        return jexPane;
    }

    public void updateBinding(String printBinding, String editBinding){
		if(printBinding != null) Jexfont.printBinding = printBinding;
		if(editBinding != null) {
			Jexfont.editBinding = editBinding;
			jexPane.d.updateBinding();
		}
    }

    private void matrixDialog(Point p, Rectangle r){

	    if((Matrix != null) && Matrix.jd.isVisible()) {
			Matrix.jd.setVisible(false); 
			closeActive();
			return;
	    }
	    JexDialog jw = new JexDialog(gJexFrame, "New Matrix");
	    Matrix = jw;
            jw.jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            GridLayout gl = new GridLayout(5,2);
            Container jwc = jw.jd.getContentPane();
            jwc.setLayout(gl);
            jwc.add(new JLabel("Rows"));
	    JTextField rows = new JTextField("2");
            jwc.add(rows);
            jwc.add(new JLabel("Row Alignment - cb"));
	    JTextField rowa = new JTextField("b");
            jwc.add(rowa);
            jwc.add(new JLabel("Cols"));
	    JTextField cols = new JTextField("3");
            jwc.add(cols);
            jwc.add(new JLabel("Col Alignment - lcr"));
	    JTextField cola = new JTextField("lll");
            jwc.add(cola);
	    JButton ok = new JButton("OK");
	    jwc.add(ok);
	    ok.addActionListener(new JexMatrixButtonListener("ok", rows, rowa, cols, cola, jw));
	    JButton cancel = new JButton("Cancel");
	    cancel.addActionListener(new JexMatrixButtonListener("cancel", rows, rowa, cols, cola, jw));
	    jwc.add(cancel);
            if(p != null){
                int h = r.height;
                int w = r.width;
                p.y += h;
                jw.jd.setLocation(p);
            }
            jw.jd.setSize(new Dimension(300, 300));
            jw.jd.setVisible(true);
	    closeActive();
	    activeD = jw;

    }

    private JexDialog Matrix = null;
    private JexDialog Layout = null;
    private JexDialog greek = null;
    private JexDialog Greek = null;
    private JexDialog Symbol = null;
    private JexDialog User = null;
   
    private void makeDialog(Point p, Rectangle r, String label, String tex){

	    JexDialog jw = null; 

	    if(label.equals("Layout")) jw = Layout;
	    if(label.equals("greek")) jw = greek;
	    if(label.equals("Greek")) jw = Greek;
	    if(label.equals("Symbol")) jw = Symbol;
	    if(label.equals("User")) jw = User;

	    if(jw != null) {
		if(jw.jd.isVisible()) {
			jw.jd.setVisible(false); 
			closeActive();
			return;
		}
		else if(!label.equals("User")){
			jw.jdbl.clearMe();
			jw.jmml.clearMe();
			jw.jd.setVisible(true);
			closeActive();
			activeD = jw; 
			return;
		}
	    }

	    closeActive();

	    int bPad = 10;

            jw = new JexDialog(gJexFrame, label);

	    if(label.equals("Layout")) Layout = jw;
	    if(label.equals("greek")) greek = jw;
	    if(label.equals("Greek")) Greek = jw;
	    if(label.equals("Symbol")) Symbol = jw;
	    if(label.equals("User")) User = jw;
	    
            jw.jd.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            JexPane jp = new JexPane();
            jp.jd = jw;
	    jw.jp = jp;
	    Container jwc = jw.jd.getContentPane();
	    jwc.add(jp, BorderLayout.CENTER);

            DefaultContext d = new DefaultContext();
            jp.d = d;
            d.x  = bPad;
            d.y = bPad;
            d.size = 30;
            d.sel = new Selection();
            d.sel.add(new Integer(1));

	    jw.jb = new JButton("Click to Leave Open");
	    jw.jmml = new JexMenuMouseListener(true, jw);
	    jw.jp.addMouseListener(jw.jmml);
	    if(label.equals("User")) jw.jmml.setUser();
	    jw.jdbl = new JexDialogButtonListener("opener", jw.jb, jw.jmml, jw);
	    jw.jb.addActionListener(jw.jdbl);
	    jw.jb.setText("Leave Open");
	    JButton jb = new JButton("Close");
	    jb.addActionListener(new JexDialogButtonListener("closer", jw.jb, jw.jmml, jw));
	    JPanel jbp = new JPanel(new FlowLayout());
	    jbp.add(jw.jb);
            jbp.add(jb);
	    jwc.add(jbp, BorderLayout.SOUTH);
	    //jwc.add(jw.jb, BorderLayout.SOUTH);
            
            if(p != null){
                int h = r.height;
                int w = r.width;
                p.y += h;
                jw.jd.setLocation(p);
            }
            
            d.copyonly = true;
            d.setMenu();
	    RowBox rb = (RowBox) MathBox.getTex(tex);
            d.setEqn(rb);
	    d.set();
	    d.makeMetrics();
	    BoxMetrics m = d.getEqn().boxMetrics;
	    int xx = m.width;
	    int yy = m.height;
            jw = jp.jd;
            jw.jd.setSize(new Dimension(xx, yy));
	    jw.jd.pack();	
	    Insets ins = jw.jd.getInsets();
	    yy = yy + ins.top + ins.bottom + jw.jb.getHeight() + 2*bPad;
	    xx = xx + ins.right + ins.left + 2*bPad;
            jw.jd.setSize(new Dimension(xx, yy));
            jw.jd.setVisible(true);
	    activeD = jw;
    }

    private class JexDialog{
	public JexPane jp;
	public JDialog jd;
	public JButton jb;
	public JexMenuMouseListener jmml;
	JexDialogButtonListener jdbl;
	
	public JexDialog(Frame owner, String title){
		jd = new JDialog(owner, title);
	}
    }

    private class JexMatrixButtonListener implements ActionListener {
	private JTextField rows;
	private JTextField rowa;
	private JTextField cols;
	private JTextField cola;
	private JexDialog jw;
	private String action;

	public JexMatrixButtonListener(String action, JTextField rows, JTextField rowa, JTextField cols, JTextField cola, JexDialog jw){
		this.rows = rows;
		this.rowa = rowa;
		this.cols = cols;
		this.cola = cola;
		this.jw = jw;
		this.action = action;
	}
	public void actionPerformed(ActionEvent e){
		if(action.equals("cancel")) jw.jd.setVisible(false);
		if(action.equals("ok")){
			int nrows = Integer.parseInt(rows.getText());
			int ncols = Integer.parseInt(cols.getText());
			MatrixBox matrix = new MatrixBox(nrows, ncols);
			matrix.setHalign(cola.getText());
			matrix.valign = rowa.getText();
			jexPane.d.insertBox(matrix);
			jw.jd.setVisible(false);
            		jexPane.mathStyle = "null";
            		jexPane.repaint();
            		jexPane.requestFocus();
		}
	}
    }

    private class JexClosing extends WindowAdapter {

	public void windowClosing(WindowEvent e){
		if(jexPane.winDone()) {
			Window win = e.getWindow();
			win.setVisible(false); 
			checkClose();
		}
	}
    }

    private class JexDialogButtonListener implements ActionListener {
	private String action;
	private JButton button;
	JexMenuMouseListener jmml;
	boolean closeMe;
	JexDialog jw;
	public JexDialogButtonListener(String action, JButton button, JexMenuMouseListener jmml, JexDialog jw){
	    this.action = action;
            this.button = button;
	    this.jmml = jmml;
	    this.jw = jw;
	    closeMe = false;
	    if(jw == activeD) activeD = null;
        }

	public void clearMe(){
		closeMe = false;
		button.setText("Leave Open");
		button.setVisible(true);
	}

	public void actionPerformed(ActionEvent e){
		if(action.equals("opener")){
			if(closeMe) jw.jd.setVisible(false);
			if(activeD == jw) activeD = null;
			jmml.dontClose();
			closeMe = true;
			button.setText("Close");
			button.setVisible(false);
		}
		if(action.equals("closer")){
			jw.jd.setVisible(false);
		}
	}
    }

    private void closeActive(){
	if(activeD != null) {
		activeD.jd.setVisible(false);
		activeD = null;
	}
    }
		
    private class JexActionListener implements ActionListener {
        private String action;
        private JButton button;
        public JexActionListener(String action, JButton button){
            this.action = action;
            this.button = button;
        }
        
        public JexActionListener(String action){
            this.action = action;
            button = null;
        }
        
        public void actionPerformed(ActionEvent e){

            Point p = null;
            Rectangle r = null;
            if(button != null){
                p = button.getLocationOnScreen();
                r = button.getBounds();
            }

            if(action.equals("Layout")){makeDialog(p, r, "Layout", Jexconf.getProp("layouts")); return;}
	    if(action.equals("greek")){makeDialog(p, r, "greek", Jexconf.getProp("greek")); return;}
	    if(action.equals("Greek")){makeDialog(p, r, "Greek", Jexconf.getProp("Greek")); return;}
            if(action.equals("Symbols")){makeDialog(p, r, "Symbol", Jexconf.getProp("symbols")); return;}
	    if(action.equals("User")){makeDialog(p, r, "User", Jexconf.getProp("user")); return;}
	    if(action.equals("Matrix")){matrixDialog(p, r); return;}

	    closeActive();

           // System.out.println(action);
            DefaultContext d = jexPane.d;
	    d.set();

            if(action.equals("Preview")) {
                if(d.showEmpty) {
                    d.setPrint();
                    button.setText("Edit");
                }
                else {
                    d.setEdit();
                    button.setText("Preview");
                }
		Jexconf.setProp("preview", "" + !d.showEmpty, true);
            }

	    if(action.equals("Thin")) d.insertBox(new SpaceBox("\\,"));
	    if(action.equals("Medium")) d.insertBox(new SpaceBox("\\:"));
	    if(action.equals("Thick")) d.insertBox(new SpaceBox("\\;"));

	    jexPane.doAction(action);
	}
    }
	
    private class JexMouseListener extends MouseAdapter {
	
        public void mousePressed(MouseEvent e) {
	    closeActive();
            DefaultContext d = jexPane.d;
	    d.set();
            d.findCaret(e.getX(), e.getY());
            jexPane.mathStyle = "null";
            jexPane.repaint();
            jexPane.requestFocus();
        }
    }
    
    private class JexMenuMouseListener extends MouseAdapter {
    
        private boolean closeOnClick;
	private JexDialog jw;
	private boolean user = false;
	private boolean initCloseOnClick;
    
        public JexMenuMouseListener(boolean closeOnClick, JexDialog jw){
            this.closeOnClick = closeOnClick;
	    initCloseOnClick = closeOnClick;
	    this.jw = jw;
        }

	public void dontClose(){
		closeOnClick = false;
	}

	public void clearMe(){
		closeOnClick = initCloseOnClick;
        }

	public void setUser(){user = true;}
	
        public void mousePressed(MouseEvent e) {
        
            Component c = e.getComponent();
            JexPane jp = (JexPane) c;
            
            DefaultContext d = jp.d;
	    d.set();
            
            d.findCaret(e.getX(), e.getY());
            Selection s = d.sel;
            if(s.size() <= 1) return;
            int ssize = s.size();
            for(int i = 1; i <= (ssize-2); i++) s.removeLast();
            s.add(new Integer(1));
            d.sel.width = 1000;
	    d.copyonly = true;
	    int onmask = InputEvent.BUTTON1_DOWN_MASK;
	    if (user && !((e.getModifiersEx() & onmask) == onmask)){
	    //if(user && e.isPopupTrigger()){
		d.copyonly = false;
		jp.doAction("Paste");
		d.copyonly = false;
		Jexconf.setProp("user", d.getEqn().toTex(), true);
		jp.repaint();
	    }
	    else{
            	MathBox get = d.insertBox(new EmptyBox());
            	if(closeOnClick) jw.jd.setVisible(false);
	    	if(activeD == jw) activeD = null;
            	d = jexPane.d;
	    	d.set();
            	//always paste a duplicate of an existing box
            	d.insertBox(get.dup());
	    }
            jexPane.mathStyle = "null";
            jexPane.repaint();
            jexPane.requestFocus();
        }
    }
	
    private class JexMouseMotionListener extends MouseMotionAdapter {
	
        public void mouseDragged(MouseEvent e) {
            DefaultContext d = jexPane.d;
	    d.set();
            d.findSelWidth(e.getX(), e.getY());
            jexPane.repaint();
            jexPane.requestFocus();
        }
    }

    class closeThread extends Thread{
	public void run(){
		nWins--;
		if(immediateShutdown){
			//nWins--;
			System.out.println(nWins);
			if(nWins == 0) {
				System.out.println("Exiting immediately");
				System.exit(0);
			}
		}
		else{
			try{
				Thread.sleep(60000);
			}
			catch(Exception ex){ex.printStackTrace();}
			//nWins--;
			if(nWins == 0 && officeclass.queryExit()) System.exit(0);
		}
	}
    }

    
//Main window    
	
    class JexPane extends JPanel{
    
        public DefaultContext d;
        public JexDialog jd;
        public String mathStyle = "null";
	public boolean greek = false;
	public String tex = null;
	
        public void JexPane() {}

	private File getFile(String type, String sug){
		String cwd = "";
		JFileChooser fd = new JFileChooser(cwd);
		//fd.setFileFilter(new ffilter());
		if(type.equals("Open")) fd.showOpenDialog(gJexFrame);
		else {
			fd.setSelectedFile(new File(sug));
			int test = fd.showSaveDialog(gJexFrame);
			if(test == JFileChooser.CANCEL_OPTION) return null;
		}
		String temp = fd.getName(fd.getSelectedFile());
		if (temp == null) return null;
		File tdir = fd.getCurrentDirectory();
		cwd = tdir.getPath();
		return fd.getSelectedFile();
	}

	public boolean winDone(){
		int n = 0;
		if(d.dirty) n = JOptionPane.showConfirmDialog(gJexFrame, "All unsaved data will be lost. OK to exit?",    "Confirm Exit", JOptionPane.YES_NO_OPTION);
		if(n == 0) return true;
		return false;
	}


	public void doAction(String action){
	    d.set();
            if(action.equals("Grab")) office.grabEq();
	    if(action.equals("New")) {
		Jex jex = Jex.newJex("{}"); 
		//if(d.md != null)jex.setMathDisplay(d.md.getNewMathDisplay(jex));
		if(office != null) office.setJex(jex);
		return;
	    }
            if(action.equals("Save")){d.md.printEq(d.getEqn().toTex(), d); d.dirty = false; return;}
	    if(action.equals("SaveExit")){
		d.dirty = false;
		d.md.printEq(d.getEqn().toTex(), d); 
		gJexFrame.setVisible(false);
		checkClose();
		return;
            }
	    if(action.equals("Paste")){
		Clipboard clip = toolkit.getSystemClipboard();
		Transferable contents=clip.getContents(gJexFrame);
		if(contents==null) System.out.println("empty");
		else{
			if(contents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
    				try {
     					String data = (String) contents.getTransferData(DataFlavor.stringFlavor);
     					if(data==null) System.out.println("no text");
     					else{
						//System.out.println(data);
						int n = data.indexOf('<');
						boolean mml = false;
						if(n >= 0){
							if(data.substring(n, n+6).equals("<mrow>")) mml = true;
							if(data.substring(n, n+11).equals("<math:mrow>")) mml = true;
						}
						MathBox mb = null;
						if(mml) mb = MathBox.getMML(data);
						else mb = MathBox.getTex(data);
						d.insertBox(mb);
					}
    				} catch(IOException ex){
     					System.err.println("IOException");
    				} catch(UnsupportedFlavorException ex){
     					System.err.println("UnsupportedFlavorException");
    				}
   			}
			else {
				System.err.println("Wrong flavor.");
			}
		}

	    }

	    if(action.equals("Copy") || action.equals("Cut") || action.equals("Copy mml")){
		Clipboard clip = toolkit.getSystemClipboard();
		Selection oldsel = d.sel;
		if(action.equals("Copy") || action.equals("Copy mml")) d.copyonly = true;
		MathBox get = d.insertBox(new EmptyBox());
		if(d.copyonly){
			d.sel = oldsel;
			d.copyonly = false;
		}
		String ttex = null;
		if(action.equals("Copy mml")) ttex = get.toMML();
		else ttex = get.toTex();
		StringSelection sel = new StringSelection(ttex);
		clip.setContents(sel, sel);
		//System.out.println(d.getEqn().toMML() + "\n");
	    }
	    if(action.equals("Exit")){
		if(winDone()){
			gJexFrame.setVisible(false);
			checkClose();
		}
		return;
	    }

	    if(action.equals("Select All")){
		d.selectAll();
	    }

	    if(action.equals("Clear")){
		d.selectAll();
		d.insertBox(new EmptyBox());
	    }

            if(action.equals("png")){
		File f = getFile("Save As", "equation.png");
		if(f != null) writeGraphicFile(f, d.size, "png");
	    }

            if(action.equals("Ekit")){
                countEQ++; 
                writeGraphicEkit("eq"+Integer.toString(countEQ)+".png", d.size, "png");
            }


            if(action.equals("jpg")){
		File f = getFile("Save As", "equation.jpg");
		if(f != null) writeGraphicFile(f, d.size, "jpg");
	    }

	    if(action.equals("wmf")){
		File f = getFile("Save As", "equation.wmf");
		if(f != null) {
			Jexwmf wmf = new Jexwmf();
			wmf.tfile = f.getAbsolutePath();
			wmf.fsize0 = (float) d.size;
			wmf.printEq("", d);
		}
	    }

	    if(action.equals("txt")){
		File f = getFile("Save As", "equation.txt");
		if(f != null) {
			try{
				String s = d.getEqn().toTex();
				FileOutputStream out = new FileOutputStream(f);
				out.write(s.getBytes());
				out.close();
			}
			catch(IOException ex){};
		}
	    }

	    if(action.equals("mml")){
		File f = getFile("Save As", "equation.mml");
		if(f != null) {
			try{
				String s = d.getEqn().toMML();
				FileOutputStream out = new FileOutputStream(f);
				out.write(s.getBytes());
				out.close();
			}
			catch(IOException ex){};
		}
	    }

	    if(action.equals("testmml")){
		String z = d.getEqn().toMML();
		System.out.println(z);
		MMLParser mml = new MMLParser(z);
		RowBox rb = mml.parse();
		d.setEqn(rb);
	    }

	    if(action.equals("Save Configuration As")){
		File f = getFile(action, "conf.jex");
		if(f != null){
			Jexconf.setFile(f);
			Jexconf.writeConf();
		}
	    }

	     if(action.equals("Install All")) {
		office.installOffice();
		office.installJavaFonts();
		office.installFonts();
	     }
	     if(action.equals("Install for Office")) {
			String unm = System.getProperty("user.name").toLowerCase();
			if(unm.equals("root"))
				JOptionPane.showMessageDialog(gJexFrame, "Office install should not be done as root. Please logoff, and run jex from a user account.", "Error", JOptionPane.DEFAULT_OPTION);
			else office.installOffice();
	     }
	     if(action.equals("Install Java Fonts")) office.installJavaFonts();
	     if(action.equals("Install Office Fonts")) office.installFonts();

	     if(action.startsWith("Install")) action = "Immediate Shutdown";
	     if(action.equals("Immediate Shutdown")){
		immediateShutdown = true;
	     }

	    if(action.equals("test1")){d.insertBox(MathBox.getTex((String)Jexconf.getProp("test1")));}
	    if(action.equals("test2")){d.insertBox(MathBox.getTex((String)Jexconf.getProp("test2")));}
	    if(action.equals("test import mt")){
			Jexmt jmt = new Jexmt("/home/dlevine/samplemathtypedata");
			RowBox rb = jmt.parseMT();
			if(rb != null) d.setEqn(rb);
	    }
	    if(action.equals("Export TeX")) office.convertToTex();
	    if(action.equals("Import TeX")) {  // System.out.println("test this"); 
                                               TexPad texp= new TexPad(d,jexPane); 
                                               texp.setVisible(true); 
                                               };
 
	    if(action.equals("Update Equations")) office.updateEquations(d, false);
	    if(action.equals("Embed All Graphics")) office.updateEquations(d, true);

	     if(action.equals("Readme")) JOptionPane.showMessageDialog(gJexFrame, "<html><body bgcolor=\"#FFFFFF\">ctrl-z = undo<br>ctrl-x = cut<br>ctrl-v = paste<br>ctrl-m = save & exit<br>ctrl-j = sub/sup<br>ctrl-l = sub<br>ctrl-h = sup<br>ctrl-= = overbox<br>ctrl-g = toggle greek mode<br>ctrl-f = toggle text mode<br>ctrl-space = half space in math mode<br>&nbsp;&nbsp;full space in text mode<hr>ctrl-\\ = \\<br>\\ = enter tex mode<br>&nbsp;&nbsp;space to paste<br>&nbsp;&nbsp;escape to exit<br>&nbsp;&nbsp;backspace to erase<hr>enter = add a line<br>space = advance one char<hr>insert, delete, backspace as expected<br>left-arrow, right-arrow as expected<hr>any mouse button except the left<br>will paste into the user menu", "Readme", JOptionPane.DEFAULT_OPTION);
 
	     if(action.equals("About")){
		InputStream in = Jex.class.getResourceAsStream("ver.txt");
		int nmax = 10000;
		byte b[] = new byte[nmax];
		String s = "";
		int nread = 0;
		try { nread = in.read(b); }
		catch (Exception e) {System.err.println(e);}
		s = new String(b,0,nread);
		JOptionPane.showMessageDialog(gJexFrame, "Jex is an equation editor.\n" + "version " + s + "\nWritten by David K. Levine in 2002-3 and released under the GPL 2.\nCode and documentation at http://www.dklevine.com/general/software/jex/index.htm.", "About Jex", JOptionPane.DEFAULT_OPTION);
	    }
	
	    if(action.equals("Highlight Levels")) {
		d.highlightLevels = !d.highlightLevels;
		Jexconf.setProp("highlight", "" + d.highlightLevels, false);
	    }
            if(action.equals("Undo")) d.undo();
            if(action.equals("Redo")) d.redo();
            if(action.equals("Zoom In") | action.equals("+")) d.size = (int) Math.ceil(1.3 * d.size);
            if(action.equals("Zoom Out") | action.equals("-")) d.size = (int) Math.ceil(d.size / 1.3);
            if(action.equals("Zoom Normal") | action.equals("0")) d.size = Integer.parseInt(Jexconf.getProp("size"));
	    if(action.startsWith("Zoom")) Jexconf.setProp("size", "" + d.size, false);	
	    if(action.equals("Toggle Math/Text")) {jexPane.toggleMathRm(); return;}
	    if(action.equals("Fence")) d.insertBox(new FenceBox(new EmptyBox(), new EmptyBox(), new EmptyBox()));
	    if(action.equals("Add Row Above")) d.insertRow(0);
	    if(action.equals("Add Row Below")) d.insertRow(1);
	    if(action.equals("Add Column Left")) d.insertCol(0);
	    if(action.equals("Add Column Right")) d.insertCol(1);
            if(action.equals("Delete Row")) d.deleteRow();
            if(action.equals("Delete Column")) d.deleteCol();
	    if(action.equals("Set As Default")){
		Jexconf.setProp("highlight", "" + d.highlightLevels, false);
		Jexconf.setProp("size", "" + d.size, false);
		Jexconf.setProp("preview", "" + !d.showEmpty, true);
	    }
            jexPane.mathStyle = "null";
            jexPane.repaint();
            jexPane.requestFocus();
	}

	public void setEqn(String tex){
		d.setEqn(MathBox.getTex(tex));
		d.setStartCaret();
		jexPane.repaint();
		jexPane.requestFocus();
	}

	public void toggleMathRm(){
                if(jexPane.mathStyle.equals("mathrm")) jexPane.mathStyle = "math";
                if(jexPane.mathStyle.equals("math")) jexPane.mathStyle = "mathrm";
                if(jexPane.mathStyle.equals("null") & d.mathrm) jexPane.mathStyle = "math";
                if(jexPane.mathStyle.equals("null") & !d.mathrm) jexPane.mathStyle = "mathrm";
		setStatus();
	}

	public void setStatus(){
		String status = "";
		boolean mathrm = false;
            	if(mathStyle.equals("mathrm")) mathrm = true;
            	if(mathStyle.equals("null") & (d.mathrm)) mathrm = true;
		if(greek) status = status + "Greek Mode";
		else if(mathrm) status = status + "Text Mode";
		else status = status + "Math Mode";
		if(tex != null)	status = "Tex Mode: " + tex;
		status = "Size: " + d.size + "   " + status;
		statusBar.setText(status);
	}
		
        public void setup() {
            d = new DefaultContext();
	    updateBinding(Jexconf.getProp("printBinding"), Jexconf.getProp("editBinding"));
	    Jexfont.setPrintFace(Jexconf.getProp("printFace"));
	    Jexfont.setEditFace(Jexconf.getProp("editFace"));
	    if(Jexconf.getProp("mathItalic").equals("false")) Jexfont.mathItalic = false;
	    if(Jexconf.getProp("greekItalic").equals("true")) Jexfont.greekItalic = true;
            RowBox rb = new RowBox();
            d.setEqn(rb);
	    d.set();
            d.x = 0;
            d.y = 10;
            d.size = Integer.parseInt(Jexconf.getProp("size"));
	    d.highlightLevels = false;
	    if(Jexconf.getProp("highlight").equals("true")) d.highlightLevels = true;
	    if(Jexconf.getProp("preview").equals("true")) {
		d.setPrint();
		previewButton.setText("Edit");
	    }
            d.sel = new Selection();
            d.sel.add(new Integer(1));
            d.setEqn(MathBox.getTex(initEqn));
        }

	public BoxMetrics writeGraphicFile(File f, int fsize, String type){
            boolean oldshowEmpty = d.showEmpty;
            boolean oldshowPhantom = d.showPhantom;
            float oldhPad = d.hPad;
            int oldsize = d.size;
	    String oldbinding = d.binding;
	    d.set();
            d.setPrint();
            d.size = fsize;
	    d.set();
            d.makeMetrics();
	    BoxMetrics m = d.getEqn().boxMetrics;
	    int w = m.width + m.edgeE + m.edgeW;
	    int h = m.height + m.edgeN + m.edgeS;
	    BufferedImage g = (BufferedImage)createImage(w, h);
	    Graphics2D g2 = g.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);	
            g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
	    g2.setBackground(Color.white);
	    g2.clearRect(0, 0, w, h);
            d.setGraphics(g2);
	    d.display();
	    try{
	    	ImageIO.write(g, type, f);
	    } catch(java.io.IOException test){}
            d.printeq = false;
            d.showEmpty = oldshowEmpty;
            d.showPhantom = d.showPhantom;
            d.hPad = oldhPad;
            d.size = oldsize;
            d.binding = oldbinding;
	    return m;
}


// write to Ekit
public BoxMetrics writeGraphicEkit(String fname, int fsize, String type){

            File f = new File(fname);
            boolean oldshowEmpty = d.showEmpty;
            boolean oldshowPhantom = d.showPhantom;
            float oldhPad = d.hPad;
            int oldsize = d.size;
            String oldbinding = d.binding;
            d.set();
            d.setPrint();
            d.size = fsize;
            d.set();
            d.makeMetrics();
            BoxMetrics m = d.getEqn().boxMetrics;
            int w = m.width + m.edgeE + m.edgeW;
            m.edgeN=(int)(m.height/2); 
            int h = m.height + m.edgeN + m.edgeS;
            // System.out.println(m.height); S.Chekanov 
            // System.out.println(m.edgeN); 
            // System.out.println(m.edgeS);
            BufferedImage g = (BufferedImage)createImage(w, h);
            Graphics2D g2 = g.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);
            g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            g2.setBackground(Color.white);
            g2.clearRect(0, 0, w, h);
            d.setGraphics(g2);
            d.display();
            try{
                ImageIO.write(g, type, f);
            } catch(java.io.IOException test){}
            d.printeq = false;
            d.showEmpty = oldshowEmpty;
            d.showPhantom = d.showPhantom;
            d.hPad = oldhPad;
            d.size = oldsize;
            d.binding = oldbinding;

            mEkitCore.insertEqImage(fname);  
            return m;
}







	
    	public void paintComponent(Graphics g){
            super.paintComponent(g);
            //setBackground(Color.white);
	    if(d.highlightLevels) setBackground(d.getBaseColor());
	    else setBackground(Color.white);
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_DEFAULT);
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            g2.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_OFF);	
            g2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
            if(showFont){
                Font f = new Font(fontName, Font.PLAIN, 36);
                Font ff = new Font("Time New Roman", Font.PLAIN, d.size);
                for(int ii = 0; ii < 256; ii++){
                    g2.setFont(ff);
                    Integer iii = new Integer(ii);
                    g2.drawString(iii.toHexString(ii), 0, ii * 40);
                    g2.setFont(f);
                    char[] tt = new char[1];
                    tt[0] = (char) ii;
                    String str = new String(tt);
                    g2.drawString(str, 100, ii * 40);
                }
            
            }
            else{
            d.setGraphics(g2);
	    d.set();
            d.makeMetrics();
	    if(d.highlightLevels) d.displayLevel();
            boolean showCaret = true;
            if(d.sel == null) showCaret = false;
            if(showCaret){
                d.normalize();
                d.getEqn().setHighlight(false);
                d.displayCaret();
                d.sel.pop(d);
            }
            d.display();
            d.getEqn().setHighlight(false);
            if (showCaret & (d.sel.width == 0)) {
		d.displayCaret();
		setStatus();
	    }
            //statusBar.setText(d.eqn.toTex());
            //GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            //String fontlist[] = ge.getAvailableFontFamilyNames();
            //for(int a = 0; a < fontlist.length; a++) System.out.println(fontlist[a]);
            //this.requestFocus();
            }
        }
    }
}


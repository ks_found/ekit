package jex;

import java.awt.*;
import java.awt.font.*;
import java.awt.geom.*;
import java.util.*;
import java.lang.Math.*;
import java.awt.image.BufferedImage;

import jex.Jexfont;

class Jexmath{}

interface MathDisplay{
        //public abstract void grabEq();
        public abstract void printEq(String tex, DefaultContext d);
        public abstract void showChar(String str, String face, String style, int size, float ratio, float stretch, float aftr, int x, double y, BoxMetrics m, int vshift);
	//public abstract MathDisplay getNewMathDisplay(Object selfref);
}


//MATHBOX CLASSES

class BoxMetrics{
	public int width;
	public int height;
	public int baseline;
	public int x;
	public int y;
	public int edgeN;
	public int edgeS;
	public int edgeE;
	public int edgeW;
	public float slant0;
	public float slant1;
	public int charHeight;
	public int charHeight0;
	public int charHeight1;
        public DefaultContext d;

	public BoxMetrics(){
		x = 0;
		y = 0;
		slant0 = 0;
		slant1 = 0;
		edgeN = 0;
		edgeS = 0;
		edgeE = 0;
		edgeW = 0;
	}

	public int getHeight(int size){
		return Jexfont.getHeight(size);
	}
	public int getWidth(int size){
		return Jexfont.getWidth(size);
	}
}

//the linked list is a linked list of integers representing an index into a mathbox
//that is a collection of other mathboxes
class Selection extends LinkedList{
	public int width;
	public Selection old = null;
	public int maxWidth = 1000;

	public void pop(DefaultContext d){
		d.sel=d.sel.old;
	}

	public void push(DefaultContext d){
		Selection s = new Selection();
		s.width = width;
		s.old = this;
		d.sel = s;
	}

	public void dup(DefaultContext d){
		push(d);
		d.sel.addAll(this);
		d.sel.width = this.width;
	}

	public void empty(DefaultContext d){
		push(d);
		d.sel.old = null;
	}

	public boolean add(int j){
		return add(new Integer(j));
	}

	public void add(int i, int j){
		add(i, new Integer(j));
	}

	public int getInt(int i){
		Integer ii = (Integer) get(i);
		return ii.intValue();
	}

	public int getLastInt(){
		Integer ii = (Integer) getLast();
		return ii.intValue();
	}

	public int removeInt(int i){
		Integer ii = (Integer) remove(i);
		return ii.intValue();
	}

	public int removeLastInt(){
		Integer ii = (Integer) removeLast();
		return ii.intValue();
	}

	public void setWidth(DefaultContext d){
		Selection start = old;
		Selection stop = this;
		int minSize = Math.min(start.size(), stop.size());
		if(minSize == 0) {
			old.width = 0;
			d.sel = old;
			return;
		}
		int ci = start.size() - 2;
		int jj = 0;
		int kk = 0;
		int i;
		for(i = 0; i < minSize; i++){
			jj = start.getInt(i);
			kk = stop.getInt(i);
			if(jj != kk) break;
		}
		if(i == ci + 2) {
			old.width = 0;
			d.sel = old;
			return;
		}
		if(i == ci + 1){
			jj = start.getInt(ci + 1);
			kk = stop.getInt(ci + 1);
			old.width = kk - jj;
			d.sel = old;
			return;
		}
		if(kk > jj) old.width = maxWidth;
		else old.width = -maxWidth;
		d.sel = old;
	}

	public void normalize(DefaultContext d){
		dup(d);
		Selection top = d.sel;
		if(size() == 0) return;
		if(width >= 0) return;
		int sw = width;
		int ibot = top.removeLastInt();
		int newbot = ibot + sw;
		sw = -sw;
		if(newbot <= 1){
			sw = sw + newbot - 1;
			newbot = 1;
		}
		top.width = sw;
		top.add(newbot);
		return;
	}
}

class DefaultContext{

	private RowBox eqn;
	public Selection sel = new Selection();
        
	public Graphics2D g = null;
        public MathDisplay md;
	
	public LinkedList undoMe = new LinkedList();
	public LinkedList redoMe = new LinkedList();
	
	public Color caretColor = Color.red;

	static public Color defFontColor = Color.black;
	public Color fontColor = defFontColor;
	public Color highColor = Color.white;
	public Color backHighColor = Color.darkGray;
	
	public int x;
	public int y;
        
        public boolean copyonly = false;
        public boolean printeq = false;
        public boolean mathrm = false;
	public boolean dirty = false;
        
	public int size;
	public String binding = Jexfont.editBinding;
	
	public float italicAngleFactor = (float) 0.5;
	public float emptyPad = (float) 0.2;
	static public float defSlantf = 0.35f;
        public float slantf = defSlantf;
	
	public float hPad = (float) 0.2;
	
	public boolean showEmpty = true;
	public boolean showPhantom = true;
	public int maxSelWidth = 200;
        
        static public float defSupf = (float) 0.75;
	static float defSubf = (float) 0.55;
	static float defDscale = (float) 0.7;
	static float defPad = (float) 0.08;
	static float defPrepad = (float) 0.05;

        public float supf = defSupf;
	public float subf = defSubf;
	public float dscale = defDscale;
	public float pad = defPad;
	public float prepad = defPrepad;

	public float red = 0.55f;
	public float green = 0.99f;
	public float blue = 0.80f;
	public boolean highlightLevels = false;

	public MathBox matrix = null;
	public int matrixRow = 0;
	public int matrixCol = 0;

	private int z;

	public void updateBinding(){
		binding = Jexfont.editBinding;
	}

	public RowBox getEqn(){return eqn;}
	public void setEqn(RowBox rb){
		eqn = rb;
		eqn.setTe(eqn);
	}

	public void set(){
		if(g == null){
			BufferedImage bi = new BufferedImage(1, 1, BufferedImage.TYPE_INT_BGR);
			g = bi.createGraphics();
		}
		eqn.dc = this;
	}

	public void setGraphics(Graphics2D graphics){
		g = graphics;
		Jexfont.setGraphics(graphics);
	}

	public Color getBaseColor(){
		return new Color(red, green * red, blue * red);
	}

	public void setPrint(){
		showEmpty = false;
		showPhantom = false;
		hPad = (float) 0;
	}

	public void setEdit(){
		showEmpty = true;
		showPhantom = true;
		hPad = (float) 0.1;
	}
        
        public void setMenu(){
                showEmpty = true;
                showPhantom = true;
                hPad = (float) 0;
        }
	
	public void findSelWidth(int x, int y){
		eqn.dc = this;
		sel.push(this);
		if(!eqn.rightHitTest(this.x + z, this.y, x, y)) eqn.findCaret(this.x + z, this.y, x, y);
		sel.setWidth(this);
	}
	
	public void findCaret(int x, int y){
		eqn.dc = this;
		sel.empty(this);
		if(!eqn.rightHitTest(this.x + z, this.y, x, y)) eqn.findCaret(this.x + z, this.y, x, y);
		if(sel.size() == 0) {
			System.out.println("no sel");
			sel.add(1);
			sel.width = 0;
		}	
	}
	
	public void makeMetrics(){
		eqn.makeMetrics(size, 0);
		z = (int)(hPad * size);
	}

	public void setStartCaret(){
		sel.empty(this);
		sel.add(1);
		sel.width = 0;
	}

	public void selectAll(){
		sel.empty(this);
		sel.add(1);
		sel.width = 1000;
	}
	
	public void displayCaret(){
		if(!copyonly) {
			if(!eqn.displayCaret(x + z, y, 0)){
				Selection s = sel.old;
				setStartCaret();
				sel.old = s;
				eqn.displayCaret(x + z, y, 0);
			}
		}
	}
	
	public void display(){
		eqn.display(x + z, y);
	}
	public void displayLevel(){
		eqn.display(x + z, y, 1);
	}

	public void setUndo(){
		redoMe = new LinkedList();
		RowBox dup = (RowBox) eqn.dup();
		dup.setTe(dup);
		undoMe.add(dup);
		dirty = true;
	}
	public MathBox insertBox(MathBox mb){
		setUndo();
		MathBox ret = eqn.insertBox(mb);
		eqn.setTe(eqn);
		return ret;
	}
	public void makeAlign(){
		setUndo();
		eqn.makeAlign();
		eqn.setTe(eqn);
	}
	public void insertCol(int i){
		matrix = null;
		eqn.findMatrix(0);
		if(matrix != null){
			setUndo();
			MatrixBox mab = (MatrixBox) matrix;
			matrixCol = matrixCol + i;
			mab.insertColAt(matrixCol);
			eqn.setTe(eqn);
		}
	}
	public void deleteCol(){
		matrix = null;
		eqn.findMatrix(0);
		if(matrix != null){
			setUndo();
			MatrixBox mab = (MatrixBox) matrix;
			mab.deleteColAt(matrixCol);
			eqn.setTe(eqn);
		}
	}
	public void insertRow(int i){
		matrix = null;
		eqn.findMatrix(0);
		if(matrix != null){
			setUndo();
			MatrixBox mab = (MatrixBox) matrix;
			matrixRow = matrixRow + i;
			mab.insertRowAt(matrixRow);
			eqn.setTe(eqn);
		}
	}
	public void deleteRow(){
		matrix = null;
		eqn.findMatrix(0);
		if(matrix != null){
			setUndo();
			MatrixBox mab = (MatrixBox) matrix;
			mab.deleteRowAt(matrixRow);
			eqn.setTe(eqn);
		}
	}
	
	public void undo(){
		if(undoMe.size() > 0){ 
			redoMe.add(eqn);
			eqn = (RowBox) undoMe.removeLast();
			dirty = true;
		}
	}
	
	public void redo(){
		if(redoMe.size() > 0){
			undoMe.add(eqn);
			eqn = (RowBox) redoMe.removeLast();
			dirty = true;
		}
	}
	
	public void advance(int j){
		eqn.advance(0, j);
		if(sel.size() == 0){
			int jj = 1;
			if(j > 0) jj = eqn.c.size() + 1;
			sel.add(jj);
		}
	}

	public void normalize(){
		sel.normalize(this);
	}
	
	
}

class Caret{
	public MathBox m;
	public int i;
}

abstract class MathBox{
	
	public boolean isHighlight = false;
	
	public boolean isLayout() {return false;};
	public boolean isRow() {return false;}
	public boolean isEmpty() {return false;}
        public boolean isMathrm() {return false;}
	public boolean isAlign() {return false;}
	public boolean isMatrix() {return false;}
	public boolean isFence() {return false;}
	
	public BoxMetrics boxMetrics;
        
        public String buildState = "closed";
	public String lang = "tex";

	public RowBox te = null;
        
        public boolean isClosed(){
            if(buildState.equals("closed")) return true;
            return false;
        }
        
        public MathBox build(String x, MathBox mb){
            return this;
        }

	public void setLang(String s){lang = s;}

	public void setTe(RowBox rb){te = rb;}
        
        public void setStyle(String style){};

	public void passProp(String prop, int val){};

	public void validate(){};
	
	public void drawCaret(int x, int y, int h, float s){
		s = 0;
		int d = (int) Math.ceil(h * s);
		te.dc.g.setColor(te.dc.caretColor);
		te.dc.g.draw(new Line2D.Double(x + d, y, x, y + h));
	}
	
	public void drawSel(int x, int y, int width, int h, float s0, float s1){
		//int d = (int) Math.ceil(h * s);
		te.dc.g.setPaint(te.dc.backHighColor);
		te.dc.g.fill(new Rectangle2D.Double(x, y, width, h));
	}
        
        static public RowBox getTex(String z){
                TexParser tp = new TexParser(z);
                return tp.parse();
        }

	static public RowBox getMML(String z){
		MMLParser tp = new MMLParser(z);
		return tp.parse();
	}
	
	abstract public MathBox dup();
	abstract public BoxMetrics makeMetrics(int size, float stretch);
	abstract public void display(int x, int y);
	abstract public int findCaret(int bx, int by, int x, int y);
        abstract public String toTex();
	abstract public String toMML();
	
	public void selAdvance(MathBox m){
		selIncrement(1);
	}
	
	public void selIncrement(int j){
		int ii = te.dc.sel.removeLastInt();
		ii += j;
		te.dc.sel.add(ii);
	}
	
	public boolean advance(int i, int j){
		return false;
	}
	
	public void setHighlight(boolean isHighlight){
		this.isHighlight = isHighlight;
	}
	
	public void setFont(String f, int s, Color c){};
	
	public void setFont(String f){
		this.setFont(f, 0, null);
	}
	
	public void setFont(int s){
		this.setFont(null, s, null);
	}
	
	public void setFont(Color c){
		this.setFont(null, 0, c);
	}
	
	public boolean hitTest(int bx, int by, int x, int y){
		BoxMetrics m = boxMetrics;
		if(x <= bx - m.edgeW) return false;
		if(x >= bx + m.width + m.edgeE) return false;
		if(y >= by + m.height + m.edgeS) return false;
		if(y <= by - m.edgeN) return false;
		return true;
	}
	
	public boolean displayCaret(int bx, int by, int i){
		return false;
	}

	public void display(int x, int y, int level){}
	
	public void fixCaret(int i){}
	
	public Caret findSel(int i){
		return null;
	}

	public void findMatrix(int i){};
	
	public MathBox insertBox(MathBox m){
		te.dc.normalize();
		Caret s = findSel(0);
		return s.m.insertBox(s.i, m);
	}
	
	public MathBox insertBox(int i, MathBox m){
		return null;
	}
	
	public MathBox wrapBox(MathBox m){
		if(m == null) m = new EmptyBox();
		if(m.isRow()) return m;
		RowBox rb = new RowBox();
		rb.addChild(m);
		return rb;
	}
}

abstract class LayoutBox extends MathBox{

	public int slantPad(float s0, int height, float s1){
		if (s1 >= s0) return 0;
		float s = s0 - s1;
		int p = (int) Math.floor(s * te.dc.italicAngleFactor * height);
		return p;
	}
	
	public boolean isLayout() {return true;}
	public boolean useHpad() {return true;}
	
	public boolean isEmpty(){
		if(c.size() == 1){
			MathBox c0 = (MathBox) c.get(0);
			if(c0.isEmpty()) return true;
		}
		return false;
	}

	public LinkedList c = new LinkedList();
        
        public MathBox buildDivision(){
                DivisionBox db = new DivisionBox(this.wrapBox(this), this.wrapBox(new EmptyBox()), this.wrapBox(new EmptyBox()));
                db.buildState = "getMiddle";
                return (MathBox) db;        
        }
	
	public MathBox dup(){
		LayoutBox lb = null;
		try{
			lb = (LayoutBox) this.getClass().newInstance();
			lb.c = new LinkedList();	
			for (Iterator i = c.iterator(); i.hasNext(); ){
				MathBox mb = (MathBox) i.next();
				lb.c.add(mb.dup());
			}
		}
		catch(InstantiationException e){System.out.println("instant");}
		catch(IllegalAccessException e){System.out.println("access");}
		return lb;
	}

	public void nvalidate(int n){
		for(int jj = 0; jj < c.size(); jj++){
			MathBox mb = (MathBox)c.get(jj);
			if(mb.isRow()) mb.validate();
			else c.set(jj, wrapBox(new EmptyBox()));
		}
		for(int jj = c.size(); jj < n; jj++) addChild(wrapBox(new EmptyBox()));
	}
        
	public void setStyle(String style){
		for (Iterator i = c.iterator(); i.hasNext(); ){
                        MathBox mb = (MathBox) i.next();
                        mb.setStyle(style);
                }
	}

	public void setTe(RowBox rb){
		te = rb;
		for (Iterator i = c.iterator(); i.hasNext(); ){
                        MathBox mb = (MathBox) i.next();
                        mb.setTe(rb);
                }
	}
	
	public void setHighlight(boolean isHighlight){
		super.setHighlight(isHighlight);
		for (Iterator i = this.c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			mb.setHighlight(isHighlight);
		}
	}
	
	public void setFont(String f, int s, Color c){
		for (Iterator i = this.c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			mb.setFont(f, s, c);
		}
	}
	
	public void display(int x, int y){
		for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			mb.display(x + mb.boxMetrics.x, y + mb.boxMetrics.y);
		}
	}

	public void display(int x, int y, int level){
		float rr = 7.0f;
		//if(level > rr) level = (int) rr;
		//rr = (float) ((1.0f - te.dc.red) / rr);
		//rr = (float) (te.dc.red + (level * rr));

		float fac = 0.4f;
		float dif = (float) (1.0f - te.dc.red);
		fac = (float) (1.0f - Math.exp(-dif * level * fac));
		rr = (float)(te.dc.red + (fac * dif));
		
		float gg = (float) (te.dc.green * rr);
		float bb = (float) (te.dc.blue * rr);
		Color cc = new Color(rr, gg, bb);
		te.dc.g.setPaint(cc);
		te.dc.emptyPad = (float) 0.2;
		int pad = (int) (te.dc.emptyPad * boxMetrics.charHeight);
		te.dc.g.fill(new RoundRectangle2D.Double(x - boxMetrics.edgeW, y - boxMetrics.edgeN, boxMetrics.width + boxMetrics.edgeE + boxMetrics.edgeW,  boxMetrics.height + boxMetrics.edgeN + boxMetrics.edgeS, pad, pad));
		for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			mb.display(x + mb.boxMetrics.x, y + mb.boxMetrics.y, level + 1);
		}
	}
		
	
	public void addChild(MathBox mbox){
		c.add(mbox);
	}
	
	public BoxMetrics getChildMetrics(int index){
		MathBox mb = (MathBox) c.get(index);
		return mb.boxMetrics;
	}
	
	public int findChildCaret(int bx, int by, int x, int y){
		if(!hitTest(bx, by, x, y)) return 0;
		int state = 0;
		int ii = 1;
		for (Iterator i = c.iterator(); i.hasNext(); ){
			te.dc.sel.add(ii);
			MathBox mb = (MathBox) i.next();
			state = mb.findCaret(bx, by, x, y);
			if(state != 1) te.dc.sel.removeLast();
			if(state != 0) break;
			ii++;
		}
		if(state < 0) state = ii * state;
		if(state == 0) state = 2;
		return state;
	}
	
	public boolean displayCaret(int bx, int by, int i){
		if(te.dc.sel.size() <= i) return false;
                int j = te.dc.sel.getInt(i);
                if(j > c.size()) j = c.size();
		MathBox mb = (MathBox) c.get(j - 1);
		return mb.displayCaret(bx + boxMetrics.x, by + boxMetrics.y, i + 1);
	}
	
	public Caret findSel(int i){
		if(te.dc.sel.size() <= i) return null;
		int kk = te.dc.sel.getInt(i);
		if(te.dc.sel.size() == i + 1){
			Caret s = new Caret();
			s.m = this;
			s.i = kk;
			return s;
		}
		MathBox mb = (MathBox) c.get(kk - 1);
		return mb.findSel(i + 1);
	}

	public void findMatrix(int i){
		if(te.dc.sel.size() <= i) return;
		int j = te.dc.sel.getInt(i);
		if(j > c.size()) return;
		MathBox mb = (MathBox) c.get(j - 1);
		if (mb.isMatrix()) te.dc.matrix = mb;
		mb.findMatrix(i + 1);
	}
	
	public boolean sIncrement(int kk, int j){
		int extra = 0;
		if(isRow()) extra = 1;
		if((kk + j <= c.size() + extra) & (kk + j >= 1)){
			selIncrement(j);
			return true;
		}
		te.dc.sel.removeLast();
		return false;	
	}
	
	public boolean advanceInto(int i, int kk, int j){
		if(kk > c.size()) {
			if((j < 0) & isRow()) return true;
			return false;
		}
		int jj = 1;
		MathBox mb = (MathBox) c.get(kk - 1);
		if(!mb.isLayout()){
			if(isRow()) return true;
			return false;
		}
		LayoutBox lb = (LayoutBox) mb;
		if (j < 0) {
			jj = lb.c.size();
			if(lb.isRow()) jj++;
		}
		te.dc.sel.add(jj);
		if(lb.advanceInto(i + 1, jj, j)) return true;
		if(isRow()) return true;
		te.dc.sel.removeLast();
		return false;
	}
	
	public boolean advance(int i, int j){
		te.dc.sel.width = 0;
		int kk = te.dc.sel.getInt(i);
		int ks = te.dc.sel.size();
		MathBox mb = null;
		if(ks > i + 1){
			if(kk <= c.size() & kk >= 1){
				mb = (MathBox) c.get(kk - 1);
				if(mb.advance(i + 1, j)) return true;
			}
			if(j < 0) {
				if(isRow()) return true;
				if(!sIncrement(kk, j)) return false;
			}
		}
		if(ks == i + 1){
			if(j < 0) {
				if(!sIncrement(kk, j)) return false;
				kk = te.dc.sel.getInt(i);
			}
			if(kk <= c.size() & kk >= 1){
				mb = (MathBox) c.get(kk - 1);
				//FOR ADVANCE OVER, JUST GET RID OF THE NEXT LINE
				if(mb.isLayout()){if(advanceInto(i, kk, j)) return true;}			
			}
		}
		if(j > 0){if(!sIncrement(kk, j)) return false;}
		if(isRow()) return true;
		kk = te.dc.sel.getInt(i);		
		if(advanceInto(i, kk, j)) return true;
		return false;
	}
	
	public int findCaret(int bx, int by, int x, int y){
		bx += boxMetrics.x;
		by += boxMetrics.y;
		int state = findChildCaret(bx, by, x, y);
		if(state == 0) return 0;
		if(state == 1) return 1;
		return -1;
	}

	public void findEdges(int size){
		int minX = 0;
		int maxX = 0;
		int minY = 0;
		int maxY = 0;
		int hpad = 0;
                if (useHpad()) hpad = (int) Math.ceil(te.dc.hPad * Jexfont.getWidth(size));
                int vpad = 0;
                boxMetrics.height = boxMetrics.height + (2 * vpad);
                boxMetrics.width = boxMetrics.width + (2 * hpad);
		for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			BoxMetrics m = mb.boxMetrics;
                        
                        m.x = m.x + hpad;
                        m.y = m.y + vpad;
                        
			int cMinX = m.x - m.edgeW;
			int cMaxX = m.x + m.width + m.edgeE;
			int cMinY = m.y - m.edgeN;
			int cMaxY = m.y + m.height + m.edgeS;
			if(cMinX < minX) minX = cMinX;
			if(cMaxX > maxX) maxX = cMaxX;
			if(cMinY < minY) minY = cMinY;
			if(cMaxY > maxY) maxY = cMaxY;
		}
		if(minY < 0) boxMetrics.edgeN = -minY;
		if(minX < 0) boxMetrics.edgeW = -minX;
		if(maxX > boxMetrics.width) boxMetrics.edgeE = maxX - boxMetrics.width;
		if(maxY > boxMetrics.height) boxMetrics.edgeS = maxY - boxMetrics.height;
	}
}

class RowBox extends LayoutBox{
	
	public boolean isRow() {return true;}
	public boolean useHpad() {return false;}
        public boolean isMathrm() {
            if(buildState.equals("\\mathrm") || buildState.equals("\\operatorname")) return true;
            return false;
        }
	public boolean isAlign(){
		MathBox mb = (MathBox)c.get(0);
		return mb.isAlign();
	}
	public DefaultContext dc = null;

	public void passProp(String prop, int val){
	    boolean test = false;
	    int newVal = val;
	    if(prop.equals("divLev")) test = true;
	    if(test){
            	for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
                        mb.passProp(prop, newVal);
            	}
	    }	
	}

	public void makeAlign(){
		if(isAlign()){
			MatrixBox mb = (MatrixBox) c.get(0);
			int j = te.dc.sel.getInt(0);
			if(j != 1) return;
			j = te.dc.sel.getInt(1);
			j = j - 1;
			mb.insertRowAt((j/mb.cols) + 1);
		}
		else{
			MatrixBox mb = new MatrixBox((RowBox)wrapBox(new EmptyBox()), this, (RowBox)wrapBox(new EmptyBox()), (RowBox)wrapBox(new EmptyBox()));
			te.dc.setEqn((RowBox) wrapBox(mb));
		}
	}
        
        public String toTex(){
            String x = "{";
            for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
                        x = x + mb.toTex();
            }
            x = x + "}";
            return x;
        }

        public String toMML(){
            String x = "<mrow>";
            for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
                        x = x + mb.toMML();
            }
            x = x + "</mrow>";
            return x;
        }

	public void validate(){
		int mm = c.size() - 1;
		while(mm >= 0){
			MathBox mb = (MathBox) c.get(mm);
			if(mb.isRow())c.remove(mm);
			else mb.validate();
			mm--;
        	}
	}
        
        private MathBox pBox = null;

	public boolean bMathrm = false;
        
        public MathBox build(String x, MathBox m){
                if(x.equals("add")){
                    if(m.isEmpty()) return this;
                    if(isEmpty()) c.removeLast();
                    if(m.isRow()){
                        RowBox rb = (RowBox) m;
                        LinkedList cc = rb.c;
                        for (Iterator i = cc.iterator(); i.hasNext(); ) c.addLast(i.next());
                    }
                    else c.addLast(m);
                    if(buildState.equals("\\mathrm") || buildState.equals("\\operatorname")) {
                        if(buildState.equals("\\operatorname")) {
                            buildState = "closed";
                            build("add", new SpaceBox("\\,"));
                        }
                        else buildState = "closed";
                        this.setStyle("mathrm");
                    }
                    return this;
                }
                boolean isNew = false;
                if(x.equals("open")) isNew = true;
                if(x.equals("{") | x.equals("\\leftgrp") | x.equals("array") | x.equals("\\mathrm") | x.equals("\\operatorname")) {
                    buildState = x;
                    isNew = true;
                    pBox = m;
                }
                if(isNew) {
                    if(x.equals("\\operatorname")) c.add(new SpaceBox("\\,"));
                    else c.addLast(new EmptyBox());
                }
                if(x.equals("_") | x.equals("^")){
                    SubSupBox ssb = new SubSupBox(this, new EmptyBox(), new EmptyBox());
                    ssb.buildState = x;
                    return (MathBox) ssb;
                }
                if(x.equals("\\overgrp")) return buildDivision();
                if(x.equals("close")) buildState = "closed";
                if(x.equals("open")) buildState = "open";
                if(x.equals("}") | x.equals("\\rightgrp")){
                    if(!buildState.equals(Jextex.getLeft(x))) System.out.println("ERROR: " + x + " does not match");
		    if(bMathrm) this.setStyle("mathrm");
                    buildState = "closed";
                    if(x.equals("\\rightgrp")) return pBox;
                }
                if(x.equals("&") | x.equals("\\\\") | x.equals("\\end")){
                    if(!buildState.equals(Jextex.getLeft(x))) System.out.println("ERROR: " + x + " does not match");
                    buildState = "closed";
                    if(pBox != null) return pBox.build(x, this);
		    else return wrapBox(new EmptyBox());
                }
                
                
                return this;
        }       
	
	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		m.width = 0;
		m.height = 0;
		m.baseline = 0;
		m.charHeight = 0;
		int belowBase = 0;
		int aboveBase = 0;
		float lastSlant = 0;
		int lastCharHeight = 0;
		boolean first = true;
		for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			if(first) mb.passProp("rowPos", 0);
			else mb.passProp("rowPos", 1);
			BoxMetrics cm = mb.makeMetrics(size, stretch);
			if(first){
				m.slant0 = cm.slant0;
				m.charHeight0 = cm.charHeight0;
				first = false;
			}
			else{
				int hh = (int) Math.ceil(Math.min(lastCharHeight, cm.charHeight0));
				m.width += slantPad(lastSlant, hh, cm.slant0);
			}
			cm.x = m.width;
			lastCharHeight = cm.charHeight1;
			lastSlant = cm.slant1;
			m.width += cm.width;
			if(cm.charHeight >= m.charHeight) m.charHeight = cm.charHeight;
			mb.boxMetrics.y = cm.baseline;
			if(cm.baseline >= m.baseline) m.baseline = cm.baseline;
			int cmBelowBase = cm.height - cm.baseline;
			if(cmBelowBase >= belowBase) belowBase = cmBelowBase;
			int cmAboveBase = m.baseline;
			if(cmAboveBase > cm.height) cmAboveBase = cm.height;
			if(cmAboveBase >= aboveBase) aboveBase = cmAboveBase;
		}
		m.slant1 = lastSlant;
		m.charHeight1 = lastCharHeight;
		m.height = aboveBase + belowBase;
		for (Iterator i = c.iterator(); i.hasNext(); ){
			MathBox mb = (MathBox) i.next();
			mb.boxMetrics.y = m.baseline - mb.boxMetrics.y;
		}
		boxMetrics = m;
		findEdges(size);
		return m;
	}

	public boolean rightHitTest(int bx, int by, int x, int y){
		if(x >= bx + boxMetrics.width){
			te.dc.sel.width = 0;
			te.dc.sel.add(c.size() + 1);
			return true;
		}
		return false;
	}     
	
	public int findCaret(int bx, int by, int x, int y){
		bx += boxMetrics.x;
		by += boxMetrics.y;
		int state = findChildCaret(bx, by, x, y);
		if(state == 0) return 0;
		if(state == 1) return 1;
		MathBox mb = null;
		int ii = 1;
		int ax;
		for (Iterator i = c.iterator(); i.hasNext(); ){
			mb = (MathBox) i.next();
			ax = bx + mb.boxMetrics.x;
			if(x <= ax + (mb.boxMetrics.width/2)) break;
			ii++;
		}
		te.dc.sel.width = 0;
		te.dc.sel.add(ii);
		return 1;
	}
	
	public boolean displayCaret(int bx, int by, int i){
		if(te.dc.sel.size() == i + 1) {
			if(isEmpty()){
				te.dc.sel.width = 1;
				if(te.dc.sel.getInt(i) > 1) te.dc.sel.width = -1;
				te.dc.sel.old = null;
				te.dc.normalize();
			}
			int kk = te.dc.sel.getInt(i) - 1;
			bx += boxMetrics.x;
			by += boxMetrics.y;
			int found = 0;
			MathBox mb = null;
                        int ijk = kk - 1;
			if(kk >= c.size()){
				mb = (MathBox) c.getLast();
				found = mb.boxMetrics.width;
                                ijk = c.size() - 1;
			}
			else mb = (MathBox) c.get(kk);
                        te.dc.mathrm = false;
                        if(ijk >= 0){
                            MathBox lmb = (MathBox) c.get(ijk);
                            if(lmb.isMathrm()) te.dc.mathrm = true;
                        }
                        //if(te.dc.mathrm) System.out.println("mathrm mode");
                        //else System.out.println("math mode");
			bx += mb.boxMetrics.x + found;
			if(te.dc.sel.width == 0 | (!te.dc.showEmpty & isEmpty())) 
                            drawCaret(bx, by, boxMetrics.height + boxMetrics.edgeS, mb.boxMetrics.slant1);
			else {
				int kk2 = kk + te.dc.sel.width;
				int sFact = 0;
				if(kk2 >= c.size()) {
					kk2 = c.size() - 1;
					sFact = 1;
				}
				MathBox mb2 = (MathBox) c.get(kk2);
				int sWidth = (sFact * mb2.boxMetrics.width) + mb2.boxMetrics.x - mb.boxMetrics.x - found;
				drawSel(bx, by, sWidth, boxMetrics.height + boxMetrics.edgeS, mb.boxMetrics.slant1, mb.boxMetrics.slant1);
				if(sFact == 0) kk2--;
				for(i = kk; i <= kk2; i++){
					mb = (MathBox) c.get(i);
					mb.setHighlight(true);
				}
			}
			return true;
		}
		return super.displayCaret(bx, by, i);
	}

	
	public MathBox insertBox(int i, MathBox m){
		RowBox rb = new RowBox();
		rb.setTe(te);
		m.setTe(te);
		int kk = i - 1;
		int last = kk + te.dc.sel.width;
		if(last >= c.size()) last = c.size();
		if(!te.dc.copyonly) for(int j = kk; j  < last; j++) rb.addChild((MathBox)c.remove(kk));
                else            for(int j = kk; j  < last; j++) rb.addChild((MathBox)c.get(j));
		if(rb.c.size() == 0) {
			rb.addChild(new EmptyBox());
			rb.setTe(te);
		}
		te.dc.sel.width = 0;
		if(kk > c.size()) kk = c.size(); 
		if(!m.isEmpty()){
                    int jc;
                    if(!m.isRow()){
 			c.add(kk, m);
			m.selAdvance(rb);                   
                    }
                    else {
                        RowBox mrb = (RowBox) m;
                        jc = mrb.c.size();
                        for(int j = 0; j < jc; j++){
                            MathBox mm = (MathBox) mrb.c.get(j);
                            c.add(kk + j, mm);
                            if(j == 0) mm.selAdvance(rb);
                            else mm.selAdvance(new EmptyBox());
                        }
                    }
		}
		if(c.size() == 0) c.add(new EmptyBox());
		return rb;
	}
}

class SubSupBox extends LayoutBox{

	private float supf = DefaultContext.defSupf;
	private float subf = DefaultContext.defSubf;
	private float dscale = DefaultContext.defDscale;
	private float pad = DefaultContext.defPad;
	private float prepad = DefaultContext.defPrepad;
        private float slantf = DefaultContext.defSlantf;
	
	public SubSupBox(){
            initSubSup();
        }

	public SubSupBox(MathBox m0, MathBox m1, MathBox m2){
		addChild(wrapBox(m0));
		addChild(wrapBox(m1));
		addChild(wrapBox(m2));
                initSubSup();
	}
        
        public void initSubSup(){
	    if(te != null){
		if(te.dc != null){
            		supf = te.dc.supf;
            		subf = te.dc.subf;
            		dscale = te.dc.dscale;
            		pad = te.dc.pad;
            		prepad = te.dc.prepad;
            		slantf = te.dc.slantf;
		}
	    }	
        }

	public void validate(){
		nvalidate(3);
	}
        
        public String toTex(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            MathBox m2 = (MathBox) c.get(2);
            String x = m0.toTex() + "_" + m1.toTex() + "^" + m2.toTex();
            return x;
        }

        public String toMML(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            MathBox m2 = (MathBox) c.get(2);
            String x = "<msubsup>" + m0.toMML() + " " + m1.toMML() + " " + m2.toMML() + "</msubsup>";
            return x;
        }

        public MathBox buildMML(String x, MathBox mb){
            if(x.equals("add")){
		if(buildState.equals("getSuperscript")) buildState = "closed";
                if(buildState.equals("getSubscript")) buildState = "getSuperscript";
                if(buildState.equals("getBody")) buildState = "getSubscript";
		addChild(wrapBox(mb));
            }
            else buildState = "getBody";
            return this;
        }
        
        public MathBox build(String x, MathBox mb){
	    if(lang.equals("mml")) return buildMML(x, mb);
            if(x.equals("add")){
                RowBox rb = null;
                if(buildState.equals("_")) rb = (RowBox) c.get(1);
                if(buildState.equals("^")) rb = (RowBox) c.get(2);
                rb.build("add", mb);
                buildState = "closed";
            }
            if(x.equals("_") | x.equals("^")) buildState = x;
            if(x.equals("\\overgrp")) return buildDivision();
            return this;
        }
	
	public void selAdvance(MathBox mb){
		MathBox c0 = (MathBox) c.get(0);
		if(c0.isEmpty()){
			c.remove(0);
			c.add(0, mb);
			te.dc.sel.add(new Integer(2));
			te.dc.sel.add(new Integer(1));
			te.dc.sel.width = 1;
		}
		else super.selAdvance(mb);
	}
	
	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		MathBox mb0 = (MathBox) c.get(0);
		BoxMetrics m0 = mb0.makeMetrics(size, stretch);
		MathBox mb1 = (MathBox) c.get(1);
		int dsize = (int) Math.ceil(size * dscale);
		BoxMetrics m1 = mb1.makeMetrics(dsize, stretch);
		MathBox mb2 = (MathBox) c.get(2);
		BoxMetrics m2 = mb2.makeMetrics(dsize, stretch);
		
		m.slant0 = m0.slant0;
		m.slant1 = m0.slant1;
		if(mb2.isEmpty()) m.slant1 = 0;
		
		int subdif = (int) Math.ceil(subf * Jexfont.getHeight(dsize));
		int supdif = (int) Math.ceil(supf * Jexfont.getHeight(dsize));
		int basedif = 0;
		if(m0.height < m0.baseline) basedif = m0.baseline - m0.height;
		int xdif1 = m1.width;
		int xdif2 = m2.width;
		int zdif = (int)Math.floor(size * prepad);
	
		int height2 = m2.height - supdif - basedif;
		m.height = m0.height + Math.max(0,m1.height - subdif) + Math.max(0,height2);
	
		mb0.boxMetrics.x = 0;
		mb1.boxMetrics.x = m0.width + zdif;
		mb2.boxMetrics.x = m0.width + zdif;
		mb2.boxMetrics.x = m0.width;
		
		mb2.boxMetrics.y = 0;
		mb0.boxMetrics.y = height2;
		mb1.boxMetrics.y = m2.height - supdif + m0.height - subdif;

		if(height2 < 0){
			mb2.boxMetrics.y = mb2.boxMetrics.y - height2;
			mb1.boxMetrics.y = mb1.boxMetrics.y - height2;
			mb0.boxMetrics.y = 0;
		}
		
		m.baseline = mb0.boxMetrics.y + m0.baseline;
		
		m.charHeight = m.baseline;
		m.charHeight0 = m0.charHeight0;
		m.charHeight1 = m.charHeight;
		if(mb2.isEmpty()) m.charHeight1 = m0.charHeight1;
                
		if(mb0.boxMetrics.slant1 != 0){
                        float m0s = mb0.boxMetrics.slant1;
                        float m1s = mb1.boxMetrics.slant0;
                        float m2s = mb2.boxMetrics.slant0;
			int hh = mb0.boxMetrics.height;
			int subshift = slantPad(m0s, hh, ((1-slantf) * m0s) + (slantf * m1s));
			int supshift = slantPad(m0s, hh, ((1-slantf) * m0s) + (slantf * m2s));
			xdif1 += subshift;
			xdif2 += supshift;	
			subshift += (int) -Math.floor(mb0.boxMetrics.slant1 * (mb1.boxMetrics.height - subdif));
			supshift += (int)  Math.ceil(mb0.boxMetrics.slant1 * (mb0.boxMetrics.height - supdif));
			//supshift += (int)  Math.ceil(mb0.boxMetrics.slant1 * mb0.boxMetrics.charHeight);
			mb1.boxMetrics.x += subshift;
			mb2.boxMetrics.x += supshift;
		}
		
		int xdif = Math.max(xdif1, xdif2);
		m.width = m0.width + xdif + zdif + (int)Math.ceil(size * pad);
		boxMetrics = m;
		findEdges(size);
		return m;
	}
}

class FenceBox extends LayoutBox{

	public FenceBox(){}

	public FenceBox(MathBox m0, MathBox m1, MathBox m2){
		addChild(wrapBox(m0));
		addChild(wrapBox(m1));
		addChild(wrapBox(m2));
	}

	public void validate(){
		nvalidate(3);
	}

	public boolean isFence() {return true;}
        
        public String toTex(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            MathBox m2 = (MathBox) c.get(2);
            String left = m0.toTex();
            if(m0.isEmpty()) left = ".";
            String right = m2.toTex();
            if(m2.isEmpty()) right = ".";
            String x = " " + Jextex.texMacro("\\leftgrp" + left) + " " + m1.toTex() + " ";
            x = x + Jextex.texMacro("\\rightgrp" + right) + " ";
            return x;
        }

	private String fixFence(String fence){
 	    //String out = fence.replaceAll("<mi>", "");
	    //out = out.replaceAll("</mi>", "");
	    //out = out.replaceAll("<mrow>", "");
	    //out = out.replaceAll("</mrow>", "");
	    String out = fence.substring(10, fence.length() - 12);
	    return out;
	}	    

        public String toMML(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            MathBox m2 = (MathBox) c.get(2);
            String left = m0.toMML();
            if(m0.isEmpty()) left = "<mrow><mi></mi></mrow>";
            String right = m2.toMML();
            if(m1.isEmpty()) right = "<mrow><mi></mi></mrow>";
            String x = "<mo fence=\"true\">" + fixFence(left) + "</mo>" + m1.toMML() + "<mo fence=\"true\">" + fixFence(right) + "</mo>";
            return x;
        }
        
        private MathBox fixDot(MathBox mb){
            RowBox rb = (RowBox) mb;
            if(rb.c.size() != 1) return mb;
            MathBox cb = (MathBox) rb.c.get(0);
            String s = cb.toTex();
            if(!s.equals("."))return mb;
            rb.c.removeLast();
            rb.c.addLast(new EmptyBox());
            return mb;
        }
        
        public MathBox build(String x, MathBox mb){
	    if(x.equals("_") || x.equals("^")) {
		RowBox rb = new RowBox();
		rb.build("{", null);
		rb.build("add", this);
		rb.build("}", null);
		return rb.build(x, mb);
	    }
            if(buildState.equals("getRight")){
                addChild(fixDot(wrapBox(mb)));
                buildState = "closed";
            }
            if(buildState.equals("\\leftgrp")){
                addChild(fixDot(wrapBox(mb)));
                RowBox rb = new RowBox();
                rb.build(buildState, this);
                addChild(rb);
                buildState = "getRight";
                return rb;
            }
            if(x.equals("\\leftgrp")) buildState = x;
            return this;
        }
        
	
	public void selAdvance(MathBox mb){
		MathBox c1 = (MathBox) c.get(1);
		if(c1.isEmpty()){
			c.remove(1);
			c.add(1, mb);
			te.dc.sel.add(new Integer(1));
			te.dc.sel.add(new Integer(1));
			te.dc.sel.width = 1;
		}
		else super.selAdvance(mb);
	}
	
	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		MathBox mb0 = (MathBox) c.get(0);
		BoxMetrics m0 = mb0.makeMetrics(size, stretch);
		MathBox mb1 = (MathBox) c.get(1);
		BoxMetrics m1 = mb1.makeMetrics(size, stretch);
		float stretch0 = (float) m1.height;
		stretch0= -stretch0/m0.height;
		m0 = mb0.makeMetrics(size, stretch0);
		MathBox mb2 = (MathBox) c.get(2);
		BoxMetrics m2 = mb2.makeMetrics(size, stretch0);
		
		m.slant0 = 0;
		m.slant1 = 0;
		
		mb0.boxMetrics.x = 0;
		mb1.boxMetrics.x = m0.width;
		mb2.boxMetrics.x = m0.width + m1.width;
		
		mb2.boxMetrics.y = 0;
		mb0.boxMetrics.y = 0;
		mb1.boxMetrics.y = 0;

		m.baseline = m1.baseline;
		m.height = m1.height;
		m.charHeight = m.baseline;
		m.charHeight0 = m.charHeight;
		m.charHeight1 = m.charHeight;

		m.width = m0.width + m1.width + m2.width;
		boxMetrics = m;
		findEdges(size);
		return m;
	}
}

class OverBox extends LayoutBox{

	public OverBox(){}

	public OverBox(MathBox m0, MathBox m1){
		addChild(wrapBox(m0));
		addChild(wrapBox(m1));
	}

	public boolean useHpad(){return false;}
        
        public String toTex(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            String x = " " + Jextex.texMacro("\\overset" + m1.toTex()) + m0.toTex();
            return x;
        }

        public String toMML(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            String x = "<mover>" + m0.toMML() + " " + m1.toMML() + "</mover>";
            return x;
        }
        
        public MathBox build(String x, MathBox mb){
	    if(x.equals("_") || x.equals("^")) {
		RowBox rb = new RowBox();
		rb.build("{", null);
		rb.build("add", this);
		rb.build("}", null);
		return rb.build(x, mb);
	    }
            if(x.equals("add")){
                if(buildState.equals("getBody")){
                    buildState = "closed";
                    c.set(0, wrapBox(mb));
		    if(lang.equals("mml")){
			LinkedList c1 = new LinkedList();
			c1.add(c.get(1));
			c1.add(c.get(0));
			c = c1;
		    }
                }
                if(buildState.equals("getDecoration")){
                    buildState = "getBody";
                    addChild(wrapBox(new EmptyBox()));
                    addChild(wrapBox(mb));
                }
            }
            else buildState = "getDecoration";
            return this;
        }
	
	public void selAdvance(MathBox mb){
		MathBox c0 = (MathBox) c.get(0);
		if(c0.isEmpty()){
			c.remove(0);
			c.add(0, mb);
			te.dc.sel.add(new Integer(2));
			te.dc.sel.add(new Integer(1));
			te.dc.sel.width = 1;
		}
		else super.selAdvance(mb);
	}
	
	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		MathBox mb0 = (MathBox) c.get(0);
		BoxMetrics m0 = mb0.makeMetrics(size, stretch);
		MathBox mb1 = (MathBox) c.get(1);
		BoxMetrics m1 = mb1.makeMetrics(size, stretch);
		float stretch1 = (float) m0.width;
		if(m0.width < (size / 3)) stretch1 = (float) (size / 2);
		stretch1= stretch1/m1.width;
		m1 = mb1.makeMetrics(size, stretch1);
		
		m.slant0 = m0.slant0;
		m.slant1 = m0.slant1;
		
		mb0.boxMetrics.x = 0;
		mb1.boxMetrics.x = (int) Math.floor(m0.slant0 * m0.charHeight0);
		
		int bump = m1.height - m0.baseline + m0.charHeight;
		mb0.boxMetrics.y = bump;
		mb1.boxMetrics.y = 0;
		if(bump < 0){
			mb0.boxMetrics.y = 0;
			mb1.boxMetrics.y = -bump;
		}

		bump = Math.max(0, bump);
		m.baseline = m0.baseline + bump;
		m.height = m0.height;
		m.charHeight = m0.charHeight + m1.height;
		m.charHeight0 = m0.charHeight0;
		m.charHeight1 = m0.charHeight1;

		m.width = m0.width;
		if(mb1.boxMetrics.height>2) mb1.boxMetrics.y -= m.charHeight1/5;
		boxMetrics = m;
		findEdges(size);
		m.edgeN = m.edgeN + (int)(0.51 + (0.25 * m.height));
		return m;
	}
}

class UnderBox extends OverBox{

	public UnderBox(){}

	public UnderBox(MathBox m0, MathBox m1){
		addChild(wrapBox(m0));
		addChild(wrapBox(m1));
	}

	public void validate(){
		nvalidate(2);
	}
        
        public String toTex(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            String x = " " + Jextex.texMacro("\\underset" + m1.toTex()) + m0.toTex();
            return x;
        }

        public String toMML(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            String x = "<munder>" + m0.toMML() + " " + m1.toMML() + "</munder>";
            return x;
        }

	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		MathBox mb0 = (MathBox) c.get(0);
		BoxMetrics m0 = mb0.makeMetrics(size, stretch);
		MathBox mb1 = (MathBox) c.get(1);
		BoxMetrics m1 = mb1.makeMetrics(size, stretch);
		float stretch1 = (float) m0.width;
		stretch1= stretch1/m1.width;
		m1 = mb1.makeMetrics(size, stretch1);
		
		m.slant0 = m0.slant0;
		m.slant1 = m0.slant1;
		
		mb0.boxMetrics.x = 0;
		mb1.boxMetrics.x = 0;
		
		int bump = m1.height - m0.baseline + m0.charHeight;
		mb0.boxMetrics.y = 0;
		mb1.boxMetrics.y = m0.height + m0.edgeS;

		m.baseline = m0.baseline;
		m.height = m0.height;
		m.charHeight = m0.charHeight;
		m.charHeight0 = m.charHeight;
		m.charHeight1 = m.charHeight;

		m.width = m0.width;
		boxMetrics = m;
		findEdges(size);
		return m;
	}
}

class DivisionBox extends LayoutBox{

	public DivisionBox(){}
	private int divLev;

	public DivisionBox(MathBox m0, MathBox m2){
		addChild(wrapBox(m0));
		addChild(wrapBox(new CharBox(45)));
		addChild(wrapBox(m2));
	}
        
	public DivisionBox(MathBox m0, MathBox m1, MathBox m2){
		addChild(wrapBox(m0));
		addChild(wrapBox(m1));
		addChild(wrapBox(m2));
	}

	public void passProp(String prop, int val){
		if(prop.equals("divLev")) divLev = val;
	}
	public void setTe(RowBox rb){
		super.setTe(rb);
		divLev = 0;
	}

	public void validate(){
		nvalidate(3);
	}
        
        public String toTex(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            MathBox m2 = (MathBox) c.get(2);
            String x = " {" + m0.toTex() + Jextex.texMacro("\\overgrp" + m1.toTex()) + m2.toTex() + "} ";
            return x;
        }

        public String toMML(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            MathBox m2 = (MathBox) c.get(2);
	    String x;
	    if(m1.toTex().equals("{-}")) x = "<mfrac>" + m0.toMML() + " " + m2.toMML() + "</mfrac>";
	    else x = "<mfracext>" + m0.toMML() + " " + m1.toMML() + " " + m2.toMML() + "</mfracext>";
            return x;
        }

	public boolean isExt = true;

        public MathBox buildMML(String x, MathBox mb){
            if(x.equals("add")){
		if(buildState.equals("getBottom")) buildState = "closed";
                if(buildState.equals("getMiddle")) {
			if(isExt) buildState = "getBottom";
			else {
				buildState = "closed";
				MathBox cb = new CharBox('-');
				addChild(wrapBox(cb));
			}
		}
                if(buildState.equals("getTop")) buildState = "getMiddle";
		addChild(wrapBox(mb));
            }
            else buildState = "getTop";
            return this;
        }
        
        public MathBox build(String x, MathBox mb){
	    if(lang.equals("mml")) return buildMML(x, mb);
	    if(x.equals("frac")) buildState = "getTop";
            if(x.equals("add")){
                if(buildState.equals("getBottom")){
                    buildState = "closed";
                    c.set(2, wrapBox(mb));
                }
                if(buildState.equals("getMiddle")){
                    buildState = "getBottom";
                    c.set(1, wrapBox(mb));
                }
		if(buildState.equals("getTop")){
			addChild(wrapBox(mb));
			addChild(wrapBox(new CharBox('-')));
			addChild(wrapBox(mb));
			buildState = "getBottom";
		}
            }
            return this;
        }
        
	public void selAdvance(MathBox mb){
		MathBox c0 = (MathBox) c.get(0);
		if(c0.isEmpty()){
			c.remove(0);
			c.add(0, mb);
			te.dc.sel.add(new Integer(3));
			te.dc.sel.add(new Integer(1));
			te.dc.sel.width = 1;
		}
		else super.selAdvance(mb);
	}


	public BoxMetrics makeMetrics(int size, float stretch){
		int dsize = size;
		if(divLev > 1) dsize = (int) Math.ceil(size * te.dc.dscale);
		BoxMetrics m = new BoxMetrics();
		MathBox mb0 = (MathBox) c.get(0);
		mb0.passProp("divLev", divLev + 1);
		BoxMetrics m0 = mb0.makeMetrics(dsize, stretch);
		MathBox mb1 = (MathBox) c.get(1);
		BoxMetrics m1 = mb1.makeMetrics(dsize, stretch);
		float stretch1 = (float) m0.width;
		stretch1= stretch1/m1.width;
		
		MathBox mb2 = (MathBox) c.get(2);
		mb2.passProp("divLev", divLev + 1);
		BoxMetrics m2 = mb2.makeMetrics(dsize, stretch);
		float stretch2 = (float) m2.width;
		stretch2= stretch2/m1.width;
		
		stretch1 = Math.max(stretch1, stretch2);
		
		m1 = mb1.makeMetrics(dsize, stretch1);
		
		m.slant0 = 0;
		m.slant1 = 0;
		
		m.width = (int) Math.max(m0.width, m2.width);
		mb0.boxMetrics.x = (m.width - m0.width) / 2;
		mb1.boxMetrics.x = 0;
		mb2.boxMetrics.x = (m.width - m2.width) / 2;
		
		mb0.boxMetrics.y = 0;
		mb1.boxMetrics.y = m0.height + m0.edgeS + (int)(0.5 * Jexfont.getThinWidth(dsize));
		m.baseline = m0.height + m0.edgeS + (Jexfont.getHeight(dsize)/2);
		mb2.boxMetrics.y = mb1.boxMetrics.y + m1.height - (int)(0.2 * Jexfont.getThinWidth(dsize));
		m.height = mb2.boxMetrics.y + m2.height;

		m.charHeight = m0.baseline;
		m.charHeight0 = m.charHeight;
		m.charHeight1 = m.charHeight;

		boxMetrics = m;
		findEdges(size);
		return m;
	}
}

class RootBox extends LayoutBox{

	public RootBox(){}

	public RootBox(MathBox m0, MathBox m1){
		addChild(wrapBox(m0));
		addChild(wrapBox(m1));
	}

	public void validate(){
		nvalidate(2);
	}
        
        public String toTex(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            String x = " \\sqrt[" + m0.toTex() + "]" + m1.toTex() + " ";
            return x;
        }

        public String toMML(){
            MathBox m0 = (MathBox) c.get(0);
            MathBox m1 = (MathBox) c.get(1);
            String x = "<mroot>" + m1.toMML() + " " + m0.toMML() + "</mroot>";
            return x;
        }

	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		MathBox mb0 = (MathBox) c.get(0);
		BoxMetrics m0 = mb0.makeMetrics(size, stretch);
		MathBox mb1 = (MathBox) c.get(1);
		BoxMetrics m1 = mb1.makeMetrics(size, stretch);
		
		m.slant0 = 0;
		m.slant1 = 0;
		
		mb0.boxMetrics.x = (m1.width - m0.width) / 2;
		mb1.boxMetrics.x = 0;
		m.width = m1.width;
		
		mb0.boxMetrics.y = 0;
		mb1.boxMetrics.y = m0.height + m0.edgeS;
		m.baseline = m0.height + m0.edgeS + (Jexfont.getHeight(size)/2);
		m.height = mb1.boxMetrics.y + m1.height;

		m.charHeight = m0.baseline;
		m.charHeight0 = m.charHeight;
		m.charHeight1 = m.charHeight;

		boxMetrics = m;
		findEdges(size);
		return m;
	}
}

class MatrixBox extends LayoutBox{
	
	public int rows;
	public int cols;
        public LinkedList halign = new LinkedList();
        public double spacef = 1.0;
        public double leadf = 1.0;
        public String valign = "b";
        public String defHalign = "l";
        public String texType = "array";
        
        private void setHalign(){
            for(int jj = 0; jj < cols; jj++) halign.add(defHalign);
        }

	public void setHalign(String x){
		halign = new LinkedList();
		for(int jj = 0; jj < x.length();jj++){
		    defHalign = x.substring(jj, jj + 1);
                    halign.add(defHalign);
		}
		for(int jj = x.length(); jj < cols; jj++) halign.add(defHalign);
	}
        
        public MatrixBox(){}

	public MatrixBox(int i, int j){
		rows = i;
		cols = j;
		for(int ii = 0; ii < rows; ii++){
			for(int jj = 0; jj < cols; jj++){
				addChild(wrapBox(new EmptyBox()));
			}
		}
                setHalign();
	}

	public MatrixBox(RowBox mb0, RowBox mb1, RowBox mb2, RowBox mb3){
		rows = 2;
		cols = 2;
		halign.add("r");
		halign.add("l");
		addChild(mb0);
		addChild(mb1);
		addChild(mb2);
		addChild(mb3);
		texType = "align";
                spacef = 0.0;
                leadf = 0.0;
	}

	public void findMatrix(int i){
		int j = te.dc.sel.getInt(i);
		j = j - 1;
		te.dc.matrixRow = j/cols;
		te.dc.matrixCol = j%cols;
		super.findMatrix(i);
	}

	public void addEndRow(){
		rows++;
		for(int jj = 0; jj < cols; jj++) addChild(wrapBox(new EmptyBox()));
	}

	public void deleteRowAt(int row){
		if(row > rows) return;
		if(rows == 1) return;
		LinkedList nc = new LinkedList();
		int pos = 0;
		for(int ii = 0; ii < row; ii++){
			for(int jj = 0; jj < cols; jj++){
				nc.add(c.get(pos));
				pos++;
			}
		}
		pos = pos + cols;
		for(int ii = row + 1; ii < rows; ii++){
			for(int jj = 0; jj < cols; jj++){
				nc.add(c.get(pos));
				pos++;
			}
		}		
		rows--;
		c = nc;	
	}

	public void deleteColAt(int col){
		if(col > cols) return;
		if(cols == 1) return;
		LinkedList nc = new LinkedList();
		int pos = 0;
		for(int ii = 0; ii < rows; ii++){
			for(int jj = 0; jj < col; jj++){
				nc.add(c.get(pos));
				pos++;
			}
			pos++;
			for(int jj = col + 1; jj < cols; jj++){
				nc.add(c.get(pos));
				pos++;
			}
		}	
		cols--;
		halign.removeLast();
		c = nc;	
	}

	public void insertColAt(int col){
		if(col > cols) col = cols;
		LinkedList nc = new LinkedList();
		int pos = 0;
		for(int ii = 0; ii < rows; ii++){
			for(int jj = 0; jj < col; jj++){
				nc.add(c.get(pos));
				pos++;
			}
			nc.add(wrapBox(new EmptyBox()));
			for(int jj = col; jj < cols; jj++){
				nc.add(c.get(pos));
				pos++;
			}
		}
		halign.add(halign.getLast());
		cols++;
		c = nc;	
	}

	public void insertRowAt(int row){
		if(row > rows) addEndRow();
		LinkedList nc = new LinkedList();
		int pos = 0;
		for(int ii = 0; ii < row; ii++){
			for(int jj = 0; jj < cols; jj++){
				nc.add(c.get(pos));
				pos++;
			}
		}
		for(int jj = 0; jj < cols; jj++){
			nc.add(wrapBox(new EmptyBox()));
		}
		for(int ii = row; ii < rows; ii++){
			for(int jj = 0; jj < cols; jj++){
				nc.add(c.get(pos));
				pos++;
			}
		}		
		rows++;
		c = nc;	
	}		

	public boolean isAlign(){
		if(texType.equals("align")) return true;
		return false;
	}

	public boolean isMatrix(){return true;}

	public void validate(){
		nvalidate(rows * cols);
		for(int jj = halign.size(); jj < cols; jj++) halign.add(defHalign);
	}
        
        public String toTex(){
            String x = "\\begin{" + texType + "} ";
            if(texType.equals("array")) x = x + "[" + valign + "] ";
            if(texType.equals("array")){
                x = x + "{";
                for(int ii = 0; ii < cols; ii++) x = x + halign.get(ii);
                x = x + "} ";
            }
            for(int ii = 0; ii < rows; ii++){
                for(int jj = 0; jj < cols; jj++){
                    MathBox mb = (MathBox) c.get((ii * cols) + jj);
                    x = x + mb.toTex();
                    if(jj < cols - 1) x = x + " &  ";
                }
                if(ii < rows - 1) x = x + " \\\\ ";
            }
            x = x + "\\end{" + texType + "}";
            return x;
        }

        public String toMML(){
	    String alg = "";
	    if(valign.equals("c")) alg = "rowalign=\"center\"";
	    alg = alg + " colalign=\"";
	    for(int ii = 0; ii < cols; ii++) {
		String h = (String) halign.get(ii);
		if(h.equals("l")) alg = alg + "left ";
		if(h.equals("c")) alg = alg + "center ";
	 	if(h.equals("r")) alg = alg + "right ";
	    }
	    alg = alg + "\"";
	    if(texType.equals("align")) alg = alg + " columnspacing=\"0.0em\"";
            String x = "<mtable " + alg + ">";
            for(int ii = 0; ii < rows; ii++){
		x = x + "<mtr>";
                for(int jj = 0; jj < cols; jj++){
                    MathBox mb = (MathBox) c.get((ii * cols) + jj);
                    x = x + "<mtd>" + mb.toMML() + "</mtd>";
                }
                x = x + "</mtr>";
            }
            x = x + "</mtable>";
            return x;
        }
        
        private int colct;
        
        public MathBox build(String x, MathBox mb){
            if(x.equals("add")) buildState = "closed"; //problem
            if(buildState.equals("nextItem")){
                if(x.equals("\\end")){
		    if(lang.equals("mml")) {
			rows--;
			System.out.println("hIHI");
		    }
                    buildState = "closed";
                    return this;
                }
                if(x.equals("&")) {
                    colct++;
                    if(colct > cols) ; //add a col do not forget to set halign
                }
                if(x.equals("\\\\")){
                    for(int jj = colct + 1; jj <= cols; jj++){
			addChild(wrapBox(new EmptyBox()));
		    }
                    colct = 1;
                    rows++;
                }
		if(lang.equals("tex") || x.equals("mtr") || x.equals("mtd")){
                	RowBox rb = new RowBox();
                	rb.build("array", this);
                	addChild(rb);
                	return rb;
		}
		else return this;
            }
            if(buildState.equals("halign")){
                if(texType.equals("array")){
                    for(cols = 0; cols < x.length(); cols++){
                    halign.add(x.substring(cols, cols + 1));
                    }
                }
                else{
                    halign.add("r");
		    halign.add("l");
		    cols = 2;
                }
                buildState = "nextItem";
                return build(x, mb);
            }
            if(buildState.equals("valign")){
                if(texType.equals("array")) valign = x;
                else valign = "b";
                buildState = "halign";
            }
            if(buildState.equals("\\begin")){
                texType = x;
                colct = 1;
                rows = 1;
                if(texType.equals("align")) {
                    buildState = "newRow";
                    spacef = 0.0;
                    leadf = 0.0;
                    buildState = "valign";
                }
                if(texType.equals("array")) {
                    spacef = 1.0;
                    leadf = 1.0;
                    buildState = "valign";
                }
            }
            if(x.equals("\\begin")) buildState = x;
	    if(x.equals("mtable")) {
		buildState = "\\begin";
		lang = "mml";
	    }
            return this;
        }
        
    	public MathBox dup(){
		MatrixBox lb = new MatrixBox(rows, cols);
		lb.c = new LinkedList();	
                for (Iterator i = c.iterator(); i.hasNext(); ){
                    MathBox mb = (MathBox) i.next();
                    lb.c.add(mb.dup());
                }
                for (Iterator i = halign.iterator(); i.hasNext();) lb.halign.add(i.next());
                lb.spacef = spacef;
                lb.leadf = leadf;
                lb.valign = valign;
                lb.defHalign = defHalign;
                lb.texType = texType;
		return lb;
	}


	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		MathBox[] mb = new MathBox[rows * cols];
		BoxMetrics[] mm = new BoxMetrics[rows * cols];
		int ii = 0;
		int maxHeight = 0;
		int maxBase = 0;
		int maxWidth = 0;
		int space = (int) Math.ceil(spacef * Jexfont.getWidth(size));
		int lead = (int) Math.ceil(leadf * Jexfont.getLeading(size));
                
                int rowHeight[] = new int[rows];
		int rowBase[] = new int[rows];
                int colWidth[] = new int[cols];
                
                for(int jj = 0; jj < rows; jj++) rowHeight[jj] = 0;
                for(int jj = 0; jj < rows; jj++) rowBase[jj] = 0;
                for(int jj = 0; jj < cols; jj++) colWidth[jj] = 0;
                
                int ri = 0;
                int ci = 0;
                
		for (Iterator i = c.iterator(); i.hasNext(); ){
			mb[ii] = (MathBox) i.next();
			mm[ii] = mb[ii].makeMetrics(size, stretch);
			if(mm[ii].height >= maxHeight) maxHeight = mm[ii].height;
                        if(mm[ii].height >= rowHeight[ri]) rowHeight[ri] = mm[ii].height;
			if(mm[ii].baseline >= maxBase) maxBase = mm[ii].baseline;
                        if(mm[ii].baseline >= rowBase[ri]) rowBase[ri] = mm[ii].baseline;
			if(mm[ii].width >= maxWidth) maxWidth = mm[ii].width;
                        if(mm[ii].width >= colWidth[ci]) colWidth[ci] = mm[ii].width;
			ii++;
                        ci++;
                        if(ci >= cols) {
                            ri++;
                            ci = 0;
                        }
		}
                
                //for fixed height/width set rowHeight, ColWidth to max values
                
                m.width = 0;
                for(int jj = 0; jj < cols; jj++) m.width = m.width + colWidth[jj];
                
                m.height = 0;
                for(int jj = 0; jj < rows; jj++) m.height = m.height + rowHeight[jj];
                
                m.width = m.width + (cols - 1) * space;
		m.height = m.height + (rows - 1) * lead;
                
		int x = 0;
		int y = 0;
		for(ii = 0; ii < rows; ii++){
			x = 0;
			for(int jj = 0; jj < cols; jj++){
				int kk = (ii * cols) + jj;
                                int rightshift = 0;
                                String hal = (String) halign.get(jj);
                                if(hal.equals("c")) rightshift = (int) Math.ceil((colWidth[jj] - mm[kk].width)/2);
                                if(hal.equals("r")) rightshift = (int) Math.ceil(colWidth[jj] - mm[kk].width);
				mm[kk].x = x + rightshift;
                                int downshift = 0;
                                if(valign.equals("c")) downshift = (int)Math.ceil((rowHeight[ii] - mm[kk].height)/2);
                                if(valign.equals("b")) downshift = (int)Math.ceil(rowBase[ii] - mm[kk].baseline);
				mm[kk].y = y + downshift;
				x += colWidth[jj] + space;
			}
			y += rowHeight[ii] + lead;
		}
	
		m.slant0 = 0;
		m.slant1 = 0;



		m.baseline = (int) Math.ceil((m.height + Jexfont.getHeight(size))/2);

		m.charHeight = m.baseline;
		m.charHeight0 = m.charHeight;
		m.charHeight1 = m.charHeight;

		boxMetrics = m;
		findEdges(size);
		return m;
	}
}

class CharBox extends MathBox{

	float flux = (float) 0.3;

	private Font f;
	private Color c;
	private float thin;
	private Jexfont jf;
        private int lpad;
        private String jfstyle = "math";
	private int vshift;
        
        private int size;
        private float stretch;
	private float ratio;
	
	public void setFont(String face, int s, Color c){
		if(c != null) this.c = c;
	}
	
	public CharBox(int token){
		jf = new Jexfont(token);
		c = DefaultContext.defFontColor;
		if(te != null) {
			if(te.dc !=null) c = te.dc.fontColor;
		}
	}

	public CharBox(){}

	public void passProp(String prop, int val){
		if(jf == null) return;
		if(prop.equals("rowPos"))jf.setBinary(val);
	}
	
	public MathBox dup(){
		CharBox cb = new CharBox(jf.unichar);
                cb.setStyle(jfstyle);
                return cb;
	}
        
        public String toTex(){
	    jf.JexStyle(jfstyle);
            return jf.texString();
        }

        public String toMML(){
	    jf.JexStyle(jfstyle);
            return jf.MMLString();
        }	
	
	public void setColor(Color color){
		c = color;
	}
        
        public void setStyle(String style){
            jfstyle = style;
	    jf.JexStyle(jfstyle);
        }
        
        public boolean isMathrm(){
            if(jfstyle.equals("mathrm")) return true;
            return false;
        }

	private void getGlyphRect(Graphics2D g, BoxMetrics m){
		char[] tt = new char[1];
		tt[0] = (char) jf.aschar;
		FontRenderContext frc = g.getFontRenderContext();
		GlyphVector v = f.createGlyphVector(frc, tt);
		GlyphMetrics gm = v.getGlyphMetrics(0);
		Rectangle2D r = gm.getBounds2D();
		m.charHeight = (int) Math.ceil(-r.getY()*1.2);
		float w = (float) r.getWidth();
		float lsb = (float) gm.getLSB();
		m.edgeE = ((int) Math.ceil(w + lsb)) - m.width;
		m.edgeW = (int) Math.ceil(-lsb);
		m.edgeS = (int) Math.ceil(r.getHeight() - m.charHeight);
		m.charHeight0 = (int) r.getWidth();
	}            
	
	public BoxMetrics makeMetrics(int size, float stretch){
		jf.setBinding(te.dc.binding);
            	jf.JexStyle(jfstyle);
                this.size = size;
                this.stretch = stretch;
		thin = -1;
		BoxMetrics m = new BoxMetrics();
		boxMetrics = m;
		f = new Font(jf.face, jf.style, size);
		int fdescent = 0;
		FontMetrics fm;
                lpad = (int)(Jexfont.getWidth(size) * jf.getLpad());
		if(stretch > 0){
			getGlyphRect(te.dc.g, m);
			double w = (double) m.charHeight0;
			te.dc.g.setFont(f);
			fm = te.dc.g.getFontMetrics();
			double w0 = fm.charWidth(jf.aschar) + lpad + (Jexfont.getWidth(size) * jf.rpad);
			double ts = 0.9 * stretch * (float) w0 / (float) w;
			if(jf.getCenter() >= 0){
				lpad = (int) (0.1 * Jexfont.getWidth(size)) + (int) (ts * m.edgeW);
				f = f.deriveFont(new AffineTransform(ts, 0, 0, 1.0, 0, 0));
			}
			else {
				lpad = (int) ((stretch * w0/2) - w);
			}
			m.slant0 = 0;
			m.slant1 = 0;
			ratio = stretch;
			this.stretch = (float)(jf.getPrintWidth() * fm.charWidth(jf.aschar) / w);
			thin = (float) w;
			m.width = (int) (stretch * w0);	
			m.edgeW = 0;
			m.edgeE = 0;
			m.edgeS = 0;
			m.edgeN = 0;	
			m.height = Jexfont.getThinWidth(size)/2;
			vshift = fm.getAscent() - m.height;
			m.baseline = m.height;
			m.charHeight0 = m.charHeight;
			m.charHeight1 = m.charHeight;
			return m;
		}
		else {
			m.slant0 = this.f.getItalicAngle() - jf.aftr + jf.fixangle; 
			if(jf.aftr != 0) f = f.deriveFont(new AffineTransform(1,0,jf.aftr,1,0,0));
			if(stretch < 0){
				te.dc.g.setFont(f);
				fm = te.dc.g.getFontMetrics();
				fdescent = fm.getDescent();
				f = f.deriveFont(new AffineTransform(1.0, 0, 0, -stretch, 0, 0));
				m.slant0 = 0;
			}
		}
		te.dc.g.setFont(f);
		fm = te.dc.g.getFontMetrics();
		m.slant1 = m.slant0;
		m.width = fm.charWidth(jf.aschar);
		m.height = fm.getAscent(); 
		vshift = 0;
		getGlyphRect(te.dc.g, m);
		m.charHeight0 = m.charHeight;
		m.charHeight1 = m.charHeight;
		m.edgeN = Math.max(0, m.charHeight - m.height);
                m.height = m.height + (int) Math.ceil(jf.hpad * Jexfont.getHeight(size)); 
                if(stretch < 0) {
			//m.baseline = m.height - fm.getMaxDescent() + fdescent;
			m.baseline = m.height - (int)(1.3f * (-stretch - 1) * fdescent);
			vshift = m.height - m.baseline;
			vshift = (int)(4.9f * (-stretch - 2.1) * fdescent);
		}
		else m.baseline = m.height;
                m.width = m.width + lpad + (int)(Jexfont.getWidth(size) * jf.rpad);
		return m;
	};
	
	public void display(int x, int y){

		char[] tt = new char[1];
		tt[0] = (char) jf.aschar;
		String str = new String(tt);

		double bump = 0;
		float aftr = jf.aftr;
		int oldEdgeW = boxMetrics.edgeW;
		int oldWidth = boxMetrics.width;
		if(thin > 0) {
			bump = -boxMetrics.baseline + Math.abs(jf.getCenter()) * boxMetrics.charHeight;
			aftr = thin;
			boxMetrics.edgeW = lpad;
			if(jf.getCenter() < 0) boxMetrics.width = 0;
		}
		bump = bump + y + boxMetrics.baseline + (boxMetrics.height * jf.vpad);
                if(te.dc.printeq){
		    int s = (int)Math.ceil(jf.rescale * size);
		    if(stretch > 0) stretch = stretch * jf.getStretchScale();
                    te.dc.md.showChar(str, jf.face, jf.styleName(), s, ratio, stretch, aftr, x + lpad, bump, boxMetrics, vshift);
                }
                else{
                    te.dc.g.setColor(c);
                    if(isHighlight) te.dc.g.setColor(te.dc.highColor);
                    te.dc.g.setFont(f);               
                    te.dc.g.drawString(str, x + lpad, (int) Math.ceil(bump));
                }
		boxMetrics.edgeW = oldEdgeW;
		boxMetrics.width = oldWidth;
	}
	
	public int findCaret(int bx, int by, int x, int y){
		bx += boxMetrics.x;
		by += boxMetrics.y;
		if(!hitTest(bx, by, x, y)) return 0;
		return -1;
	}
}

class SpaceBox extends CharBox{
        
        public double sbwidth = 1.0;
        private String texString = "\\,";
        private String jfstyle = "math";
        
	private Color c;
	private Jexfont jf;
        
        public SpaceBox(String texString){
            this.texString = texString;
            if(texString.equals("\\,")) sbwidth = 0.5;
            if(texString.equals("\\:")) sbwidth = 1.0;
            if(texString.equals("\\;")) sbwidth = 1.5;
            jf = new Jexfont(32);
        }
	
	public MathBox dup(){
                SpaceBox newsb = new SpaceBox(texString);
                newsb.sbwidth = sbwidth;
                newsb.setStyle(jfstyle);
		return newsb;
	}
        
        public String toTex(){
            return texString + " ";
        }

        public String toMML(){
            return "<mspace width=\"" + sbwidth + "em\"/>";
        }
        
	public void setFont(String face, int s, Color c){
		if(c != null) this.c = c;
	}
        
        public void setColor(Color color){
		c = color;
	}
        
        public void setStyle(String style){
            jf.JexStyle(style);
            jfstyle = style;
        }
        
        public boolean isMathrm(){
            if(jfstyle.equals("mathrm")) return true;
            return false;
        }
        
	public BoxMetrics makeMetrics(int size, float stretch){
		BoxMetrics m = new BoxMetrics();
		Font f = new Font(jf.face, jf.style, size);
		m.slant0 = f.getItalicAngle() - jf.aftr; 
                m.slant1 = m.slant0;

                te.dc.g.setFont(f);
		FontMetrics fm = te.dc.g.getFontMetrics();
		m.width = (int) Math.ceil(sbwidth*fm.charWidth(32));
		m.height = fm.getAscent();
		m.charHeight = m.height;
		m.charHeight0 = m.height;
		m.charHeight1 = m.height;
                m.baseline = m.height;
		boxMetrics = m;
		return m;
	}
        
	public void display(int x, int y){
		//te.dc.g.setColor(c);
	}
        
	public int findCaret(int bx, int by, int x, int y){
		bx += boxMetrics.x;
		by += boxMetrics.y;
		if(!hitTest(bx, by, x, y)) return 0;
		return -1;
	}
}

class EmptyBox extends MathBox{

	public boolean isEmpty() {return true;};

	private Color c = Color.lightGray;
        private String signal;
	private float stretch;
        
        public EmptyBox(){}
        public EmptyBox(String signal){
            this.signal = signal;
        }
	
	public MathBox dup(){
		return new EmptyBox();
	}
        
        public String toTex(){
            return "{}";
        }

	public String toMML(){
		return "<mi></mi>";
	}
	
	public BoxMetrics makeMetrics(int size, float stretch){
		this.stretch = stretch;
		BoxMetrics m = new BoxMetrics();
		m.slant0 = 0; 
		m.slant1 = 0;
		m.height = Jexfont.getHeight(size);
		m.charHeight = m.height;
		m.charHeight0 = m.height;
		m.charHeight1 = m.height;
		m.width = Jexfont.getWidth(size);
		if(stretch < 0) {
			m.width = Jexfont.getThinWidth(size);
			m.height = (int) Math.ceil(-stretch * m.height);
		}
		if(stretch > 0){
			m.height = Jexfont.getThinWidth(size)/2;
			m.width = (int) Math.ceil(stretch * m.width);
		}
		m.baseline = m.height;
                if(!te.dc.showEmpty) m.width = 0;
		boxMetrics = m;
		return m;
	}
	
	public void setFont(String f, int s, Color c){
		if(c != null) this.c = c;
	}
	
	public void display(int x, int y){
		te.dc.g.setColor(c);
		te.dc.emptyPad = (float) 0.2;
		int pad = (int) (te.dc.emptyPad * boxMetrics.width);
		if(stretch > 0) pad = (int) (te.dc.emptyPad * boxMetrics.height);
                if(te.dc.showEmpty){
                    te.dc.g.draw(new RoundRectangle2D.Double(x + pad, y + pad, boxMetrics.width - 2*pad, boxMetrics.height - pad, pad, pad));
                }
	}
	
	public int findCaret(int bx, int by, int x, int y){
		bx += boxMetrics.x;
		by += boxMetrics.y;
		if(!hitTest(bx, by, x, y)) return 0;
		return -1;
	}
}

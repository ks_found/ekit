package jex;

import java.awt.*;
import java.util.*;

import jex.Jexmath;

public class Jextex{

        static private Hashtable texDic;
        static private Hashtable dicTex;
        static private Hashtable tokens;
        static private Hashtable pairs;
        static private Hashtable h;

	static public String texMacro(String x){
            String y = (String) texDic.get(x);
            if(y != null) {
                y = y + " ";
                return y;
            }
            return x;
        }
        
        static public String tokenReplace(String x){
            return (String) dicTex.get(x);
        }
        
        static public String tokenType(String x){
            return (String) tokens.get(x);
        }
        
        static public String getLeft(String x){
            return (String) pairs.get(x);
        }
        
        static public Integer getType(String x){
            return (Integer) h.get(x);
        }
        
        static public void init(){
        
        tokens = new Hashtable();
        
        tokens.put("\\leftgrp", "FenceBox");
        tokens.put("\\overset", "OverBox");
        tokens.put("\\underset", "UnderBox");
        //tokens.put("\\unicode", "CharBox");
        tokens.put("\\root", "RootBox");
        tokens.put("\\root[", "[RootBox");
        tokens.put("{", "RowBox");
        tokens.put("^", "operator");
        tokens.put("_", "operator");
        tokens.put("\\overgrp", "operator");
	tokens.put("\\frac", "DivisionBox");
        tokens.put("\\rightgrp", "closer");
        tokens.put("}", "closer");
        tokens.put("&", "closer");
        tokens.put("\\\\", "closer");
        tokens.put("\\begin", "begin");
        tokens.put("\\end", "end");
        //tokens.put("]", "closer");
        tokens.put("\\unicode", "unichar");
        tokens.put("\\mathrm", "RowBox");
        tokens.put("\\operatorname", "RowBox");
        tokens.put("\\,", "space");
        tokens.put("\\:", "space");
        tokens.put("\\;", "space");
        
        pairs = new Hashtable();
        
        pairs.put("}", "{");
        pairs.put("\\rightgrp", "\\leftgrp");
        pairs.put("]", "[");
        pairs.put("&", "array");
        pairs.put("\\\\", "array");
        pairs.put("\\end", "array");
        
        h = new Hashtable();
        
        h.put("\\", new Integer(2));
        h.put("{", new Integer(1));
        h.put("}", new Integer(1));
        h.put(" ", new Integer(0));
        h.put("\r", new Integer(0));
        h.put("\n", new Integer(0));
        h.put("%", new Integer(0));
        h.put("_", new Integer(1));
        h.put("^", new Integer(1));
        h.put("~", new Integer(1));
        h.put("[", new Integer(3));
        h.put("]", new Integer(3));
        
        texDic = new Hashtable();
        dicTex = new Hashtable();
        
	texDic.put("*", "\\ast");
	texDic.put("'", "\\prime");
        texDic.put("\\unicode{3b1}", "\\alpha");
        texDic.put("\\unicode{3b2}", "\\beta");
        texDic.put("\\unicode{3c8}", "\\psi");
        texDic.put("\\unicode{3b4}", "\\delta");
        texDic.put("\\unicode{3b5}", "\\epsilon");
        texDic.put("\\unicode{3d5}", "\\phi");
        texDic.put("\\unicode{3b3}", "\\gamma");
        texDic.put("\\unicode{3b7}", "\\eta");
        texDic.put("\\unicode{3b9}", "\\iota");
        texDic.put("\\unicode{3c6}", "\\varphi");
        texDic.put("\\unicode{3ba}", "\\kappa");
        texDic.put("\\unicode{3bb}", "\\lambda");
        texDic.put("\\unicode{3bc}", "\\mu");
        texDic.put("\\unicode{3bd}", "\\nu");
        texDic.put("\\unicode{3bf}", "\\omicron");
        texDic.put("\\unicode{3c0}", "\\pi");
        texDic.put("\\unicode{3c7}", "\\chi");
        texDic.put("\\unicode{3c1}", "\\rho");
        texDic.put("\\unicode{3c3}", "\\sigma");
        texDic.put("\\unicode{3c4}", "\\tau");
        texDic.put("\\unicode{3c5}", "\\upsilon");
        texDic.put("\\unicode{3c9}", "\\omega");
        texDic.put("\\unicode{3be}", "\\xi");
        texDic.put("\\unicode{3b8}", "\\theta");
        texDic.put("\\unicode{3b6}", "\\zeta");  
        
        texDic.put("\\unicode{39e}", "\\Xi");
        texDic.put("\\unicode{3a8}", "\\Psi");
        texDic.put("\\unicode{394}", "\\Delta");
        texDic.put("\\unicode{393}", "\\Gamma");
        texDic.put("\\unicode{3a6}", "\\Phi");
        texDic.put("\\unicode{39b}", "\\Lambda");
        texDic.put("\\unicode{3a0}", "\\Pi");
        texDic.put("\\unicode{3a3}", "\\Sigma");
        texDic.put("\\unicode{3a5}", "\\Upsilon");
        texDic.put("\\unicode{3a9}", "\\Omega");
        texDic.put("\\unicode{398}", "\\Theta");

        texDic.put("\\operatorname{tanh}", "\\tanh");
        texDic.put("\\operatorname{tan}", "\\tan");
        texDic.put("\\operatorname{sup}", "\\sup");
        texDic.put("\\operatorname{sinh}", "\\sinh");
        texDic.put("\\operatorname{sin}", "\\sin");
        texDic.put("\\operatorname{sec}", "\\sec");
        texDic.put("\\operatorname{min}", "\\min");
        texDic.put("\\operatorname{max}", "\\max");
        texDic.put("\\operatorname{ln}", "\\ln");
        texDic.put("\\operatorname{limsup}", "\\limsup");
        texDic.put("\\operatorname{liminf}", "\\liminf");
        texDic.put("\\operatorname{lim}", "\\lim");
        texDic.put("\\operatorname{lg}", "\\lg");
        texDic.put("\\operatorname{ker}", "\\ker");
        texDic.put("\\operatorname{inf}", "\\inf");
        texDic.put("\\operatorname{hom}", "\\hom");
        texDic.put("\\operatorname{gcd}", "\\gcd");
        texDic.put("\\operatorname{exp}", "\\exp");
        texDic.put("\\operatorname{dim}", "\\dim");
        texDic.put("\\operatorname{det}", "\\det");
        texDic.put("\\operatorname{deg}", "\\deg");
        texDic.put("\\operatorname{csc}", "\\csc");
        texDic.put("\\operatorname{cot}", "\\cot");
        texDic.put("\\operatorname{coth}", "\\coth");
        texDic.put("\\operatorname{cos}", "\\cos");
        texDic.put("\\operatorname{cosh}", "\\cosh");
        texDic.put("\\operatorname{arctan}", "\\arctan");
        texDic.put("\\operatorname{arcsin}", "\\arcsin");
        texDic.put("\\operatorname{arccos}", "\\arccos");
	texDic.put("\\operatorname{log}", "\\log");
       
        texDic.put("\\unicode{222b}", "\\int");
        texDic.put("\\unicode{2211}", "\\sum");
        texDic.put("\\unicode{220f}", "\\prod");
        texDic.put("\\unicode{22c5}", "\\cdot");
        texDic.put("\\unicode{b7}", "\\bullet");
        texDic.put("\\unicode{b1}", "\\pm");
        texDic.put("\\unicode{2208}", "\\in");
        texDic.put("\\unicode{220b}", "\\ni");
        texDic.put("\\unicode{2209}", "\\notin");
        texDic.put("\\unicode{2264}", "\\leq");
        texDic.put("\\unicode{2265}", "\\geq");
        texDic.put("\\unicode{2260}", "\\neq");
        texDic.put("\\unicode{2282}", "\\subset");
        texDic.put("\\unicode{2286}", "\\subseteq");
        texDic.put("\\unicode{2283}", "\\supset");
        texDic.put("\\unicode{2287}", "\\supseteq");
        texDic.put("\\unicode{2229}", "\\cap");
        texDic.put("\\unicode{222a}", "\\cup");
        texDic.put("\\unicode{2192}", "\\rightarrow");
        texDic.put("\\unicode{2190}", "\\leftarrow");
        texDic.put("\\unicode{2207}", "\\nabla");
        texDic.put("\\unicode{2202}", "\\partial");
        texDic.put("\\unicode{221e}", "\\infty");
        texDic.put("\\unicode{2205}", "\\emptyset");
        texDic.put("\\unicode{211c}", "\\Re");
        texDic.put("\\unicode{2026}", "\\ldots");
        texDic.put("\\unicode{d7}", "\\times");
        texDic.put("\\unicode{221d}", "\\propto");
        texDic.put("\\unicode{2261}", "\\equiv");
        texDic.put("\\unicode{2248}", "\\approx");
	texDic.put("\\unicode{2112}", "\\Ell");
	texDic.put("\\unicode{2113}", "\\ell");
	texDic.put("\\unicode{e090}", "\\|");
        
        texDic.put("\\unicode{7b}","\\{");
        texDic.put("\\unicode{7d}","\\}");
        texDic.put("\\unicode{5e}","\\^");
        texDic.put("\\unicode{5f}","\\_");
        texDic.put("\\unicode{7e}","\\~");
        texDic.put("\\unicode{26}", "\\&");
        texDic.put("\\unicode{6c}", "\backslash");
        texDic.put("\\unicode{25}", "\\%");
        texDic.put("\\unicode{24}", "\\$");
        texDic.put("\\unicode{23}", "\\#");
        
        texDic.put("\\leftgrp{[}", "\\left[");
        texDic.put("\\rightgrp{]}", "\\right]");
        
        texDic.put("\\overgrp{-}", "\\over");
        
        texDic.put("\\overset{-}", "\\overline");
        texDic.put("\\overset{\\^}", "\\widehat");
        texDic.put("\\overset{\\~}", "\\widetilde");
        //tilde should be unicode rather than backslash tilde
        //backslash tilde apparently means put a tilde over the next letter
        texDic.put("\\overset{\\unicode{2190}}", "\\overleftarrow");
        texDic.put("\\overset{\\unicode{2192}}", "\\overrightarrow");

        texDic.put("\\underset{-}", "\\underline");
        texDic.put("\\underset{\\unicode{2190}}", "\\underleftarrow");
        texDic.put("\\underset{\\unicode{2192}}", "\\underrightarrow");
        
        for(Enumeration e = texDic.keys(); e.hasMoreElements();){
            Object ob = e.nextElement();
            dicTex.put(texDic.get(ob), ob);
        }

        texDic.put("\\leftgrp{(}", "\\left(");
        texDic.put("\\leftgrp{\\{}", "\\left\\{");
        texDic.put("\\leftgrp.", "\\left.");
        texDic.put("\\rightgrp{)}", "\\right)");
        texDic.put("\\rightgrp{\\}}", "\\right\\}");
        texDic.put("\\rightgrp.", "\\right.");
	texDic.put("\\leftgrp{{}}", "\\left.");
	texDic.put("\\rightgrp{{}}", "\\right.");
        
        dicTex.put("\\left", "\\leftgrp");
        dicTex.put("\\right", "\\rightgrp");
	dicTex.put("\\vert", "|");
	dicTex.put("\\Vert", "\\|");

//    * \hat{o} is similar to the circumflex (cf. \^)
//    * \widehat{oo} is a wide version of \hat over several letters
//    * \check{o} is a vee or check (cf. \v)
//    * \tilde{o} is a tilde (cf. \~)
//    * \widetilde{oo} is a wide version of \tilde over several letters
//    * \acute{o} is an acute accent (cf. \`)
//    * \grave{o} is a grave accent (cf. >\')
//    * \dot{o} is a dot over the letter (cf. \.)
//    * \ddot{o} is a double dot over the letter
//    * \breve{o} is a breve
//    * \bar{o} is a macron (cf. \=)
//    * \vec{o} is a vector (arrow) over the letter
//    * \mp minus or plus sign
//    * \div divided by sign
//    * \ast an asterisk (centered)
//    * \star a five-point star (centered)
//    * \circ an open bullet
//    * \ll much less than
//    * \gg much greater than
//    * \sim similar to
//    * \simeq similar or equal to
//    * \per "perpendicular to" symbols
//    * \langle (left angle bracket)
//    * \rangle (right angle bracket)
//    * uparrow (uparrow)
//    * downarrow (down arrow)
//    * updownarrow (up/down arrow)
//    * \cdots horizontally center of line (math mode only)
//    * \ddots diagonal (math mode only)
//    * \vdots vertical (math mode only)
//    * \oint a surface (circular) integral sign
//    * \bigcup big "U"
//    * \bigcap big inverted "U"
//    * \bigvee big "V"
//    * \bigwedge big inverted "V"
//    * \bigodot big "O" with dot at center
//    * \bigotimes big "O" with cross inside
//    * \bigoplus big "O" with a + inside
//    * \biguplus big "U" with a + inside
//    * \varepsilon (variation, script-like)
//    * \vartheta (variation, script-like)
//    * \varpi (variation)
//    * \varrho (variation, with the tail)
//    * \varsigma (variation, script-like)
//    * \aleph Hebrew aleph
//    * \hbar h-bar, Planck's constant
//    * \imath variation on i; no dot
//    * \jmath variation on j; no dot
//    * \wp fancy script lowercase P
//    * \Im script capital I (Imaginary)
//    * \prime prime (also obtained by typing ')
//    * \surd radical (square root) symbol
//    * \angle angle symbol
//    * \forall for all (inverted A)
//    * \exists exists (left-facing E)
//    * \triangle open triangle symbol
//    * \Box open square
//    * \Diamond open diamond
//    * \flat music: flat symbol
//    * \natural music: natural symbol
//    * \clubsuit playing cards: club suit symbol
//    * \diamondsuit playing cards: diamond suit symbol
//    * \heartsuit playing cards: heart suit symbol
//    * \spadesuit playing cards: space suit symbol
        
        }
}

class TexParser{

    TokenParser tp;
    Stack stack;
    
    public TexParser(String x){
        tp = new TokenParser(x);
        stack = new Stack();
    }
    
    private void pushClosed(MathBox mb){
        if(stack.isEmpty()) return;
        MathBox sb = (MathBox) stack.pop();
        if(sb.isClosed()){
            MathBox sb2 = (MathBox) stack.pop();
            sb2 = sb2.build("add", sb);
            stack.push(sb2);
            stack.push(mb);
            //System.out.println("add to close: " + sb.toTex());
        }
        else{
            if(sb.isRow() & (!sb.isMathrm())){
                stack.push(sb);
                stack.push(mb);
                //System.out.println("open row push push");
            }
            else{
                sb = sb.build("add", mb);
                if(sb.isClosed()) pushClosed(sb);
                else stack.push(sb);
                //System.out.println("add: " + mb.toTex());
            }
        }
    }
    
    private void pushCloser(String z){
	if(stack.isEmpty()) return;
        MathBox sb = (MathBox) stack.pop();
        if(sb.isClosed()){
            MathBox sb2 = (MathBox) stack.pop();
            sb = sb2.build("add", sb);
        }
	MathBox sbd = sb;
        sb = sb.build(z, null);
	if(sb == null) tp.diagnose("Cannot build " + sbd.getClass() + " using " + z);
	else{
        	if(sb.isClosed()) pushClosed(sb);
        	else stack.push(sb);
	}
    }
    
    private void pushOperator(String z){
        if(stack.isEmpty()) return;
        MathBox sb = (MathBox) stack.pop();
        if(sb.isClosed()){
            sb = sb.build(z, null);
            stack.push(sb);
        }
        else{
            stack.push(sb);
            MathBox rn = sb.wrapBox(new EmptyBox());
            rn = rn.build("close", null);
            pushClosed(rn);
            pushOperator(z);
        }
    }
    
    public RowBox parse(){
        RowBox rb = new RowBox();
        rb.build("open", null);
        stack.push(rb);
        
        String z;
        boolean iter = true;
        while(iter){
            z = tp.nextToken();
            if(z != "") {
                String ttype = Jextex.tokenType(z);
                if(ttype != null){
                    if((!ttype.equals("operator")) & (!ttype.equals("closer"))){
                        MathBox mb = null;
                        if(ttype.equals("FenceBox")) mb = new FenceBox();
                        if(ttype.equals("OverBox")) mb = new OverBox();
                        if(ttype.equals("UnderBox")) mb = new UnderBox();
                        if(ttype.equals("RootBox")) mb = new RootBox();
                        if(ttype.equals("RowBox")) mb = new RowBox();
                        if(mb != null) {
                            mb = mb.build(z, null);
                            stack.push(mb);
                        }
                        else {
                            if(ttype.equals("unichar")){
                                int zz = tp.uniParse();
                                //System.out.println("uniparse: " + zz);
                                if(zz >= 0){
                                    mb = rb.wrapBox(new CharBox(zz));
                                    mb = mb.build("close", null);
                                    pushClosed(mb);
                                }
                            }
                            else if(ttype.equals("space")){
                                mb = rb.wrapBox(new SpaceBox(z));
                                mb = mb.build("close", null);
                                pushClosed(mb);
			    }
			    else if(ttype.equals("DivisionBox")){
				mb = new DivisionBox();
				mb = mb.build("frac", null);
				stack.push(mb);
                            }
                        }
                        

                    }
                    if(ttype.equals("closer")) pushCloser(z);
                    if(ttype.equals("operator")) pushOperator(z);
                    if(ttype.equals("begin")){
                        String[] r = tp.beginParse(true);
                        MathBox mb = new MatrixBox();
                        mb.build("\\begin", null);
                        mb.build(r[0], null);
                        mb.build(r[1], null);
                        stack.push(mb.build(r[2], null));
                    }
                    if(ttype.equals("end")){
                        tp.beginParse(false);
                        pushCloser(z);
                    }
                }
                if((ttype == null) & (z.length() == 1)){
                    char zz = z.charAt(0);
                    MathBox rn = rb.wrapBox(new CharBox(zz));
                    rn = rn.build("close", null);
                    pushClosed(rn);
                }
            }
            else {
                pushClosed(new EmptyBox());
                iter = false;
            }
        }
        rb.validate();
        return rb;
    }
}

class TokenParser{
	int pos = 0;
	String x;
	String zx;
	public TokenParser(String x){
		this.x = x;
	}

	public void diagnose(String estr){
		System.out.println(estr);
		System.out.println(zx);
	}
        
        public int uniParse(){
            pos = x.indexOf('}');
            if(pos < 0) return -1;
            String a = x.substring(0,pos);
            x=x.substring(pos + 1);
            pos = 0;
            int n = a.indexOf('{');
            if(n >= 0) a = a.substring(n + 1);
            a = a.trim();
            if(a.length() == 0) return -1;
            return Integer.parseInt(a, 16);   
        }
        
        public String[] beginParse(boolean begin){
            String[] r = new String[3];
            r[2] = "error";
	    r[1] = "error";
	    zx = "";
            pos = x.indexOf("}"); if(pos < 0) {pos = 0; return r;}
            String a = x.substring(0,pos);
            x = x.substring(pos + 1);
            pos = 0;
            if(!begin) {pos = 0; return r;}
            int n = a.indexOf("{"); if(n < 0) return r;
            a = a.substring(n + 1);
            r[0] = a.trim();
	    if(r[0].startsWith("eqnarray")) {
		r[0] = "array";
		r[1] = "b";
		r[2] = "rcl";
		return r;
	    }
	    if(r[0].startsWith("multline")){
		r[0] = "array";
		r[1] = "b";
		r[2] = "l";
		return r;
	    }
	    if(r[0].startsWith("split")){
		r[0] = "array";
		r[1] = "b";
		r[2] = "rl";
		return r;
	    }
	    if(r[0].startsWith("gather")){
		r[0] = "array";
		r[1] = "b";
		r[2] = "c";
		return r;
	    }
	    if(r[0].startsWith("flalign")){
		r[0] = "array";
		r[1] = "b";
		r[2] = "rlrl";
		return r;
	    }
	    if(r[0].equals("align")) return r;
            pos = x.indexOf("]");
            n = x.indexOf("{");
            if((pos < 0) | (pos > n)) r[1] = "c";
            else{
                a = x.substring(0, pos);
                x = x.substring(pos + 1);
                n = a.indexOf("["); if(n < 0) {pos = 0; return r;}
                a = a.substring(n + 1);
                a = a.trim();
                a = a.substring(0,1);
                r[1] = a;
            }
            pos = x.indexOf("}"); if(pos < 0) {pos = 0; return r;}
            a = x.substring(0, pos);
            x = x.substring(pos + 1);
            pos = 0;
            n = a.indexOf("{"); if(n < 0) return r;
            a = a.substring(n + 1);
            a = a.trim(); if(a.length() == 0) return r;
            r[2] = a;
            return r;
        }

	public String nextToken(){
		int state = 0;
		String a = "";
		while(state < 2){
                        if(pos == x.length()) {
                            x = "";
                            pos = 0;
                            return a;
                        }
			String y = x.substring(pos, pos + 1);
			Integer n = Jextex.getType(y);
                        char zz = y.charAt(0);
			if(n == null){
                                if(((zz >= 'a' & zz <= 'z') | (zz >= 'A' & zz <= 'Z')) & (state == 1)){
                                        a = a.concat(y);
                                        pos++;
                                }
                                else {
                                    if((state == 0) | ((state == 1) & (a.length() == 1))){
                                        a = a.concat(y);
                                        pos++;
                                    }
                                    state = 2;
                                }
			}
			else{
                                int nn = n.intValue();
				if(state == 1){
                                        if(nn == 2) nn = 1;
                                        if(nn == 3){
                                            a = a.concat(y);
                                            pos++;
                                        }
                                        if((nn == 1) & (a.length() == 1)){
                                            a = a.concat(y);
                                            pos++;
                                        }
					state = 2;
					if(nn == 0) pos++;
				}
				if(state == 0){
                                        if(nn != 0) a = a.concat(y);
                                        pos++;
					if(nn == 2) state = 1;
					if(nn == 1) state = 2;
                                        if(nn == 3) state = 2;
				}
			}
		}

                x = x.substring(pos);
                pos = 0;
                String zex = Jextex.tokenReplace(a);
                if(zex != null){
                    x = zex + " " + x;
                    a = nextToken();
                }
		zx = zx + " " + a;
		return a;
	}
}

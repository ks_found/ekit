package jex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import jex.Jex;
import jex.Jexmath;

public class Jexkey extends KeyAdapter{
	
        static private Hashtable uniGrk;
        boolean shift = false;
        boolean ctrl = false;
        boolean alt = false;
        boolean greek = false;
        boolean mathrm = false;
	boolean tex = false;
	String tstring;
        int keyCode;
        int keyChar;
        int SHIFT = 16;
        int CTRL = 17;
        int ALT = 18;
        Jex.JexPane jexPane;
        
        static public int gr(int a){
            Integer b = (Integer) uniGrk.get(new Integer(a));
            if(b == null) return 0;
            return b.intValue();
        }
        
        static private void usg(int a, int b){
            uniGrk.put(new Integer(a), new Integer(b));
        }
        
        Jexkey(Jex.JexPane jexPane){
            this.jexPane = jexPane;
        }
        
        static public void init(){
        
                uniGrk = new Hashtable();
                usg('a', 0x3b1);
                usg('b', 0x3b2);
                usg('c', 0x3c8); //psi
                usg('d', 0x3b4);
                usg('e', 0x3b5);
                usg('f', 0x3d5);
                usg('g', 0x3b3);
                usg('h', 0x3b7); //eta
                usg('i', 0x3b9);
                usg('j', 0x3c6); //varphi
                usg('k', 0x3ba);
                usg('l', 0x3bb);
                usg('m', 0x3bc);
                usg('n', 0x3bd);
                usg('o', 0x3bf);
                usg('p', 0x3c0);
                usg('q', 0x3c7); //chi
                usg('r', 0x3c1);
                usg('s', 0x3c3);
                usg('t', 0x3c4);
                usg('u', 0x3c5);
                usg('w', 0x3c9); //omega
                usg('x', 0x3be);
                usg('y', 0x3b8); //theta
                usg('z', 0x3b6);  
        
            
        }
		
        public void keyPressed(KeyEvent e){
            DefaultContext d = jexPane.d;
	    d.set();
            keyCode = e.getKeyCode();
            if(keyCode == SHIFT) shift = true;
            if(keyCode == CTRL) ctrl = true;
            if(keyCode == ALT) alt = true;
      //      System.out.println("press " + e.getKeyCode());
            if(keyCode == 39) {
                d.advance(1);
                jexPane.repaint();
            }
            else if(keyCode == 37) {
                d.advance(-1);
                jexPane.repaint();
            }
	    else if(keyCode == 27) {
		tex = false;
		jexPane.tex = null;
		jexPane.setStatus();
	    }
        }

	public void interpretTex(int keyChar){
//		System.out.println("interpetTex keyChar: " + keyChar);
		if(keyChar == 32){
            		DefaultContext d = jexPane.d;
	    		d.set();
			tex = false;
			jexPane.tex = null;
			d.insertBox(d.insertBox(MathBox.getTex("{" + tstring + "}")));
			jexPane.repaint();
			return;
		}
		if(keyChar == 8){
			if(tstring.length() > 0) tstring = tstring.substring(0, tstring.length() - 1);
		}
		else {
			char[] tt = new char[1];
			tt[0] = (char) keyChar;
			String str = new String(tt);
			tstring = tstring + str;
		}
		jexPane.tex = tstring;
		jexPane.setStatus();
	}
	
        public void keyTyped(KeyEvent e) {
            DefaultContext d = jexPane.d;
	    d.set();
            mathrm = false;
            boolean clearmath = true;
            if(jexPane.mathStyle.equals("mathrm")) mathrm = true;
            if(jexPane.mathStyle.equals("null") & (d.mathrm)) mathrm = true;
            keyChar = e.getKeyChar();
	    if(tex){
		interpretTex(keyChar);
		return;
	    }
            if(keyCode == 8) {
                if(d.sel.width == 0) d.sel.width = -1;
                d.insertBox(new EmptyBox());
                greek = false;
            }
            else if(keyCode == 127) {
                if(d.sel.width == 0) d.sel.width = 1;
                d.insertBox(new EmptyBox());
                greek = false;
            }
            else if(ctrl & (keyCode == 32)){
                String sbtype = "\\,";
                if(mathrm) sbtype = "\\:";
                d.insertBox(new SpaceBox(sbtype));
            }
	    else if(ctrl & (keyCode == 92)) {d.insertBox(new CharBox(92));}
	    else if(ctrl & (keyChar == 26)) {d.undo(); 
            // System.out.println("undo");
           }
	    else if(ctrl & (keyChar == 24)) jexPane.doAction("Cut");
	    else if(ctrl & (keyChar == 3)) jexPane.doAction("Copy");
	    else if(ctrl & (keyChar == 22)) jexPane.doAction("Paste");
	    else if(ctrl & (keyCode == 77)) jexPane.doAction("SaveExit");
	    else if(keyCode == 10) {
		d.makeAlign();
	    }
	    else if(keyCode == 32) d.advance(1);
            else if(ctrl & ((keyCode == 74) || (keyChar == 12) || (keyChar == 8))) {
                if(d.sel.width == 0) d.sel.width = -1;
                d.insertBox(new SubSupBox(new EmptyBox(), new EmptyBox(), new EmptyBox()));
                greek = false;
		if(keyChar == 8) {
			d.advance(1);
			d.advance(1);
		}
            }
	    else if(ctrl & (keyChar == 61)){
                if(d.sel.width == 0) d.sel.width = -1;
                d.insertBox(new OverBox(new EmptyBox(), new EmptyBox()));
                greek = false;
            }
            else if(ctrl  & (keyChar == 7)) {greek = true;}
            else if(ctrl & (keyChar == 6)){
		jexPane.toggleMathRm();
                clearmath = false;
            }
            else {
                if(greek) {
                    int gChar = keyChar;
                    boolean cap = false;
                    if((keyChar >= 'A') & (keyChar <= 'Z')) cap = true;
                    if(cap) gChar = gChar + 'a' - 'A'; 
                    int grk = gr(gChar);
                    if(grk == 0) d.insertBox(new CharBox(keyChar));
                    else {
                        if(cap) {
                            if(grk == 0x3d5) grk = 0x3A6;
                            else grk = grk + 0x391 - 0x03b1;
                        }
                        d.insertBox(new CharBox(grk));
                    }
                }
                else {
		    if(keyChar == 92) {
			tex = true;
			tstring = "\\";
			jexPane.tex = tstring;
			jexPane.setStatus();
		    }
		    else if((keyChar >= 32) & (keyChar <= 126)){
                    	CharBox x = new CharBox(keyChar);
                    	d.insertBox(x);
                    	if(mathrm) x.setStyle("mathrm");
		    }
                }
                greek = false;
            }
	    jexPane.greek = false;
	    if(greek) jexPane.greek = true;
            if(clearmath) jexPane.mathStyle = "null";
            jexPane.repaint();
           // System.out.println("getKeyChar: " + (int)e.getKeyChar());
        }
		
        public void keyReleased(KeyEvent e){
            DefaultContext d = jexPane.d;
	    d.set();
            shift = false;
            ctrl = false;
            alt = false;
           // System.out.println("release " + e.getKeyCode());
        }
    }

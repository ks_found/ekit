package jex;

import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

public class Jexconf{

    static File confjex = null;

    static TreeMap parms = new TreeMap();

    static void init(){

	parms.put("jex.xml", "null");
	parms.put("printBinding", "ooeuclid");
	parms.put("editBinding", "");
	parms.put("printFace", "Euclid");
	parms.put("editFace", "Euclid");
	parms.put("latex2html", "latex2html");
	parms.put("mathItalic", "true");
	parms.put("greekItalic", "false");

	parms.put("size", "36");
	parms.put("highlight", "false");
	parms.put("preview", "false");

	parms.put("test1", "{a \\mathrm l  \\mathrm o  \\begin{array} {lr} a & b \\\\ c\\:e & d \\end{array}\\int x\\widehat\\alpha ab^\\left(abc\\right]{ bz_y^{ab}\\over{x}c}d_{zy}eA^x  b_{abc^3}^{a_0}{fg{hi }jk} l{}mn}");
	parms.put("test2", "\\int^{2}_{\\sum} \\frac{x^2}{2y}");

	parms.put("default", "");

    	parms.put("layouts", "\\begin{array} {llll} {}_{}^{} & \\overset {} {} & \\underset {} {} & {} \\over {} \\end{array}");

	//add root

	parms.put("symbols", "\\begin{array} {llllll} \\int & \\sum & \\prod & \\times & \\cdot & \\bullet \\\\ \\leq  & \\geq  & \\neq  & \\in    & \\ni   & \\notin \\\\ \\equiv & \\approx & \\propto & \\ldots & & \\\\ \\cap & \\cup & \\subset & \\subseteq & \\supset & \\supseteq \\\\ \\nabla & \\partial & \\infty & \\Re & \\emptyset & \\| \\\\ \\rightarrow & \\leftarrow & \\pm & \\ell & \\Ell & \\end{array}");

	parms.put("greek", "\\begin{array} {lllll} \\alpha & \\beta & \\psi & \\delta & \\epsilon \\\\ \\phi & \\gamma & \\eta & \\iota & \\varphi \\\\ \\kappa & \\lambda & \\mu & \\nu & \\omicron \\\\ \\pi & \\chi & \\rho & \\sigma & \\tau \\\\ \\upsilon & \\omega & \\xi & \\theta & \\zeta \\end{array}");

	parms.put("Greek", "\\begin{array} {lllll} A & B & \\Psi & \\Delta & E \\\\ \\Phi & \\Gamma & H & I & \\Phi \\\\ K & \\Lambda & M & N & O \\\\ \\Pi & X & P & \\Sigma & T \\\\ \\Upsilon & \\Omega & \\Xi & \\Theta & Z \\end{array}");

	parms.put("user", "{\\begin{array} {llll} {{}} &  {{}} &  {{}} &  {{}} \\\\ {{}} &  {{}} &  {{}} &  {{}} \\\\ {{}} &  {{}} &  {{}} &  {{}} \\\\ {{}} &  {{}} &  {{}} &  {{}} \\\\ {{}} &  {{}} &  {{}} &  {{}}\\end{array}}");

    }

    static void setProp(String prop, String val, boolean write){
		parms.put(prop, val);
		if((confjex != null) && write) writeConf();
	}

    static String getProp(String prop){
	return (String) parms.get(prop);
    }

    static void setFile(File f){
	confjex = f;
    }

    public static String getEntry(String s, String n){
	int pos = s.indexOf(n);
	if(pos < 0) return null;
	s = s.substring(pos);
	pos = s.indexOf("\n\n");
	if(pos < 0) return null;
	s = s.substring(0, pos);
	return s + "\n";	
    }

    public static String getLine(String s, String n){
	if (s == null) return null;
	int pos = s.indexOf(n);
	if (pos < 0) return null;
	s = s.substring(pos);
	pos = s.indexOf("=");
	if (pos < 0) return null;
	s = s.substring(pos + 1);
	pos = s.indexOf("\n");
	if(pos < 0) return null;
	s = s.substring(0, pos);
	s = s.trim();
	return s;
    }

    public static void writeConf(){
	String s = "";
	Iterator i = parms.keySet().iterator();
	String key;
	while(i.hasNext()){
		key = (String) i.next();
		s = s + key + "=" + parms.get(key) + "\n";
		try{
			FileOutputStream out = new FileOutputStream(confjex);
			out.write(s.getBytes());
			out.close();
		}
		catch(IOException ex){};		
	}
    }

    public static void readConf(){
	try{
		FileInputStream in = new FileInputStream(confjex);
		int nmax = 1000000; 
		byte b[] = new byte[nmax];
		int nread = 0;
		String s = "";
		while((nread = in.read(b)) >= 0) s = s + new String(b, 0, nread);
		int pos = s.indexOf("\n");
		String line = "";
		while(pos > 0){
			line = s.substring(0, pos);
			s = s.substring(pos + 1);
			pos = line.indexOf("=");
			if (pos >= 0){
				parms.put(line.substring(0,pos).trim(), line.substring(pos + 1).trim());
			}
			pos = s.indexOf("\n");
		}
	}
	catch(IOException ex){};

    }


    static Dimension frameSize = new Dimension(500, 300);
    static Dimension canvasSize = new Dimension(800, 12000);

    static int x = 0;
    static int y = 0;
    static int size = 36;

 
}


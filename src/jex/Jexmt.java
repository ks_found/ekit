package jex;

import java.awt.*;
import java.util.*;
import java.io.*;
import java.util.zip.*;

import jex.Jexmath;

public class Jexmt{

	public Jexmt(InputStream in){
		getInputStream(in);
	}

	public Jexmt(String fname){
		try{
			FileInputStream in = new FileInputStream(fname);
			getInputStream(in);	
		}
		catch(Exception ex){ex.printStackTrace();}
	}

	public Jexmt(byte b[]){
		ptr = 0;
		mtd = b;
	}

	public static RowBox getMTData(String fname, String oname){
		try{
			FileInputStream in = new FileInputStream(fname);
			ZipInputStream zip = new ZipInputStream(in);
			String ozname = "";
			for(ZipEntry ze = zip.getNextEntry(); ze != null; ze = zip.getNextEntry()){
				if(ze.getName().equals("content.xml")){
					byte b[] = streamToByteArray(zip);
					String instr = new String(b);
					String s = "draw:name=\"" + oname + "\"";
					int n = instr.indexOf(s);
					if(n < 0) return null;
					n = n + s.length();
					instr = instr.substring(n);
					s = "xlink:href=\"./";
					n = instr.indexOf(s);
					if(n < 0) return null;
					n = n + s.length();
					instr = instr.substring(n);
					n = instr.indexOf("\"");
					if(n < 0) return null;
					instr = instr.substring(0, n);
					System.out.println(instr);
					ozname = instr;
					break;
				}
			}
			zip.close();
			in.close();
			in = new FileInputStream(fname);
			zip = new ZipInputStream(in);
			for(ZipEntry ze = zip.getNextEntry(); ze != null; ze = zip.getNextEntry()){
				if(ze.getName().equals(ozname)){
					byte b[] = streamToByteArray(zip);			
					Jexmt jmt = new Jexmt(b);
					return jmt.parseMT();
				}
			}
			zip.close();
			in.close();	
		}
		catch(Exception ex){ex.printStackTrace();}	
		return null;	   
	}
	

	public static byte[] streamToByteArray(InputStream is){
		try{
			int nmin = 1000000;
			int nborg = 40000;
			int nmax = nmin + ( 5 * nborg); 
			byte b[] = new byte[nmax];
			int nread = 0;
			int noff = 0;
			ByteArrayOutputStream baost = new ByteArrayOutputStream();
			while((nread = is.read(b, noff, nborg)) >= 0){
				noff = noff + nread;
				if(noff > nmax - (2 * nborg)){
					baost.write(b, 0, noff);
					noff = 0;
				}
			}
			baost.write(b, 0, noff);
			return baost.toByteArray();
		}
		catch(Exception ex){ex.printStackTrace();}
		return null;
	}

	private byte[] mtd;
	private int ptr = 0;

	private void getInputStream(InputStream in){
		try{
			mtd = streamToByteArray(in);
			ptr = 0;
			
		}
		catch(Exception ex){ex.printStackTrace();}
	}

	private boolean checkNext(){
		if(ptr >= mtd.length) return false;
		return true;
	}

	private byte getNext(){
		byte b = mtd[ptr];
		ptr++;
		return b;
	}

	private boolean getStart(){
		boolean done = false;
		int state = 0;
		while(!done){
			if(!checkNext()) return false;
			byte b = getNext();
			switch (state){
				case 0: if (b == 5) state = 1;
					break;
				case 1: if ((b == 0) || (b == 1)) state = 2;
					else state = 0;
					break;
				case 2: if ((b == 0) || (b == 1)) state = 3;
					else state = 0;
					break;
				case 3: if (b >= 4) state = 4;
					else state = 0;
					break;
				case 4: state = 5;
					break;
				case 5: if(b == 'D') state = 6; 
					else state = 0;
					break;
				case 6:	if(b == 'S') state = 7;
					else state = 0;
					break;
				case 7:	if(b == 'M') state = 8;
					else state = 0;
					break;
				case 8: if(b == 'T') state = 9;
					else state = 0;
					break;
				case 9: if(b == 0) done = true;
					break;
				default: state = 0; 
					break;
			}
		}
		if (state != 9) return false;
		ptr++;
		return true;
	}

	private void parseForNull(){
		boolean done = false;
		while(!done){
			if(!checkNext()) return;
			byte b = getNext();
			if(b == 0) return;
		}
		return;
	}

	public int endct = 0;

	public void lineAfterFont(){
		if(true) return;
		if (mtd[ptr] == 1) {
			ptr ++;
			endct++;
			System.out.println("LINE after font");
			if(mtd[ptr] == 0){
				//ptr++;
				//endct--;
				System.out.println("END after font LINE");
			}
		}
	}

	public int recType;

	private void parseRuler(){
		int c = (int) getNext();
		ptr = ptr + (c * 3);
	}

	private void skipNudge(){
		byte b = getNext();
		byte bb = getNext();
		if((b == -128) && (bb == -128)) ptr = ptr + 4;
	}

	private int getInt(){
		int b =  (int) getNext() & 0xFF;
		int bb = (int) getNext() & 0xFF;
		//System.out.println("" + b + " " + bb);
		return b + (0x100 * bb);
	}

	private long getUInt(){
		int b = (int) getNext() & 0xFF;
		long bl = (long) b;
		if(b >= 255){
			b = (int) getNext() & 0xFF;
			int bb = (int) getNext() & 0xFF;
			bl = b + (0x100 * bb);
		}
		return bl;
	}
			

	private int subType = 0;

	private MathBox nextReal(){
		MathBox mb = null;
		subType = 0;
		recType = -1;
		while(mb == null){
			if(!checkNext()) return null;
			mb = nextRecord(true);	
		}
		return mb;
	}

	private boolean embellOver = true;

	private RowBox getEmbell(byte bb){
		RowBox rb = new RowBox();
		int uni = 0;
		int rep = 1;
		embellOver = true;		
		switch(bb){
			case 2: uni = '.'; break;
			case 3: uni = '.'; rep = 2; break;
			case 4: uni = '.'; rep = 3; break;
			case 8: uni = '~'; break;
			case 9: uni = '^'; break;
			case 11: uni = 0x2192; break;
			case 12: uni = 0x2190; break;
			case 17: uni = '-'; break;
			case 24: uni = '.'; rep = 4; break;
			case 25: uni = '.'; embellOver = false; break;
			case 26: uni = '.'; embellOver = false; rep = 2; break;
			case 27: uni = '.'; embellOver = false; rep = 3; break;
			case 28: uni = '.'; embellOver = false; rep = 4; break;
			case 29: uni = '-'; embellOver = false; break;
			case 30: uni = '~'; embellOver = false; break;
			case 33: uni = 0x2192; embellOver = false; break;
			case 34: uni = 0x2190; embellOver = false; break;
			default: return null;
		}
		for(int ii = 0; ii < rep; ii++)	rb.addChild(new CharBox(uni));
		return rb;
	}

	private void getNull(int id){
		MathBox mb = null;
		subType = 0;
		recType = -1;
		while(recType != 0){
			if(!checkNext()) return;
			mb = nextRecord(true);	
		}
		System.out.println("END of TMPL " + id);
	}

	private MathBox getLine(RowBox rb){
		boolean done = false;
		while(!done){
			MathBox mb = nextRecord(true);
			if(mb != null) {
				if(subType == 1) {
					MathBox zb = (MathBox) rb.c.removeLast();
					RowBox zbnew = new RowBox();
					zbnew.addChild(zb);
					LayoutBox lb = (LayoutBox) mb;
					lb.c.remove(0);
					lb.c.add(0,zbnew);
					rb.addChild(mb);
				}
				else if(recType == 1){
					RowBox zrb = (RowBox) mb;
					int nn = zrb.c.size();
					for(int ii = 0; ii < nn; ii++){
						MathBox mbx = (MathBox) zrb.c.remove(0);
						if(!mbx.isEmpty()) rb.addChild(mbx);
					}
				}
						else rb.addChild(mb);
			}
			if((recType == 0) || (!checkNext())) {
				subType = 0;
				recType = 1;
				if(rb.c.size() == 0) rb.addChild(new EmptyBox());
				return rb;
			}
		}
		return null;
	}

	private int nid = 0;
					
	private MathBox nextRecord(boolean prs){
		subType = 0;
		recType = -1;
		if(!checkNext()) return null;
		byte b = getNext();
		recType = b;
		switch(b) {
			case 0: if(prs) System.out.println("END");
				break;
			case 1: if(!prs) break;
				b = getNext();
				if((b & 0x8) != 0) skipNudge();
				if((b & 0x4) != 0) ptr = ptr + 2; //lspace
				if((b & 0x2) != 0) { //ruler
					ptr++;
					parseRuler();
				}
				RowBox rb = new RowBox();
				if((b & 0x1) != 0) {
					System.out.println("LINE + END");
					rb.addChild(new EmptyBox());
					subType = 0;
					recType = 1;
					subType = 2;
					recType = -1;
					rb = null;
					return rb;
				}
				int tline = nid++;
				System.out.println("LINE " + tline);
				getLine(rb);
				System.out.println("" + tline + ": " + rb.toTex());
				return rb;
			case 2: if(!prs) break;
				byte bb = getNext();
				if((bb | 0x8) == 0) skipNudge();
				//skip over the typeface
				byte bbb = getNext();
				if(bbb == 255) ptr = ptr + 2; //signed integer is not packed
				//0x01  mtefOPT_CHAR_EMBELL  	character is followed by an embellishment list
				//0x02 	mtefOPT_CHAR_FUNC_START character starts a function (sin, cos, etc.)
				//0x04 	mtefOPT_CHAR_ENC_CHAR_8 character is written with an 8-bit encoded value
				//0x10 	mtefOPT_CHAR_ENC_CHAR_16 character is written with an 16-bit encoded value
				//0x20 	mtefOPT_CHAR_ENC_NO_MTCODE character is written without an 16-bit MTCode value
				int uni = 0;
				if((bb & 0x20) == 0) uni = getInt();
				if(uni == 0x2212) uni = '-';
				CharBox cb = new CharBox(uni);
				if(uni == 61192) {
					subType = 0;
					recType = 2;
					return null;
				}
				if(uni == 61185) cb = new SpaceBox("\\,");
				if(uni == 61186) cb = new SpaceBox("\\,");
				if(uni == 61188) cb = new SpaceBox("\\:");
				if(uni == 61189) cb = new SpaceBox("\\;");				
				System.out.println("CHAR " + (char) uni + " " + uni);
				if((bb & 0x4) != 0) ptr++;
				if((bb & 0x10) != 0) ptr = ptr + 2;
				if((bb & 0x01) != 0){
					MathBox bmb = cb;
					MathBox emb = null;
					boolean donee = false;
					while(!donee){
						if(!checkNext()) donee = true;
						else emb = nextRecord(true);
						if(!donee){
							if((emb != null) && (recType == 6)){
								if(embellOver) bmb = new OverBox(bmb, emb);
								else bmb = new UnderBox(bmb, emb);
							}
							if(recType == 0) donee = true;
						}
						if(donee){
							subType = 0;
							recType = 2;
							return bmb;
						}
					}
				}
				subType = 0;
				recType = 2;
				return cb;
			case 3: if(!prs) break;
				byte bb3 = getNext();
				if((bb3 | 0x8) == 0) skipNudge();
				byte tt = getNext();
				int ttmpl = nid++;
				System.out.println("TMPL " + tt + " " + ttmpl);
				byte var = getNext();
				int ivar = (int) var;
				if(var < 0) {
					byte tmp = getNext();
					ivar = (int)((var & 0x7F)  |  (tmp << 8));
				}
				byte topt = getNext();
				if((tt == 27) || (tt == 28) || (tt == 29)){
					SubSupBox ssb = new SubSupBox();
					RowBox rmain = new RowBox();
					rmain.addChild(new EmptyBox());
					ssb.addChild(rmain);
					//RowBox rsub = new RowBox();
					//ssb.addChild(rsub);
					//RowBox rsup = new RowBox();
					//ssb.addChild(rsup);
					if(tt == 28){
						ssb.addChild(ssb.wrapBox(new EmptyBox()));
						ssb.addChild(ssb.wrapBox(nextReal()));
					}
					if(tt == 27){
						ssb.addChild(ssb.wrapBox(nextReal()));
						ssb.addChild(ssb.wrapBox(new EmptyBox()));
					}
					if(tt == 29){
						ssb.addChild(ssb.wrapBox(nextReal()));
						ssb.addChild(ssb.wrapBox(nextReal()));
					}
					getNull(ttmpl);
					subType = 1;
					recType = 3;
					return ssb;
				}
				if(tt == 11){
					DivisionBox db = new DivisionBox(nextReal(), nextReal());
					getNull(ttmpl);
					subType = 0;
					recType = 3;
					return db;
				}
				if(tt <= 8){
					MathBox mainslot = nextReal();
					//documentation claims the upper limit is first, but in fact lower limit is
					MathBox lf = new EmptyBox();
					if((ivar & 0x0001) != 0) lf = nextReal();
					MathBox rf = new EmptyBox();
					if((ivar & 0x0002) != 0) rf = nextReal();
					FenceBox fb = new FenceBox(lf, mainslot, rf);
					getNull(ttmpl);
					subType = 0;
					recType = 3;
					return fb;
				}
				if((tt >= 15) && (tt <= 22)){
					//variation bits
					// 0x0001  	tvBO_LOWER  	lower limit is present
					// 0x0002 	tvBO_UPPER 	upper limit is present
					// the previous is as documented, but appears wrong, should be 0x0010, 0x0020
					// 0x0040 	tvBO_SUM 	summation-style limit positions, else integral-style
					MathBox mainslot = null;
					//according the to documentation tt = 21, 22 should also have mainslot, but does not
					if(tt <= 20) mainslot = nextReal();
					//documentation claims the upper limit is first, but in fact lower limit is
					MathBox llim = null;
					if((ivar & 0x0010) != 0) llim = nextReal();
					MathBox ulim = null;
					if((ivar & 0x0020) != 0) ulim = nextReal();
					RowBox newrow = new RowBox();
					if((ulim != null) || (llim != null)){
						MathBox ochar = nextReal();
						SubSupBox ssb = new SubSupBox();
						ssb.addChild(ssb.wrapBox(ochar));
						if(llim != null) ssb.addChild(ssb.wrapBox(llim));
						else ssb.addChild(ssb.wrapBox(new EmptyBox()));
						if(ulim != null) ssb.addChild(ssb.wrapBox(ulim));
						else ssb.addChild(ssb.wrapBox(new EmptyBox()));
						newrow.addChild(ssb);
					}
					else {
						int unitmp = 0;
						if(tt == 15) unitmp = 8747;
						if(tt == 16) unitmp = 8721;
						if(tt == 17) unitmp = 8719;
						if(tt == 19) unitmp = 8746;
						if(tt == 20) unitmp = 8745;
						CharBox ncb = new CharBox(unitmp);
						//newrow.addChild(ncb);
						newrow.addChild(newrow.wrapBox(nextReal()));
					}
					if(mainslot != null) newrow.addChild(mainslot);
					getNull(ttmpl);
					subType = 0;
					recType = 1;
					return newrow;						
				}
				RowBox rb3 = new RowBox();
				int tun = nid++;
				System.out.println("unknown TMPL " + tun);
				getLine(rb3);
				FenceBox bogus = new FenceBox();
				bogus.addChild(bogus.wrapBox(new CharBox('?')));
				bogus.addChild(rb3);
				bogus.addChild(bogus.wrapBox(new CharBox('?')));
				subType = 0;						
				recType = 3;
				return bogus;
			case 4: if(!prs) break;
				int tpile = nid++;
				System.out.println("PILE " + tpile);
				byte bb4 = getNext();
				if((bb4 | 0x8) == 0) skipNudge();
				byte halign4 = getNext();
				byte valign4 = getNext();
				if((b & 0x2) != 0) {
					ptr++;
					parseRuler();
				}
				MatrixBox map = new MatrixBox();
				map.cols = 2;
				map.halign.add("r");
				map.halign.add("l");
				map.texType = "align";
				map.spacef = 0.0;
				map.leadf = 0.0;
				boolean d4 = false;
				while(!d4){
					if(!checkNext()) d4 = true;
					else if (mtd[ptr] == 0) {
						d4 = true;
						ptr++;
					}
					if(d4){
						recType = 4;
						subType = 0;
						System.out.println("PILE END " + tpile);
						return map;
					}					
					MathBox mb4 = nextReal();
					map.rows++;
					map.addChild(map.wrapBox(mb4));
					map.addChild(map.wrapBox(new EmptyBox()));
				}
				break;
			case 5: if(!prs) break;
				int tmat = nid++;
				System.out.println("MATRIX " + tmat);
				byte bb5 = getNext();
				if((bb5 | 0x8) == 0) skipNudge();
				byte valign = getNext();
				byte hjust = getNext();
				byte vjust = getNext();
				byte rows = getNext();
				byte cols = getNext();
				getNext(); //parse over row_parts
				getNext(); //parse over col_parts
				MatrixBox mab = new MatrixBox();
				mab.rows = rows;
				mab.cols = cols;
				if(vjust == 3) mab.valign = "c";
				String hh = "l";
				if(hjust == 2) hh = "c";
				if(hjust == 3) hh = "r";
				boolean d5 = false;
				for(int jj = 0; jj < cols; jj++) mab.halign.add(hh);
				for(int ii = 0; ii < rows; ii++){
					for(int jj = 0; jj < cols; jj++){
						if(!d5){
							MathBox mb5 = nextReal();
							if(recType == 0) d5 = true;
							else mab.addChild(mab.wrapBox(mb5));
						}
						if(d5) mab.addChild(mab.wrapBox(new EmptyBox()));
					}
				}
				if(recType != 0) getNull(-1);
				recType = 5;
				subType = 0;
				System.out.println("END MATRIX " + tmat);
				return mab;
			case 6: if(!prs) break;
				byte bb6 = getNext();
				if((bb6 | 0x8) == 0) skipNudge();
				byte emb6 = getNext();
				System.out.println("EMBELL " + emb6);
				MathBox gemb = getEmbell(emb6);
				subType = 0;
				recType = 6;
				return gemb;
			case 7: if(prs) System.out.println("RULER");
				else break;
				parseRuler();
				break;
			case 8: if(prs) System.out.println("FONT_STYLE_DEF");
				else break;
				getUInt();
				getNext();
				break;	
			case 9: if(prs) System.out.println("SIZE");
				else break;
				byte bb9 = getNext();
				if(bb9 == 101) ptr = ptr + 2;
				else if(bb9 == 100) ptr = ptr + 3;
				else ptr++;
				break;			
			case 10: if(prs) System.out.println("FULL size");
				else break;
				lineAfterFont();
				break;
			case 11: if(prs) System.out.println("SUB size");
				else break;
				lineAfterFont();
				break;
			case 12: if(prs) System.out.println("SUB2 size");
				else break;
				lineAfterFont();
				break;
			case 13: if(prs) System.out.println("SYM size");
				else break;
				lineAfterFont();
				break;
			case 14: if(prs) System.out.println("SUBSYM size");
				else break;
				lineAfterFont();
				break;
			case 15: if(prs) System.out.println("COLOR");
				else break;
				getUInt();
				break;
			case 16: if(prs) System.out.println("COLOR_DEF");
				else break;
				byte bb16 = getNext();
				ptr = ptr + 3;
				if((bb16 | 0x1) == 0) ptr++; //cmyk color model
				if((bb16 | 0x4) == 0) parseForNull();
				break;				
			case 17: if(prs) System.out.println("FONT_DEF");
				else break;
				parseForNull();
				break;
			case 18: if(prs) System.out.println("EQN_PREFS");
				else break;
				boolean test = true;
				while(test){
					nextRecord(false);
					if(!checkNext()){
						subType = 0;
						recType = 18;
						return null;
					}
					if(recType == 10){
						if(!checkNext()){
							subType = 0;
							recType = 18;
							return null;
						}
						nextRecord(false);
						if((recType == 1) || (recType == 4)){
							ptr = ptr - 1;
							subType = 0;
							recType = 18;
							if(prs) System.out.println("OK");
							return null;
						}
					}
				}
				break;
			case 19: if(prs) System.out.println("ENCODING_DEF");
				else break;
				parseForNull();
				break;
			default: if(!prs) break;
				System.out.println("NOT IMPLEMENTED REC: " + b);
				if((b >= 100) || (b < 0)){
					long bl = getUInt();
					ptr = ptr + (int) bl;
				}					
				break;
		}
		return null;
	}

	public RowBox parseMT(){
		//System.out.println("starting");
		RowBox ans = new RowBox();
		if(getStart()){
			System.out.println("Found start");
			int nends = 0;
			while(checkNext()) {
				System.out.println("Starting interpreter");
				MathBox mb = nextRecord(true);
				if(mb != null){
					if(recType == 1){
						RowBox rb = (RowBox) mb;
						int nrb = rb.c.size();
						for(int ii = 0; ii < nrb; ii++) {
							MathBox nmb = (MathBox) rb.c.remove(0);
							if(!nmb.isEmpty()) ans.addChild(nmb);
						}
						//if(endct == 0) return ans;
						endct--;
					}
					else if(subType == 1) {
							MathBox zb = (MathBox) ans.c.removeLast();
							RowBox zbnew = new RowBox();
							zbnew.addChild(zb);
							LayoutBox lb = (LayoutBox) mb;
							lb.c.remove(0);
							lb.c.add(0,zbnew);
							ans.addChild(mb);
						}
					else ans.addChild(mb);
					return ans;
				}
				if(recType == 0){
					nends++;
					if(nends > 6) return ans;
				}
			}
		}
		return ans;
		
	}

        
}

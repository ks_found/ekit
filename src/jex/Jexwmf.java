package jex;

import java.awt.Color;
import java.util.*;
import java.io.*;

import jex.Jex;
import jex.Jexmath;
import jex.Jexfont;

public class Jexwmf implements MathDisplay{

    static int TA_LEFT   = 0;
    static int TA_RIGHT  = 2;
    static int TA_CENTER = 6;

    static int TA_TOP      = 0;
    static int TA_BOTTOM   = 8;
    static int TA_BASELINE = 24;
    public static int sf = 30;

    public String tfile;
    public float fsize0 = 12.0f;
    public BoxMetrics m;
    public int adj = 1;

    private FileOutputStream out;
    private int nobjs = 0;
    private int nwords = 12; //header + footer
    private int maxrec = 3;  //cannot have a record shorter than this
    private ByteArrayOutputStream bout = new ByteArrayOutputStream();


    private byte[] getWord(int w){
	byte b[] = new byte[2];
	b[0] = (byte) (w & 0xff);
	w = w & 0xff00;
	b[1] = (byte) (w >> 8);
	return b;
    }

    private byte[] getDWord(int w){
	byte b[] = new byte[4];
	b[0] = (byte) (w & 0xff);
	int i = w & 0xff00;
	b[1] = (byte) (i >> 8);
	i = w & 0xff0000;
	b[2] = (byte) (i >> 16);
	i = w & 0xff000000;
	b[3] = (byte) (i >> 24);
	return b;
    }

    private byte[] APM(int w, int check[]){
	check[0] = check[0] ^ w;
	byte b[] = getWord(w);
	return b;
    }

    public void setLength(int len){
	try{
		len = len + 2;
		bout.write(getDWord(len));
		if(len > maxrec) maxrec = len;
		nwords = nwords + len;
	}
	catch(Exception e){System.err.println(e);}
    }
	

    public void pushObj(){
	try{
		setLength(0x2);
		bout.write(getWord(0x12d));
		bout.write(getWord(nobjs));
		nobjs++;
	}
	catch(Exception e){System.err.println(e);}		
    }

    public void setTextAlign(int i){
	try{
		setLength(2);
		bout.write(getWord(0x12e));
		bout.write(getWord(i));
	}
	catch(Exception e){System.err.println(e);}
    }

    public void setFont(int height, String face, String style, int width){
	String s = face;
	int italic = 0;
	if(style.equals("Italic")) italic = 1;
	int slen = s.length() + 1;
	int wlen = slen / 2;
	int pad = 0;
	if((slen % 2) > 0) pad = 1;
	wlen = wlen + pad;
	int weight = 0x190;
	try{
		setLength(10 + wlen);
		bout.write(getWord(0x2fb));
		bout.write(getWord(height)); 	//height
		bout.write(getWord(width));	//width
		bout.write(getWord(0x0));	//escapement
		bout.write(getWord(0x0));	//orientation
		bout.write(getWord(weight));	//weight
		bout.write(italic);		//italic
		bout.write(0x0);		//underline
		bout.write(0x0);		//strikeout
		bout.write(0x1);		//charset (ansi = 0; unicode = 1; symbol = 2)
		bout.write(0x0);		//out precision
		bout.write(0x2);		//clip precision
		bout.write(0x0);		//quality
		bout.write(0x10);		//pitch and family
		bout.write(s.getBytes());
		bout.write(0x0);
		if(pad == 1) bout.write(0x0);
	}
	catch(Exception e){System.err.println(e);}
    }

    public void writeStr(int x, int y, String s){
	int pad = 0;
	int slen = s.length();
	if((slen % 2) > 0) pad = 1;
	try{
		setLength(4 + (s.length()/2) + pad);			
		bout.write(getWord(0x521));
		bout.write(getWord(slen));
		bout.write(s.getBytes("ISO-8859-1"));
		if(pad == 1) bout.write(0x00);
		bout.write(getWord(y));
		bout.write(getWord(x));
	}
	catch(Exception e){System.err.println(e);}
    }

    int base;	
     
    public void printEq(String tex, DefaultContext d){
    
        try{

	    	//int fsize = (int) (1.19 * fsize0 * sf);
		int fsize = (int) (1.10 * fsize0 * sf);
            	boolean oldshowEmpty = d.showEmpty;
            	boolean oldshowPhantom = d.showPhantom;
            	float oldhPad = d.hPad;
            	int oldsize = d.size;
	    	String oldbinding = d.binding;
		float oldpad = d.pad;
		MathDisplay oldmd = d.md;
            
		d.set();
		d.md = this;
            	d.setPrint();
            	d.printeq = true;
            	d.size = fsize;
		d.binding = Jexfont.printBinding;
		d.pad = 0;

            	d.makeMetrics();
            	m = d.getEqn().boxMetrics;

		out = new FileOutputStream(tfile);

		adj = m.getHeight(fsize);
		int hijink = (int) (0.2f * (float) adj);
		int top = -m.baseline - m.edgeN - hijink;
		int right = m.width + m.edgeE + m.edgeW;
		int bot = (top + m.height + m.edgeN + m.edgeS + hijink);
		base = m.baseline;

		int ppi = sf * 72;

		int check[] = new int[1];
		check[0] = 0;
		//write the placeable header
		out.write(APM(0xcdd7, check)); // magic number low word
		out.write(APM(0x9ac6, check)); // magic number hi word
		out.write(APM(0x0, check));    // handle always 0
		out.write(APM(0x0, check));    // left 
		out.write(APM(top, check));    // top
		out.write(APM(right, check));  // right
		out.write(APM(bot, check));    // bottom
		out.write(APM(ppi, check));    // pixels per inch
		out.write(APM(0x0, check));    // reserved always 0
		out.write(APM(0x0, check));    // reserved always 0
		out.write(getWord(check[0]));  //checksum
		//get the body of the wmf
		//setTextAlign(0);
            	d.display();

		d.md = oldmd;
            	d.printeq = false;
            	d.showEmpty = oldshowEmpty;
            	d.showPhantom = d.showPhantom;
            	d.hPad = oldhPad;
		d.pad = oldpad;
            	d.size = oldsize;
            	d.binding = oldbinding;
		//write the wmf header
		out.write(getWord(0x01));  	//FileType is disk metafile
		out.write(getWord(0x09));  	//HeaderSize (always 9)
		out.write(getWord(0x300)); 	//Windows Version
		out.write(getDWord(nwords));	//FileSize in words
		out.write(getWord(nobjs));	//Number of Objects
		out.write(getDWord(maxrec));	//Maximum Record Size in words
		out.write(getWord(0x00));	//not used always 0;

		//write the body
		out.write(bout.toByteArray());

		//write the footer
		out.write(getDWord(0x3));
		out.write(getWord(0x0));

		out.close();
            
        }
        catch( Exception exception ) {
            System.err.println( exception );
        }
    }

    public void showChar(String str, String face, String style, int size, float ratio, float stretch, float aftr, int x, double y, BoxMetrics m, int vshift){

	int width = 0;
	float rescale = 1.6f;
	if(face.equals(Jexfont.printFace) && !face.startsWith("Euclid")) {
		if(stretch == 0) rescale = 1.0f;
		else face = "Euclid";
	}

	size = (int) (rescale * size);
	int yy = (int)(y - (base + m.baseline + vshift));
	yy = (int)(yy - ((rescale - 1) * m.baseline));
	yy = yy - (int)((1.6f - rescale) * m.baseline * (0.55f));

	if(stretch > 0){
System.out.println(ratio);
		boolean fat = false;
		if(ratio > 1.8) fat = true;
		if(fat) width = (int) (m.width * rescale * stretch * 1.1);
		else width = (int) (m.width * rescale * stretch * 0.7);
		yy = (int)(yy - ((rescale - 1) * m.getHeight(size)/2));
		if(width > 0) {
			if(fat) x = (int) (x + rescale * ((0.1 * m.getWidth(size)) + (m.edgeW * 0.1) - (aftr * 0.12)));
			else x = (int) (x + rescale * ((0.085 * m.getHeight(size)) + (m.edgeW * 0.1) - (aftr * 0.12)));
		}
	}

	if(stretch < 0) {
		size = (int) (-stretch * size);
		width = (int) (2.5 * m.width);
		float a = 1.0f;
		float b = 0.50f;
		float c = 1.00f;
		//yy = yy - (int)(stretch * (float) m.getHeight(size)/50);
		yy = (int)((a * yy) + (b * size) + (c * size / stretch) + 0.5);	
	}

	setFont(size, face, style, width);
	pushObj();

	writeStr(x, yy, str);

    }
    
    //public void grabEq(){}

    //public MathDisplay getNewMathDisplay(Object obj){
	//return this;
   //}
}



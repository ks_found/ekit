package jex;

import com.sun.star.bridge.XUnoUrlResolver;
import com.sun.star.lang.XMultiServiceFactory;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.XComponentContext;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XInterface;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.beans.PropertyValue;
import com.sun.star.beans.Property;
import com.sun.star.beans.XPropertySetInfo;
import com.sun.star.frame.*;
import com.sun.star.frame.XDesktop;
import com.sun.star.frame.XFrame;
import com.sun.star.frame.XController;
import com.sun.star.frame.XModel;
import com.sun.star.frame.XDispatchHelper;
import com.sun.star.frame.XDispatchProvider;
import com.sun.star.view.XSelectionSupplier;
import com.sun.star.view.XLineCursor;
import com.sun.star.container.XIndexAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.text.XText;
import com.sun.star.text.XTextRange;
import com.sun.star.text.XTextFrame;
import com.sun.star.text.XTextContent;
import com.sun.star.text.XTextViewCursorSupplier;
import com.sun.star.text.XTextViewCursor;
import com.sun.star.text.XTextCursor;
import com.sun.star.text.TextContentAnchorType;
import com.sun.star.text.VertOrientation;
import com.sun.star.text.XTextEmbeddedObject;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.ControlCharacter;
import com.sun.star.text.XTextGraphicObjectsSupplier;
import com.sun.star.text.XTextEmbeddedObjectsSupplier;
import com.sun.star.beans.XPropertySet;
import com.sun.star.drawing.XShape;
import com.sun.star.drawing.XShapeGrouper;
import com.sun.star.drawing.XShapes;
import com.sun.star.drawing.XDrawPageSupplier;
import com.sun.star.drawing.HomogenMatrixLine3;
import com.sun.star.drawing.HomogenMatrix3;
import com.sun.star.awt.Size;
import com.sun.star.awt.FontWeight;
import com.sun.star.awt.Point;
import com.sun.star.awt.XWindow;
import com.sun.star.table.BorderLine;
import com.sun.star.util.XSearchable;
import com.sun.star.util.XSearchDescriptor;
import com.sun.star.container.XNameAccess;
import com.sun.star.container.XContentEnumerationAccess;
import com.sun.star.container.XEnumeration;
import com.sun.star.awt.FontSlant;
import com.sun.star.connection.*;

import java.awt.Color;
import java.util.*;
import java.io.*;
import java.net.URI;
import java.util.zip.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import jex.Jex;
import jex.Jexmath;
import jex.Jexwmf;
import jex.Jexmt;
import jex.Jexconf;

public class Jexoo implements MathDisplay, Office {
     
    static String connectStr = "uno:socket,host=localhost,port=8100;urp;StarOffice.ServiceManager";
    public static XDesktop xd;
    public static XDispatchHelper xdh;
    static XComponentContext xcctxt;
    static String jexexec = "";
    static String userLoc = "";
    static String shareLoc = "";
    static String javaLoc = "";
    static String euclid = "";
    static boolean server = false;
    static boolean checkExit = false;

    public Jex jex;
    public String jexname = null;

    public Jexoo(Jex jex) {
	this.jex = jex;
    }

    public Jexoo(){}

    private String makeUnSafe(String s){
	String unsafe = "";
	for(int ii = 0; ii < s.length()/2; ii++){
		int jj = 2 * ii;
		String test = s.substring(jj, jj + 2);
		jj = Integer.parseInt(test, 16);
		unsafe = unsafe + (new Character((char)jj)).toString();
	}
	return unsafe;
    }

    private String makeSafe(String s){
	String safe = "";
	if(s.startsWith("{\\begin{align}")) s = s.substring(1, s.length() - 1);
	else s = "$" + s + "$";
	for(int ii = 0; ii < s.length(); ii++){
		String flux = Integer.toHexString(s.charAt(ii));
		if(flux.length() == 1) flux = "0" + flux;
		safe = safe + flux;
	}
	return safe;
    }

    static final String jstr = "jex149dtexe";
    private String etype = "equation";

	private XText xdt;
	private XTextCursor xma;

	private String insertMarker(Object something){
		XTextContent xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, something);
      		xdt = xdoc.getText();
		XTextRange xtr = xt.getAnchor();
		xma = xdt.createTextCursorByRange(xtr);
        	xma.goLeft((short)1, false);
		xma.goRight((short)1, false);
		String tname = getName();
		xdt.insertString(xma, tname, true);
		return tname;
	}

	private void updateGraphs(String orps[], XNameAccess xna){
	for(int ii = 0; ii < orps.length; ii++){
		orc.msg.setText("Updating " + etype + " " + ii);
		String floon = orps[ii];
		Object something = null;
		try{
			something = xna.getByName(floon);
		}
		catch(Exception ex){ex.printStackTrace();}
		XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, something);
		String ans[] = getTex(xs);
		String tex = ans[0].trim();
		if(!tex.equals("") && !tex.equals("{}") && !(tex.indexOf("\\qedsymbol")>=0)){
			String tname = insertMarker(something);
			if(findview(tname)){
				xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);
				xdt.insertString(xma, "", true);
				xvc.goRight((short) 1, true);
				uedc.setEqn(MathBox.getTex(tex));
				insertEq(tex, uedc);
			}	
			else xdt.insertString(xma, "", true);		
		}
		else if(embedGraphics){
			try{
				XPropertySet xsp = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xs);
				String tfile = (String) xsp.getPropertyValue("GraphicURL");
				//System.out.println(tfile);
				if(!tfile.startsWith("vnd.sun.star")) {
					String atext = "";
					try{
						atext = (String) xsp.getPropertyValue("AlternativeText");
					}
					catch(Exception ex){System.out.println("embedGraphics cannot get AlternativeText");}
					String tname = insertMarker(something);
					if(findview(tname)){
						xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);
						xdt.insertString(xma, "", true);
						xvc.goLeft((short) 1, true);
						xvc.goRight((short) 2, true);
						if(atext.equals("[*]"))	{
							String hln = (String) xsp.getPropertyValue("HyperLinkName");
							String hlu = (String) xsp.getPropertyValue("HyperLinkURL");
							String test = "tex2html";
							int n = hln.indexOf(test);
							if(n >= 0) atext = "[*|" + hln.substring(n + test.length()) + "]";
							else {
								n = hlu.indexOf(test);
								if(n >= 0) atext = "[*||" + hlu.substring(n + test.length()) + "]";
							}
							xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);
							xdt.insertString(xma, atext, true);
						}
						else if(atext.indexOf("\\qedsymbol") >=0){
							xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);
							xdt.insertString(xma, "QED", true);
						}
						else insertGraphic(tfile, atext);
					}
					else xdt.insertString(xma, "", true);
				}
	    		}
			catch(Exception ex){ex.printStackTrace();}
		}
		if(orc.cl.isCancelled) ii = orps.length;
	}
    }

    private void fixGraphs(String orps[], XNameAccess xna){
	for(int ii = 0; ii < orps.length; ii++){
		orc.msg.setText("Encoding " + etype + " " + ii);
		String floon = orps[ii];
		Object something = null;
		try{
			something = xna.getByName(floon);
		}
		catch(Exception ex){ex.printStackTrace();}
		XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, something);
		String ans[] = getTex(xs);
		String tex = ans[0];
		if(!tex.equals("")){
			XTextContent xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, something);
      			XText xdt = xdoc.getText();
			XTextRange xtr = xt.getAnchor();
			XTextCursor xma = xdt.createTextCursorByRange(xtr);
			xma.goRight((short)1, true);
			String tname = jstr + makeSafe(tex) + "gg ";
			xdt.insertString(xma, tname, true);
		}
		if(orc.cl.isCancelled) ii = orps.length;
	}
    }

   ooRunConvert orc;
   DefaultContext uedc;
   boolean embedGraphics;

   public void updateEquations(DefaultContext d, boolean embedGraphics){
	if(!embedGraphics) orc = new ooRunConvert("Updating All Equations", "updateEquations");
	else orc = new ooRunConvert("Embedding All Graphics", "updateEquations");
	uedc = d;
	this.embedGraphics = embedGraphics;
	orc.setJexoo(this);
	orc.start();
   }

   public void updateEquations0(){
	orc.msg.setText("Starting update");
	getCurrent();
	XStorable xm = (XStorable) UnoRuntime.queryInterface(XStorable.class, xcc);
	String url = xm.getLocation();
	url = url + ".odt";
	orc.msg.setText("Saving " + url);
	PropertyValue pv[] = new com.sun.star.beans.PropertyValue[5];
	cpv = pv;
	setProp(0, "URL", url);
	setProp(1, "FilterName", "writer8");
	setProp(4, "SelectionOnly", true);
	xdh.executeDispatch(xdp, ".uno:SaveAs", "", 0, pv);
	XTextGraphicObjectsSupplier xgos = (XTextGraphicObjectsSupplier)UnoRuntime.queryInterface(XTextGraphicObjectsSupplier.class, xcc);
	XNameAccess xna = xgos.getGraphicObjects();
	String orps[] = xna.getElementNames();
	etype = "equation";
	updateGraphs(orps, xna);
	XTextEmbeddedObjectsSupplier xeos = (XTextEmbeddedObjectsSupplier)UnoRuntime.queryInterface(XTextEmbeddedObjectsSupplier.class, xcc);
	xna = xeos.getEmbeddedObjects();
	orps = xna.getElementNames();
	etype = "mathtype";
	updateGraphs(orps, xna);
	orc.jw.setVisible(false);
   }

   public void convertToTex(){
	orc = new ooRunConvert("Exporting TeX Document", "convertToTex");
	orc.setJexoo(this);
	orc.start();
   }

   public void convertFromTex(){
	orc = new ooRunConvert("Importing TeX Document", "convertFromTex");
	orc.setJexoo(this);
	orc.start();
    }	

    public void convertFromTex0(){
	orc.msg.setText("Starting conversion");
	getCurrent();
	XMultiComponentFactory xmcf = xcctxt.getServiceManager();
	try{
		com.sun.star.ui.dialogs.XFilePicker xfp = (com.sun.star.ui.dialogs.XFilePicker) UnoRuntime.queryInterface(com.sun.star.ui.dialogs.XFilePicker.class, xmcf.createInstanceWithContext("com.sun.star.ui.dialogs.FilePicker", xcctxt));
		com.sun.star.ui.dialogs.XFilterManager xfm = (com.sun.star.ui.dialogs.XFilterManager)UnoRuntime.queryInterface(com.sun.star.ui.dialogs.XFilterManager.class, xfp);
		xfm.appendFilter( "All files (*.*)", "*.*" );
		xfm.appendFilter( "LaTeX Files (*.tex)", "*.tex" );
		xfm.setCurrentFilter( "LaTeX Files (*.tex)" );
		xfp.execute();
		String fls[] = (String []) xfp.getFiles();
		String tname = fls[0];
		tname = tname.substring(7);
		String tnext = tname.substring(tname.length() - 3);
		System.out.println(tnext);
		if(tnext.equals("tex")){
			String dname = tname.substring(0, tname.length() - 4) + "dir";
			(new File(dname)).mkdir();
			File fl = new File(tname);
			String sname = tname;
			tname = dname + File.separator + fl.getName();
			FileInputStream in  = new FileInputStream(sname);
			byte b[] = Jexmt.streamToByteArray(in);
			String instr = new String(b);
			in.close();
			String esl = "\\\\";
			instr = instr.replaceAll(esl + "providecommand\\{" + esl + "tabularnewline\\}\\{" + esl + esl + "\\}", "");
			instr = instr.replaceAll(esl + "tabularnewline", esl + esl);
			instr = instr.replaceAll(" \\[", "[");
			instr = instr.replaceAll(" \\{", "{");
			instr = instr.replaceAll(esl + esl, esl + "," + esl + ", " + esl + esl);
    			FileOutputStream fos = new FileOutputStream(tname);
    			fos.write(instr.getBytes());
    			fos.close();
			String hhname = tname.substring(0, tname.length() - 3) + "html";
			String hname = "file://" + hhname;
			String exc = Jexconf.getProp("latex2html");
			String est = exc + " -split 0 -nomath -no_navigation -numbered_footnotes -show_section_numbers -nosubdir -noaddress ";
    			est = est + "-noinfo -html_version 2.0 " + tname;
			System.out.println(est);
			Runtime rt = Runtime.getRuntime();
			Process ps = rt.exec(est);
			estErrorStream oes = new estErrorStream(ps, orc);
			oes.start();
			InputStream is = ps.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
            		BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null) {
				orc.msg.setText(line);
				if(orc.cl.isCancelled){
						orc.jw.setVisible(false);
						return;
				}
			}
			ps.waitFor();

			//instr = instr.replaceAll(esl + esl, "");
			instr = instr.replaceAll("&", "&amp;");
			instr = instr.replaceAll("<", "&lt;");
			instr = instr.replaceAll(">", "&gt;");
			instr = instr.replaceAll(" \\[", "[");
			instr = instr.replaceAll(" \\{", "{");
			instr = instr.replaceAll("\\|", "\\\\vert");
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int ptr = 0;
			int next = 0;
			String lbl = "\\label{";
			while(ptr < instr.length()){
				next = instr.indexOf(lbl, ptr);
				boolean done = false;
				if(next >= 0){
					baos.write(instr.substring(ptr, next).getBytes());
					ptr = next + lbl.length();
					next = instr.indexOf("}", ptr);
					if(next >= 0) {
						ptr = next + 1;
						baos.write("\n".getBytes());
					}
					else done = true;
				}
				else done = true;
				if(done) {
					baos.write(instr.substring(ptr).getBytes());
					ptr = instr.length();
				}					
			}
			instr = new String(baos.toByteArray());	
			if(true){
    				fos = new FileOutputStream(tname + ".txt");
    				fos.write(instr.getBytes());
    				fos.close();
			}
			in = new FileInputStream(hhname);
			b = Jexmt.streamToByteArray(in);
			String hinstr = new String(b);
			in.close();
			hinstr = hinstr.replaceAll(" \\[", "[");
			hinstr = hinstr.replaceAll(" \\{", "{");
			FileOutputStream out = new FileOutputStream(hhname);
			ptr = 0;
			int hptr = 0;
			while(hptr < hinstr.length()){
				next = hinstr.indexOf("<IMG", hptr);
				if(next < 0) next = hinstr.length();
				out.write(hinstr.substring(hptr, next).getBytes());
				hptr = next;
				next = hinstr.indexOf(">", hptr);
				if(next < 0) out.write(hinstr.substring(hptr, hinstr.length()).getBytes());
				else{
					String imstr = hinstr.substring(hptr, next);
					String fbegin = "";
					String f1 = "";
					String f2 = "";
					String fend = "";
					String tl = "";
					hptr = next;
					int nalt = imstr.indexOf("ALT=\"");
					if(nalt >= 0){
						nalt = nalt + 5;
						String lmk = "% latex2html id marker";
						int nbeg = imstr.indexOf(lmk, nalt);
						if(nbeg >= 0){
							String strt = "";
							if(nbeg > 0) strt = imstr.substring(0, nbeg);
							imstr = strt + imstr.substring(nbeg + lmk.length() + 4);
						}
						nbeg = imstr.indexOf("\\begin{", nalt);
						String foo = "zfoobarfoozel88";
						if(nbeg >= 0){
							int onalt = nalt;
							nalt = nbeg + 7;
							nbeg = imstr.indexOf("}", nalt);
							if(nbeg >= 0) {
								foo = imstr.substring(nalt, nbeg).trim();
								if(foo.startsWith("math") || foo.startsWith("displaymath") || foo.startsWith("equation") || foo.startsWith("align") || foo.startsWith("eqnarray") || foo.startsWith("multline") || foo.startsWith("split") || foo.startsWith("gather") || foo.startsWith("flalign"))nalt = nbeg + 1;
								else {
									foo = "zfoobarfoozel88";
									nalt = onalt;
								}
							}
						}
						f1 = imstr.substring(nalt);
						imstr = imstr.substring(0, nalt);
						nbeg = f1.indexOf("\\end{" + foo);
						if(nbeg < 0) nbeg = f1.indexOf("% ");
						if(nbeg >= 0){
							tl = f1.substring(nbeg);
							f1 = f1.substring(0, nbeg);
							f1 = f1.trim();
							String f3 = "";
							nbeg = f1.indexOf("...\n...");
							if(nbeg >= 0){
								f2 = f1.substring(nbeg + 7);
								f3 = f2.trim();
								f1 = f1.substring(0, nbeg);
							}
							int nxt = instr.indexOf(f1, ptr);
							if(nxt < 0) nxt = instr.indexOf(f1);
							if(nbeg >= 0){
								if(nxt >= 0){
									ptr = nxt;
									nxt = instr.indexOf(f3, ptr + f1.length());
									if(nxt >= 0) {
										f1 = instr.substring(ptr, nxt + f3.length());
										ptr = ptr + f1.length();
									}
									else {
										f1 = f1 + "...\n..." + f2;
										ptr = ptr + f1.length();
										System.out.println("Could not find end: " + f1);
									}
								}
								else {
									f1 = f1 + "...\n..." + f2;
									System.out.println("Could not find start: " + f1);
								}
							}
							else{
								if(nxt >= 0) ptr = nxt + f1.length();
								else System.out.println("Could not find: " + f1);
							}
						}
					}
					imstr = imstr + fbegin + f1 + tl + fend;
					imstr = imstr.replaceAll(esl +"," + esl + ",", "");
					out.write(imstr.getBytes());
				}
			}
			out.close();

			PropertyValue pv[] = new com.sun.star.beans.PropertyValue[1];
			cpv = pv;
			setProp(0, "FilterName", "HTML (StarWriter)");
 			XComponentLoader xcl = (XComponentLoader)UnoRuntime.queryInterface(XComponentLoader.class, xd);
			xcl.loadComponentFromURL(hname,"_blank",0,pv);
		}
		else System.out.println("cancel");
	}
	catch(Exception ex){ex.printStackTrace();}
	System.out.println("Done with TeX import.");
	orc.jw.setVisible(false);
    }

    public void convertToTex0(){
	orc.msg.setText("Starting conversion");
	getCurrent();
	XStorable xm = (XStorable) UnoRuntime.queryInterface(XStorable.class, xcc);

	String url = xm.getLocation();
	String ourl = null;
	String surl = null;
	String sfile = null;

	try{
		File ofile = File.createTempFile("jex", ".odt");
		ofile.deleteOnExit();
		ourl = "file://" + ofile.toURL().getPath();
		File fsfile = File.createTempFile("jex", ".sxw");
		fsfile.deleteOnExit();
		surl = "file://" + fsfile.toURL().getPath();
		sfile = fsfile.getAbsolutePath();
	}
	catch(Exception ex){ex.printStackTrace();}

	String tfile = (new File(url.substring(7,url.length() - 3))).getAbsolutePath();
	tfile = tfile + "tex";

	orc.msg.setText("Saving " + ourl);
	PropertyValue pv[] = new com.sun.star.beans.PropertyValue[5];
	cpv = pv;
	setProp(0, "URL", ourl);
	setProp(1, "FilterName", "writer8");
	setProp(4, "SelectionOnly", true);
	xdh.executeDispatch(xdp, ".uno:SaveAs", "", 0, pv);
	
	XTextGraphicObjectsSupplier xgos = (XTextGraphicObjectsSupplier)UnoRuntime.queryInterface(XTextGraphicObjectsSupplier.class, xcc);
	XNameAccess xna = xgos.getGraphicObjects();
	String orps[] = xna.getElementNames();
	etype = "equation";
	fixGraphs(orps, xna);

	XTextEmbeddedObjectsSupplier xeos = (XTextEmbeddedObjectsSupplier)UnoRuntime.queryInterface(XTextEmbeddedObjectsSupplier.class, xcc);
	xna = xeos.getEmbeddedObjects();
	orps = xna.getElementNames();
	etype = "mathtype";
	fixGraphs(orps, xna);

	orc.msg.setText("Saving " + surl);
	setProp(0, "URL", surl);
	setProp(1, "FilterName",  "StarOffice XML (Writer)");
	xdh.executeDispatch(xdp, ".uno:SaveAs", "", 0, pv);

	String s[] = null;
	String jexxml = Jexconf.getProp("jex.xml");
	if(jexxml.equals("null")){	
		s = new String[2];
		s[0] = sfile;
		s[1] = tfile;
	}
	else{
		s = new String[4];
		s[0] = "-config";
		s[1] = jexxml;
		s[2] = sfile;
		s[3] = tfile;
	}
	writer2latex.Application w2l = new writer2latex.Application();
	orc.msg.setText("Converting .sxw to .tex");
	w2l.main(s);

	try{
		FileInputStream in = new FileInputStream(tfile);
		byte b[] = Jexmt.streamToByteArray(in);
		String instr = new String(b);
		in.close();
		FileOutputStream out = new FileOutputStream(tfile);
		int ptr = 0;
		int n = instr.indexOf(jstr);
		while(n > ptr){
			orc.msg.setText("Decoding equation at " + ptr);
			String outstr = instr.substring(ptr, n);
			ptr = n + jstr.length();
			n = instr.indexOf("gg", ptr);
			if(n > ptr){
				String cstr = instr.substring(ptr, n);
				outstr = outstr + makeUnSafe(cstr);
				ptr = n + 2;
			}
			out.write(outstr.getBytes());
			n = instr.indexOf(jstr, ptr);
		}
		if(ptr < instr.length()) out.write(instr.substring(ptr, instr.length()).getBytes());
		out.close();			
	}
	catch(Exception ex){ex.printStackTrace();}
	System.out.println("Done with math conversion.");
	orc.jw.setVisible(false);
    } 
	

    public void unzipTo(String zipfile, String target, boolean isUri){
	//System.out.println(zipfile + " " + target);
	try{
		FileInputStream in = new FileInputStream(zipfile);
		ZipInputStream zip = new ZipInputStream(in);
		int nmax = 10000000; 
		byte b[] = new byte[nmax];
		for(ZipEntry ze = zip.getNextEntry(); ze != null; ze = zip.getNextEntry()){
			int nread = 0;
			int tread = 0;
			while((tread = zip.read(b, nread, nmax - nread - 1)) >= 0){
				nread = nread + tread;
			}
			File wfile = null;
			if(isUri) {
				String towrite = target + "/" + ze.getName();
				URI uri = new URI(towrite);
				wfile = new File(uri);
			}
			else {
				wfile = new File(target + File.separator + ze.getName());
			}
			FileOutputStream out = new FileOutputStream(wfile);
			out.write(b, 0, nread);
			out.close();
		}		
	}
	catch(Exception ex){System.err.println(ex);}
    }

    public void installOffice(){
		officeLoc();
		URI uri = null;
		try {uri = new URI(userLoc + "/jexpath.txt");}
		catch(Exception e){System.err.println(e);}
		File jexpath = new File(uri);
		String javaexec = javaLoc + "/bin/java";
		File test = new File(javaexec);
		if(!test.exists()) {
			javaexec = "file:///" + javaexec + ".exe";
			javaexec = javaexec.replaceAll(" ", "%20");
			javaexec = javaexec.replace('\\', '/');
			javaexec = javaexec;
		}
 		javaexec = javaexec + " -jar " + jexexec + " --server";
		try{
			FileOutputStream out = new FileOutputStream(jexpath);
			out.write(javaexec.getBytes());
			out.close();
		}
		catch(IOException ex){};
    }

    public void installFonts(){
	officeLoc();
	unzipTo(euclid, shareLoc + "/../fonts/truetype", true);
    }
    public void installJavaFonts(){
	unzipTo(euclid, javaLoc + File.separator + "lib" + File.separator + "fonts", false);
    }
    public boolean queryExit(){
		if(!server) return true;
		checkExit = true;
		return false;
    }

    public void officeLoc(){
	if(userLoc == ""){
		connect();
		XMultiComponentFactory xsvcm = xcctxt.getServiceManager();
		try {
			Object pathSubst = xsvcm.createInstanceWithContext("com.sun.star.comp.framework.PathSettings", xcctxt);
			XPropertySet xPathPS = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, pathSubst);
			userLoc = (String) xPathPS.getPropertyValue("UserConfig");
			shareLoc = (String) xPathPS.getPropertyValue("Config");
		}
		catch(Exception exception){System.err.println("officeLoc exception: " + exception);}
	}
    }

    public static boolean connectLock = false;
    
    public static void connect(){
        if(connectLock) return;
	connectLock = true;
        try{    
    // chekanov
        	/*
        	xcctxt = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
            XMultiComponentFactory xmcf = xcctxt.getServiceManager();
            Object objectUrlResolver = xmcf.createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", xcctxt);
            XUnoUrlResolver xurlr = (XUnoUrlResolver) UnoRuntime.queryInterface( XUnoUrlResolver.class, objectUrlResolver );
            Object object0 = xurlr.resolve(connectStr);
            xmcf = (XMultiComponentFactory)  UnoRuntime.queryInterface(XMultiComponentFactory.class, object0);
            XPropertySet xpsmcf = (XPropertySet) UnoRuntime.queryInterface(XPropertySet.class, xmcf);
            Object odc = xpsmcf.getPropertyValue("DefaultContext");
            xcctxt = (XComponentContext) UnoRuntime.queryInterface(XComponentContext.class, odc);
            xd = (XDesktop) UnoRuntime.queryInterface(XDesktop.class, xmcf.createInstanceWithContext("com.sun.star.frame.Desktop", xcctxt));
            xdh = (XDispatchHelper) UnoRuntime.queryInterface(XDispatchHelper.class, xmcf.createInstanceWithContext("com.sun.star.frame.DispatchHelper", xcctxt));
      */
        }
        catch(Exception exception) {
            System.err.println("connect exception: " +  exception);
	    exception.printStackTrace();
	    xd = null;
        }
	connectLock = false;
    }
    
    Object objectS;
    XTextViewCursor xvc;
    XComponent xcc;
    XTextDocument xdoc;
    XMultiServiceFactory xmsf;
    XModel xm;
    XController xc2;
    XFrame xgf;
    XDispatchProvider xdp;

    
    void getCurrent(){
	if(xd == null) connect();
	try{
        	xgf = xd.getCurrentFrame();
	}
	catch(Exception e){
		System.err.println("getCurrent exception: " + e);
		e.printStackTrace();
		xd = null;
		connect();
		if(xd == null) return;
		xgf = xd.getCurrentFrame();
	}
        XController xc = xgf.getController();
        XSelectionSupplier xss = (XSelectionSupplier) UnoRuntime.queryInterface(XSelectionSupplier.class, xc);
	if(xss == null) System.out.println("no selection");
        objectS = xss.getSelection();
        xcc = xd.getCurrentComponent();
	xdoc = (XTextDocument)UnoRuntime.queryInterface(XTextDocument.class, xcc);
        xmsf = (XMultiServiceFactory) UnoRuntime.queryInterface(XMultiServiceFactory.class, xcc);
        xm = (XModel) UnoRuntime.queryInterface(XModel.class, xcc);
        xc2 = xm.getCurrentController();
        XTextViewCursorSupplier xtvcs = (XTextViewCursorSupplier) UnoRuntime.queryInterface(XTextViewCursorSupplier.class, xc2);
        xvc = xtvcs.getViewCursor();
	xdp = (XDispatchProvider) UnoRuntime.queryInterface(XDispatchProvider.class, xc2);
    }
    
    public static void main(String args[]) {
	Jex.officeclass = new Jexoo();
        connect();
	ooListen ool = new ooListen();
	ool.start();
        Jex jex = Jex.initJ(args);
	for(int ii = 0; ii < args.length; ii++) {
		if(args[ii].trim().equals("--server")) {
			server = true;
			ooCheckExit ooce = new ooCheckExit();
			ooce.start();
		}
	}
	Jexoo jexoo = new Jexoo(jex);
	jexoo.setJex(jex);
	if(server) jexoo.grabIn(jex, jexoo);
	//jex.gJexFrame.setState(java.awt.Frame.ICONIFIED);
	javaLoc = System.getProperty("java.home");
	String os = System.getProperty("os.name").toLowerCase() + "xxxxxxx";
	try {
		String url = Jexoo.class.getResource("Jexoo.class").toString();
		url = url.replaceAll("jar:", "");
		url = url.replaceAll("!/jex/Jexoo.class", "");
		URI uri = new URI(url);
		File jexexecf = new File(uri);
		String quotes = "";
		if(os.substring(0,7).equals("windows")) quotes = "\"";
		String confjex = jexexecf.getAbsolutePath().replaceAll("ekit.jar", "conf.jex");
		euclid = jexexecf.getAbsolutePath().replaceAll("ekit.jar", "euclid.zip");
		jexexec = quotes + jexexecf.getAbsolutePath() + quotes + " " + quotes + confjex + quotes;
	}
	catch(Exception e){System.err.println(e);}
    }

    public void setJex(Jex jex){
	jex.setMathDisplay(this);
	jex.setOffice(this);
    }
   
    XTextFrame xf;
    XShapes xss;
    int fsize;
    int hf = 35;
    int vf = 35;
    int base;
    PropertyValue[] cpv;

    private void setProp(int i, String name, String value){
		cpv[i] = new com.sun.star.beans.PropertyValue();
		cpv[i].Name = name;
		cpv[i].Value = value;
    }

    private void setProp(int i, String name, int value){
		cpv[i] = new com.sun.star.beans.PropertyValue();
		cpv[i].Name = name;
		cpv[i].Value = new Integer(value);
    }

    private void setProp(int i, String name, boolean value){
		cpv[i] = new com.sun.star.beans.PropertyValue();
		cpv[i].Name = name;
		cpv[i].Value = new Boolean(value);
    }

    private boolean findview(String str){
		return findview(str, true);
    }

    private boolean findview(String str, boolean backdir){
		PropertyValue pv[] = new com.sun.star.beans.PropertyValue[19];
		cpv = pv;
		setProp(0, "SearchItem.StyleFamily", 2);
		setProp(1, "SearchItem.CellType", 0);
		setProp(2, "SearchItem.RowDirection", true);
		setProp(3, "SearchItem.AllTables", false);
		setProp(4, "SearchItem.Backward", backdir);
		setProp(5, "SearchItem.Pattern", false);
		setProp(6, "SearchItem.Content", false);
		setProp(7, "SearchItem.AsianOptions", false);
		setProp(8, "SearchItem.AlgorithmType", 0);
		setProp(9, "SearchItem.SearchFlags", 65536);
		setProp(10, "SearchItem.SearchString", str);
		setProp(11, "SearchItem.ReplaceString", "");
		setProp(12, "SearchItem.Locale", 255);
		setProp(13, "SearchItem.ChangedChars", 2);
		setProp(14, "SearchItem.DeletedChars", 2);
		setProp(15, "SearchItem.InsertedChars", 2);
		setProp(16, "SearchItem.TransliterateFlags", 1280);
		setProp(17, "SearchItem.Command", 0);
		setProp(18, "Quiet", true);  
		Object bb = xdh.executeDispatch(xdp, ".uno:ExecuteSearch", "", 0, pv);
		DispatchResultEvent b = (DispatchResultEvent) bb;
		Boolean bbb = (Boolean)b.Result;
		//if(!(b.State == DispatchResultState.SUCCESS)){
		if(!bbb.booleanValue()){
			setProp(4, "SearchItem.Backward", !backdir);
			b = (DispatchResultEvent) xdh.executeDispatch(xdp, ".uno:ExecuteSearch", "", 0, pv);
			bbb = (Boolean)b.Result;
			boolean br = bbb.booleanValue();
			//boolean br = false;
			//if(b.State == DispatchResultState.SUCCESS) br = true;
			return(br); 
		}		
		return true;
    }

    private String tfile;

    public void printEq(String tex, DefaultContext d){

	getCurrent();
	try{
		if(jexname != null){
			boolean found = false;
			XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, objectS);
			if(xs != null){
				XPropertySet xsp = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, objectS);
				String str = (String) xsp.getPropertyValue("LinkDisplayName");
				if(str.equals(jexname)) found = true;
			}
			XTextContent xt;
			if(!found){	
				XPropertySet xsp;	
				XTextGraphicObjectsSupplier xgos = (XTextGraphicObjectsSupplier)UnoRuntime.queryInterface(XTextGraphicObjectsSupplier.class, xcc);
				XNameAccess xna = xgos.getGraphicObjects();
				Object something = null;
				try{
					something = xna.getByName(jexname);
				}
				catch(Exception ex){
					XTextEmbeddedObjectsSupplier xeos = (XTextEmbeddedObjectsSupplier)UnoRuntime.queryInterface(XTextEmbeddedObjectsSupplier.class, xcc);
					xna = xeos.getEmbeddedObjects();
					something = xna.getByName(jexname);
				}		
				xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, something);
			}
			else {
				xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, objectS);
				xdh.executeDispatch(xdp, ".uno:Escape", "", 0, null);
			}
      			XText xdt = xdoc.getText();
			XTextRange xtr = xt.getAnchor();
			XTextCursor xma = xdt.createTextCursorByRange(xtr);
        		xma.goLeft((short)1, false);
			xma.goRight((short)1, false);
			String tname = getName();
			xdt.insertString(xma, tname, true);
			findview(tname);
			xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);
			xdt.insertString(xma, "", true);
			xvc.goRight((short) 1, true);
		}	
		insertEq(tex, d);
	}
	catch(Exception exception){System.err.println(
		"printEq: " + exception);
		exception.printStackTrace();
		xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);
		insertEq(tex, d);
	}
    }

private void insertGraphic(String tfile, String atext){
        try{
		getCurrent();
		XTextContent xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, objectS);
		if(xt != null) xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);

            	XText xdt = xvc.getText();
           	XTextCursor xma = xdt.createTextCursorByRange(xvc.getStart());

		XPropertySet xspt = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xma);

		xdh.executeDispatch(xdp, ".uno:InsertPara", "", 0, null);
		xdh.executeDispatch(xdp, ".uno:InsertPara", "", 0, null);
		PropertyValue pvx[] = new com.sun.star.beans.PropertyValue[2];
		cpv = pvx;
		setProp(0, "Count", 1);
		setProp(1, "Select", false);
		xdh.executeDispatch(xdp, ".uno:GoLeft", "", 0, pvx);
 
		PropertyValue pv[] = new com.sun.star.beans.PropertyValue[4];
		cpv = pv;
		setProp(0, "FileName", tfile);
		setProp(1, "FilterName", "<All formats>");
		setProp(2, "AsLink", false);
		setProp(3, "Style", "Graphics");
		xdh.executeDispatch(xdp, ".uno:InsertGraphic", "", 0, pv);

		XPropertySet xsp = getProps();
		XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, objectS);
		xdh.executeDispatch(xdp, ".uno:Escape", "", 0, null);		

		xsp.setPropertyValue("AlternativeText", atext);
	}
        catch( Exception exception ) {
            System.err.println("insertGraphic exception: " + exception );
	    exception.printStackTrace();
        }

    }

    private XPropertySet getProps(){
		int nlim = 10;
		int ntim = 1000;
		XPropertySet xsp = null;
		getCurrent();
		for(int ii = 0; ii < nlim; ii++){
			xsp = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, objectS);
			if(xsp != null) return xsp;
			try{Thread.sleep(ntim);}
			catch(Exception ex){ex.printStackTrace();}
			System.out.println("trying properties again..." + ii);
			getCurrent();
		}
		System.out.println("failed to get properties");
		return null;
   }

    private void insertEq(String tex, DefaultContext d){
        try{
		getCurrent();
		XTextContent xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, objectS);
		if(xt != null) xdh.executeDispatch(xdp, ".uno:Delete", "", 0, null);

            	XText xdt = xvc.getText();
           	XTextCursor xma = xdt.createTextCursorByRange(xvc.getStart());

		XPropertySet xspt = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xma);

		Jexwmf wmf = new Jexwmf();
		File tfilef = File.createTempFile("jex", ".wmf");
		tfilef.deleteOnExit();
		tfile = tfilef.getAbsolutePath();
		wmf.tfile = tfile;
		Float xsptf = (Float) xspt.getPropertyValue("CharHeight");
		wmf.fsize0 = xsptf.floatValue();
		wmf.printEq(tex, d);

		xdt.insertString(xma, "a", true);

		com.sun.star.style.LineSpacing lso = (com.sun.star.style.LineSpacing) xspt.getPropertyValue("ParaLineSpacing");
		if(lso.Mode != 0){
			com.sun.star.style.LineSpacing ls = new com.sun.star.style.LineSpacing();
			ls.Mode = 0;
			ls.Height = lso.Height;
			xspt.setPropertyValue("ParaLineSpacing", ls);
		}

		xdh.executeDispatch(xdp, ".uno:InsertPara", "", 0, null);
		xdh.executeDispatch(xdp, ".uno:InsertPara", "", 0, null);
		PropertyValue pvx[] = new com.sun.star.beans.PropertyValue[2];
		cpv = pvx;
		setProp(0, "Count", 1);
		setProp(1, "Select", false);
		xdh.executeDispatch(xdp, ".uno:GoLeft", "", 0, pvx);
 
		PropertyValue pv[] = new com.sun.star.beans.PropertyValue[4];
		cpv = pv;
		setProp(0, "FileName", "file://" + tfile);
		setProp(1, "FilterName", "<All formats>");
		setProp(2, "AsLink", false);
		setProp(3, "Style", "Graphics");
		xdh.executeDispatch(xdp, ".uno:InsertGraphic", "", 0, pv);

		
		XPropertySet xsp = getProps();
		XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, objectS);
		
		xdh.executeDispatch(xdp, ".uno:SetAnchorToChar", "", 0, null);
		xdh.executeDispatch(xdp, ".uno:Escape", "", 0, null);		

		xsp.setPropertyValue("ContentProtected", new Boolean(true));
            	//xsp.setPropertyValue("AnchorType", TextContentAnchorType.AS_CHARACTER);
		xsp.setPropertyValue("AlternativeText", "jex149$tex=" + tex);
		jexname = (String) xsp.getPropertyValue("LinkDisplayName");
		jex.setTitle(jexname);

		BoxMetrics m = wmf.m;

		int hijink = -(int) (0.17f * (float) wmf.adj);
		float bbf = 1.17f;
		int bbase = (int) ((bbf * (float)(-m.baseline - m.edgeN - hijink)) + 0.5f);
		xsp.setPropertyValue("VertOrient", new Integer(3));
		xsp.setPropertyValue("VertOrientPosition", new Integer(bbase));

	    	//XPropertySetInfo xps = xsp.getPropertySetInfo();
            	//Property xgp[] = xps.getProperties();
            	//for (int jj = 0; jj < xgp.length; jj++){
		//	System.out.println(xgp[jj].Name + " " + xsp.getPropertyValue(xgp[jj].Name));
	    	//}

		xt = (XTextContent) UnoRuntime.queryInterface(XTextContent.class, objectS);
		XTextRange xtr = xt.getAnchor();
		xma = xdt.createTextCursorByRange(xtr);
            	xma.goLeft((short) 2, true);
		String test = xma.getString();
		if(!test.startsWith("a")) xma.goLeft((short) 1, true);
		xdt.insertString(xma, "", true);
		xma.goRight((short) 1, false);
		if(test.startsWith("a")){
			xma.goRight((short) 1, true);
			xdt.insertString(xma, "", true);
		}
		else {
			getCurrent();
			cpv = pvx;
			setProp(0, "Count", 1);
			setProp(1, "Select", false);
			xdh.executeDispatch(xdp, ".uno:GoRight", "", 0, pvx);
		}
		if(lso.Mode != 0) xspt.setPropertyValue("ParaLineSpacing", lso);

	}
        catch( Exception exception ) {
            System.err.println("insertEq exception: " + exception );
	    exception.printStackTrace();
        }

    }

    public void showChar(String str, String face, String style, int size, float ratio, float stretch, float aftr, int x, double y, BoxMetrics m, int vshift){}

    public void setName(String name){
	jexname = name;
    }

    public String getName(){
	GregorianCalendar cal = new GregorianCalendar();
	return "Jex$$" + cal.getTimeInMillis();
    }

    private String[] getTex(XShape xs){
	String tex = "";
	String name = "";
	String ans[] = new String[2];
	try{
	    	XPropertySet xsp = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xs);
		boolean mathtype = false;
		try{
            		tex = (String) xsp.getPropertyValue("AlternativeText");
		}
		catch(Exception ex){
			System.out.println("No alternative text, assuming mathtype equation.");
			mathtype = true;
		}
		if(!mathtype){
			int n = tex.indexOf("=");
			if((n > 0) && tex.startsWith("jex149")) tex = tex.substring(n+1);
			else {
				String texx=tex.trim();
				tex = "";
				if(texx.startsWith("$")) {
					texx = texx.substring(1);
					if(texx.startsWith("$")) texx = texx.substring(1);
					n = texx.indexOf("%");
					if(n > 0) texx = texx.substring(0, n);
					texx = texx.trim();
					if(texx.endsWith("$")) texx = texx.substring(0, texx.length() - 1);
					if(texx.endsWith("$")) texx = texx.substring(0, texx.length() - 1);
					tex = "{" + texx + "}";
				}
				else if(texx.startsWith("\\begin{")){
					int ptr = texx.indexOf("{");
					int ptr2 = texx.indexOf("}");
					if((ptr >= 0) && (ptr2 >= 0)){
						String ttype = texx.substring(ptr + 1, ptr2);
						boolean skip = false;
						if(ttype.startsWith("displaymath") || ttype.startsWith("math") || ttype.startsWith("equation")) skip = true;
						if(skip || ttype.startsWith("eqnarray") || ttype.startsWith("align") || ttype.startsWith("multline")){
							if(skip) texx = texx.substring(ptr2 + 1);
							String test = "\\end{" + ttype + "}";
							ptr = texx.indexOf(test);
							if(ptr >= 0){
								if(skip) texx = texx.substring(0, ptr);
								else texx = texx.substring(0, ptr + test.length());
								texx = texx.trim();
								tex = "{" + texx + "}";
							}
						}
					}
				}
			}
		}
		name = (String) xsp.getPropertyValue("LinkDisplayName");		
		if(mathtype) {
			XStorable xm = (XStorable) UnoRuntime.queryInterface(XStorable.class, xcc);
			String url = xm.getLocation();
			url = url.substring(7);
			RowBox rb = Jexmt.getMTData(url, name);
			if(rb == null){
				tex = "";
				System.out.println("Cannot get mathtype data. If this is a mathtype equation is the document saved as .odt?");
			}
			else tex = rb.toTex();
			System.out.println(tex);
		}
	}
	catch(Exception ex){ex.printStackTrace();}
	ans[0] = tex;
	ans[1] = name;
	return ans;
    }

    public void grabIn(Jex jex, Jexoo jexoo){
    	try {
	    getCurrent();
	    XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, objectS);
	    if(xs != null){
	    	XPropertySet xsp = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xs);
            	//String tex = (String) xsp.getPropertyValue("AlternativeText");
		String ans[] = getTex(xs);
		String tex = ans[0];
		String name = ans[1];
	    	jex.jexPane.setEqn(tex);
		jexoo.setName(name);
		jex.setTitle(name);
	    }
        }
        catch( Exception exception ) {
            System.err.println("grabIn: " + exception );
	    exception.printStackTrace();
        }
    }


    public void grabEq(){
    
        try {
            getCurrent(); 
	    XShape xs = (XShape) UnoRuntime.queryInterface(XShape.class, objectS);
	    if(xs == null){
		Jex jex = Jex.newJex("{}");
		Jexoo jexoo = new Jexoo(jex);
		jexoo.setJex(jex);
	    }
	    else{
	    	XPropertySet xsp = (XPropertySet)UnoRuntime.queryInterface(XPropertySet.class, xs);
		String ans[] = getTex(xs);
		String tex = ans[0];
		String name = ans[1];
	    	Jex jex = Jex.newJex(tex);
		Jexoo jexoo = new Jexoo(jex);
		jexoo.setName(name);
		jexoo.setJex(jex);
		jex.setTitle(name);
	    }
        }
        catch( Exception exception ) {
            System.err.println("grabEq: " + exception);
	    exception.printStackTrace(); 
        }
    }
}

class estErrorStream extends Thread {
	private Process ps;
	private ooRunConvert orc;
	public estErrorStream(Process ps, ooRunConvert orc){
		this.ps = ps;
		this.orc = orc;
	}
	
	public void run(){
		InputStream is = ps.getErrorStream();
		InputStreamReader isr = new InputStreamReader(is);
            	BufferedReader br = new BufferedReader(isr);
		String line;
		try{
			while ((line = br.readLine()) != null) {
				orc.msg.setText(line);
				if(orc.cl.isCancelled){
						orc.jw.setVisible(false);
						return;
				}
			}
			ps.waitFor();
		}
		catch(Exception ex){ex.printStackTrace();}
	}
}

class ooRunConvert extends Thread {
	Jexoo jexoo;
	public JDialog jw;
	public JLabel msg = new JLabel();
	public cancelListener cl;
	private String title;
	private String op;
	public void setJexoo(Jexoo jxo){
		jexoo = jxo;
	}

	public ooRunConvert(String title, String op){
		this.title = title;
		this.op = op;
	}

	private void setCenter(Component c){
		Frame f = jexoo.jex.gJexFrame;
		int w = f.getWidth();
		int h = f.getHeight();
		int x = f.getX();
		int y = f.getY();
		int ww = c.getWidth();
		int hh = c.getHeight();
		c.setLocation(x + ((w - ww) /2), y + ((h - hh) /2));
	}

	public void run(){
		jw = new JDialog(jexoo.jex.gJexFrame, title, false);
		Container cp = jw.getContentPane();
		cp.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		cp.add(msg, c); 
		c.gridy = 1;
		cp.add(new JLabel(" "), c);
		c.gridy = 2;
		JButton item = new JButton("Cancel");
		cl = new cancelListener();
		item.addActionListener(cl);
		cp.add(item, c);
            	jw.setSize(new Dimension(300, 110));
		setCenter(jw);
            	jw.setVisible(true);
		if(op.equals("convertToTex")) jexoo.convertToTex0();
		if(op.equals("updateEquations")) jexoo.updateEquations0();
		if(op.equals("convertFromTex")) jexoo.convertFromTex0();
	}
}

class cancelListener implements ActionListener {
	public boolean isCancelled = false;

	public void actionPerformed(ActionEvent e){
		isCancelled = true;
	}
}

class ooCheckExit extends Thread {

	public void run() {
		while(true){
			if(Jexoo.checkExit){
				if(Jex.nWins > 0) Jexoo.checkExit = false;
				else {
					Jexoo.connect();
					if(Jexoo.xd == null) {
						System.exit(0);
					}
				}
			}
			try{
				sleep(60000);
			}
			catch(Exception exception){
				System.err.println("ooCheckExit exception: " + exception);
			}
		}
	}
}
		

class ooListen extends Thread {

	Jexoo jexoo;

	private String getMsg(XConnection xc){
		byte b[][]= new byte[1][10];
		try {xc.read(b, 5);}
		catch(Exception e){System.out.println("error getting message: "); System.err.println(e);}
		return new String(b[0]);
	}

	public void run () {
	
		while(true){

		try{

            		XComponentContext xcc = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
            		XMultiComponentFactory xmsf = xcc.getServiceManager();
			Object aa = xmsf.createInstanceWithContext("com.sun.star.connection.Acceptor", xcc);
			XAcceptor xa = (XAcceptor) UnoRuntime.queryInterface(XAcceptor.class, aa);
			while(true) {
				jexoo = new Jexoo();
				XConnection xc = xa.accept("socket,host=localhost,port=8101");
				String msg = getMsg(xc);
				if(msg.equals("jexst")) {
					Jex jex = Jex.newJex("{}");
					jexoo = new Jexoo(jex);
					jexoo.setJex(jex);
					xc.write("jexcfpad".getBytes());
					xc.flush();
				}
				else jexoo.grabEq();
				xc.close();
			}
		}
		catch(Exception e){
			try{
				System.out.println("ooTryingToConnect exception" + e);
				XComponentContext xcc = com.sun.star.comp.helper.Bootstrap.createInitialComponentContext(null);
            			XMultiComponentFactory xmsf = xcc.getServiceManager();
				Object bb = xmsf.createInstanceWithContext("com.sun.star.connection.Connector", xcc);
				XConnector xco = (XConnector) UnoRuntime.queryInterface(XConnector.class, bb);
				XConnection xc = xco.connect("socket,host=localhost,port=8101");
				xc.write("jexstpad".getBytes());
				xc.flush();
				String msg = getMsg(xc);
				if(msg.equals("jexcf")) System.exit(0);
				xc.close();
				//this.destroy();
			}
			catch(Exception ee){
				System.out.println("ooListen connect exception " + ee);
			}
		}
		try{
			sleep(2000);
		}
		catch(Exception exception){
			System.err.println("ooListen exception: " + exception);
		}

		}
	}
}



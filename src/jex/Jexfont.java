package jex;

import java.awt.*;
import java.util.*;

class Jexbinding{
	public String dface;
	public String gface;
	public String Gface;
	public String sface[];
	public Hashtable uniSym;
	public Hashtable uniFac;
	public Hashtable unigrk;
	public Hashtable uniGrk;
	public Hashtable uniArk;
	public double svpad = 0;
	public double gvpad = 0;
	public double Gvpad = 0;
	public float aftr = 0;
	public float sfixangle = 0;
	public float gfixangle = 0;
	public float Gfixangle = 0;
	public float srescale = 1.0f;
	public float grescale = 1.0f;
	public float Grescale = 1.0f;
	public float binpad = 0.0f;
        public float slpad = 0.1f;
        public float srpad = 0.2f;
        public float shpad = 0.2f;
}

public class Jexfont{

        static private Jexbinding jbinding;
	static Graphics2D g;

	static private Jexbinding esstix;
	static private Jexbinding symbol;
	static private Jexbinding euclid;
	static private Jexbinding ooeuclid;
	static private Jexbinding opensymbol;
	static private Jexbinding jopensymbol;

	static public String printBinding = "ooeuclid";
	static public String editBinding = "";
	static public String printFace;
	static public String editFace;
	static public boolean mathItalic = true;
	static public boolean greekItalic = false;
	static public String bindingList[] = {"esstix", "symbol", "euclid", "ooeuclid", "opensymbol", "jopensymbol"};

	static public Hashtable uniSym;
	static public Hashtable uniFac;
	static public Hashtable unigrk;
	static Hashtable uniGrk;
	static Hashtable uniArk;

	static public void setPrintFace(String face){
		setFace(printBinding, face);
		printFace = face;
	}

	static public void setEditFace(String face){
		setFace(editBinding, face);
		editFace = face;
	}

	static private void setFace(String binding, String face){
		if(binding.equals("")) jbinding.dface = face;
            	if(binding.equals("esstix")) esstix.dface = face;
            	if(binding.equals("symbol")) symbol.dface = face;
	    	if(binding.equals("euclid")) euclid.dface = face;
	    	if(binding.equals("ooeuclid")) ooeuclid.dface = face;
		if(binding.equals("opensymbol")) opensymbol.dface = face;
		if(binding.equals("jopensymbol")) jopensymbol.dface = face;
	}
	
	static public void setGraphics(Graphics2D graphics){
		g = graphics;
	}
	
	static private FontMetrics getFM(int size){
		Font ff = new Font(jbinding.dface, Font.PLAIN, size);
		g.setFont(ff);
		return g.getFontMetrics();	
	}

	static int getHeight(int size){
		FontMetrics fm = getFM(size);
		return fm.getAscent();
	}
	
	static int getThinWidth(int size){
		FontMetrics fm = getFM(size);
		return fm.charWidth('[');
	}
	
	static int getWidth(int size){
		FontMetrics fm = getFM(size);
		return fm.charWidth('A');
	}
	
	static int getLeading(int size){
		FontMetrics fm = getFM(size);
		return fm.getDescent() + fm.getLeading();
	}
        
        static private void usp(int a, int b){
            uniSym.put(new Integer(a), new Integer(b));
        }
        
        static private void usf(int a, int b){
            uniFac.put(new Integer(a), new Integer(b));
        }
        
        static private void usg(int a, int b){
            unigrk.put(new Integer(a), new Integer(b));
        }
        
        static private void usG(int a, int b){
            uniGrk.put(new Integer(a), new Integer(b));
        }
        
        static private void usA(int a, int b){
            uniArk.put(new Integer(a), new Integer(b));
        }

        static public void init(){
        
	    esstix = new Jexbinding();
                esstix.dface = new String("Times New Roman");
                esstix.sface = new String[9];
                esstix.gface = new String("ESSTIXNine");
                esstix.Gface = new String("ESSTIXTen");
                esstix.sface[0] = new String("ESSTIXFour");
                esstix.sface[1] = new String("ESSTIXOne");
                esstix.sface[2] = new String("ESSTIXTwo");
                esstix.sface[3] = new String("ESSTIXThree");
                esstix.sface[4] = new String("ESSTIXFive");
                esstix.sface[5] = new String("ESSTIXSix");
                esstix.sface[6] = new String("ESSTIXNine");
                esstix.sface[7] = new String("ESSTIXTen");
                esstix.sface[8] = new String("ESSTIXFifteen");
		esstix.aftr = (float) 0.2;
                
                unigrk = new Hashtable();
                uniGrk = new Hashtable();
                uniArk = new Hashtable();
                uniSym = new Hashtable();
                uniFac = new Hashtable();
		esstix.unigrk = unigrk;
		esstix.uniGrk = uniGrk;
		esstix.uniArk = uniArk;
		esstix.uniSym = uniSym;
		esstix.uniFac = uniFac;

                usg(0x3b1, 0x61);
                usg(0x3b2, 0x62);
                usg(0x3c8, 0x6a); 
                usg(0x3b4, 0x64);
                usg(0x3b5, 0x65);
                usg(0x3d5, 0x66);
                usg(0x3b3, 0x67);
                usg(0x3b7, 0x68); 
                usg(0x3b9, 0x69);
                usg(0x3c6, 0x34); 
                usg(0x3ba, 0x6b);
                usg(0x3bb, 0x6c);
                usg(0x3bc, 0x6d);
                usg(0x3bd, 0x6e);
                usg(0x3bf, 0x6f);
                usg(0x3c0, 0x70);
                usg(0x3c7, 0x63); 
                usg(0x3c1, 0x72);
                usg(0x3c3, 0x73);
                usg(0x3c4, 0x74);
                usg(0x3c5, 0x79);
                usg(0x3c9, 0x77); 
                usg(0x3be, 0x78);
                usg(0x3b8, 0x71); 
                usg(0x3b6, 0x7a);   
            

                usA(0x3b1, 'A');
                usA(0x3b2, 'B');
                usG(0x3c8, 0x4a);
                usG(0x3b4, 0x44);
                usA(0x3b5, 'E');
                usG(0x3b3, 0x47);
                usA(0x3b7, 'H');
                usA(0x3b9, 'I');
                usG(0x3c6, 0x46);
                usA(0x3ba, 'K');
                usG(0x3bb, 0x4c);
                usA(0x3bc, 'M');
                usA(0x3bd, 'N');
                usA(0x3bf, 'O');
                usG(0x3c0, 0x50);
                usA(0x3c7,  'X');
                usA(0x3c1, 'P');
                usG(0x3c3, 0x53);
                usA(0x3c4, 'T');
                usG(0x3c5, 0x59);
                usG(0x3c9, 0x57);
                usG(0x3be, 0x58);
                usG(0x3b8, 0x51);
                usA(0x3b6, 'Z');                  
            
                

                usp(0x222b, 0x21); //int 45 is more curly
                usf(0x222b, 5);
                usp(0x2211, 0x3e); //sum  3e is lighter 2d is heavier
                usf(0x2211, 5);
                usp(0x220f, 0x2e); //prod 3f is lighter
                usf(0x220f, 5);
                usp(0x22c5, 0x24); //cdot 25 is larger dot
                usf(0x22c5, 4);
                usp(0x00b1, 0x47); //pm
                usf(0x00b1, 4);
                
                //ESSTIXFour
                usp(0x2208, 0x32); //in
                usp(0x220b, 0x48); //ni
                usp(0x2209, 0x3b); //notin
                usp(0x2260, 0x73); //neq
                usp(0x2282, 0x33); //subset
                usp(0x2286, 0x34); //subseteq
                usp(0x2283, 0x49); //supset
                usp(0x2287, 0x4a); //supseteq
                usp(0x2026, 0x2e); //ldots
                usp(0x2261, 0x68); //equiv
                usp(0x2248, 0x7a); //approx
                
                usp(0x2264, 0x25); //leq
                usf(0x2264, 3);
                usp(0x2265, 0x52); //geq
                usf(0x2265, 3);
                usp(0x2229, 0x68); //cap
                usf(0x2229, 2); 
                usp(0x222a, 0x67); //cup
                usf(0x222a, 2);
                usp(0x2192, 0x2f); //rightarrow
                usf(0x2192, 1);
                usp(0x2190, 0x29); //leftarrow
                usf(0x2190, 1);
                usp(0x2207, 0x56); //grad
                usf(0x2207, 7);
                usp(0x2202, 0x76); //partial
                usf(0x2202, 6);
                usp(0x221e, 0x4e); //infinity
                usf(0x221e, 3);
                usp(0x2205, 0x5c); //emptyset 5b is script lowercase l
                usf(0x2205, 4);
                
                usp(0x211c, 0x52); //real
                usf(0x211c, 8);
                
                usp(0x221d, 0x66); //propto
                usf(0x221d, 3);
                usp(0x00b7, 0x25); //bullet
                usf(0x00b7, 4);
		usp(0x00d7, 0x21); //times
		usf(0x00d7, 4);

	    symbol = new Jexbinding();
	    euclid = new Jexbinding();
	    ooeuclid = new Jexbinding();
                symbol.dface = new String("Nimbus Roman");
		euclid.dface = new String("Euclid");
		ooeuclid.dface = new String("Euclid");
                symbol.sface = new String[1];
		euclid.sface = new String[1];
		ooeuclid.sface = new String[1];
                symbol.gface = new String("Symbol");
		euclid.gface = new String("Euclid Symbol");
		ooeuclid.gface = new String("Euclid Symbol");
                symbol.Gface = new String("Symbol");
		euclid.Gface = new String("Euclid Symbol");
		ooeuclid.Gface = new String("Euclid Symbol");
		symbol.sface[0] = new String("Symbol");
                euclid.sface[0] = new String("Euclid Symbol");
                ooeuclid.sface[0] = new String("Euclid Symbol");
		ooeuclid.svpad = 0.31f;
		ooeuclid.gvpad = 0.05f;
		euclid.binpad = 0.3f;
		ooeuclid.binpad = 0.28f;
		euclid.srpad = 0.0f;
		ooeuclid.srpad = 0.0f;
		//euclid.grescale = 1.25f;
		//euclid.Grescale = euclid.grescale;
		//euclid.srescale = euclid.grescale;
		symbol.svpad = 0.18;
		symbol.aftr = -0.2f;

                unigrk = new Hashtable();
                uniGrk = new Hashtable();
                uniArk = new Hashtable();
                uniSym = new Hashtable();
                uniFac = new Hashtable();

		symbol.unigrk = unigrk;
		symbol.uniGrk = uniGrk;
		symbol.uniArk = uniArk;
		symbol.uniSym = uniSym;
		symbol.uniFac = uniFac;

		euclid.unigrk = unigrk;
		euclid.uniGrk = uniGrk;
		euclid.uniArk = uniArk;
		euclid.uniSym = uniSym;
		euclid.uniFac = uniFac;

		ooeuclid.unigrk = unigrk;
		ooeuclid.uniGrk = uniGrk;
		ooeuclid.uniArk = uniArk;
		ooeuclid.uniSym = uniSym;
		ooeuclid.uniFac = uniFac;

                usg(0x3b1, 0x61);
                usg(0x3b2, 0x62);
                usg(0x3c8, 0x6a); 
                usg(0x3b4, 0x64);
                usg(0x3b5, 0x65);
                usg(0x3d5, 0x66);
                usg(0x3b3, 0x67);
                usg(0x3b7, 0x68); 
                usg(0x3b9, 0x69);
                usg(0x3c6, 0x6a); 
                usg(0x3ba, 0x6b);
                usg(0x3bb, 0x6c);
                usg(0x3bc, 0x6d);
                usg(0x3bd, 0x6e);
                usg(0x3bf, 0x6f);
                usg(0x3c0, 0x70);
                usg(0x3c7, 0x63); 
                usg(0x3c1, 0x72);
                usg(0x3c3, 0x73);
                usg(0x3c4, 0x74);
                usg(0x3c5, 0x79);
                usg(0x3c9, 0x77); 
                usg(0x3be, 0x78);
                usg(0x3b8, 0x71); 
                usg(0x3b6, 0x7a);             

                usA(0x3b1, 'A');
                usA(0x3b2 ,'B');
                usG(0x3c8, 0x4a);
                usG(0x3b4, 0x44); 
                usA(0x3b5, 'E');
                usG(0x3b3, 0x47);
                usA(0x3b7, 'H');
                usA(0x3b9, 'I');
                usG(0x3c6, 0x46);
                usA(0x3ba, 'K');
                usG(0x3bb, 0x4c);
                usA(0x3bc, 'M');
                usA(0x3bd, 'N');
                usA(0x3bf, 'O');
                usG(0x3c0, 0x50);
                usA(0x3c7,  'X');
                usA(0x3c1, 'P');
                usG(0x3c3, 0x53);
                usA(0x3c4, 'T');
                usG(0x3c5, 0x59);
                usG(0x3c9, 0x57);
                usG(0x3be, 0x58);
                usG(0x3b8, 0x51);
                usA(0x3b6, 'Z'); 
            
        

                usp(0x222b, 0xf2);
                usp(0x2211, 0xe5);
                usp(0x220f, 0xd5);
                usp(0x22c5, 0xd7);
                usp(0x00b1, 0xb1);
                usp(0x2208, 0xce);
                usp(0x220b, 0x27);
                usp(0x2209, 0xcf);
                usp(0x2264, 0xa3);
                usp(0x2265, 0xb3);
                usp(0x2260, 0xb9);
                usp(0x2282, 0xcc);
                usp(0x2286, 0xcd);
                usp(0x2283, 0xc9);
                usp(0x2287, 0xca);
                usp(0x2229, 0xc7);
                usp(0x222a, 0xc8);
                usp(0x2192, 0xae);
                usp(0x2190, 0xac);
                usp(0x2207, 0xd1);
                usp(0x2202, 0xb6);
                usp(0x221e, 0xa5);
                usp(0x2205, 0xc6);
                usp(0x211c, 0xc2);
                usp(0x2026, 0xbc);
                usp(0x221d, 0xb5);
                usp(0x2261, 0xba);
                usp(0x2248, 0xbb);
                usp(0x00b7, 0xb7);

		usp(0x2112, 0x3f); //upper script L
		usp(0x2113, 0x3f); //lower script L
		usp(0xe090, 0x3f); //norm

	    opensymbol = new Jexbinding();
	    jopensymbol = new Jexbinding();
                opensymbol.dface = new String("Lucida Bright");
                opensymbol.sface = new String[2];
                opensymbol.gface = new String("OpenSymbol");
		opensymbol.Gface = new String("OpenSymbol");
		opensymbol.sface[0] = new String("OpenSymbol");
		opensymbol.sface[1] = new String("Lucida Bright");
		opensymbol.gvpad = -0.03;
		opensymbol.svpad = 0.18;
		//opensymbol.aftr = (float) 0.1;
		opensymbol.gfixangle = (float) 0.2;

                jopensymbol.dface = new String("Lucida Bright");
                jopensymbol.sface = new String[1];
                jopensymbol.gface = new String("OpenSymbol");
		jopensymbol.Gface = new String("OpenSymbol");
		jopensymbol.sface[0] = new String("OpenSymbol");
		//jopensymbol.gvpad = -0.03;
		//jopensymbol.svpad = 0.18;
		//jopensymbol.aftr = (float) 0.1;
		jopensymbol.gfixangle = (float) 0.2;

                unigrk = new Hashtable();
                uniGrk = new Hashtable();
                uniArk = new Hashtable();
                uniSym = new Hashtable();
                uniFac = new Hashtable();
		opensymbol.unigrk = unigrk;
		opensymbol.uniGrk = uniGrk;
		opensymbol.uniArk = uniArk;
		opensymbol.uniSym = uniSym;
		opensymbol.uniFac = uniFac;

		jopensymbol.unigrk = unigrk;
		jopensymbol.uniGrk = uniGrk;
		jopensymbol.uniArk = uniArk;
		jopensymbol.uniSym = uniSym;
		jopensymbol.uniFac = uniFac;

                usg(0x3b1, 0xe0b7);
                usg(0x3b2, 0xe0b8);
                usg(0x3c8, 0xe0cd); 
                usg(0x3b4, 0xe0ba); 
                usg(0x3b5, 0xe0bb);
                usg(0x3d5, 0xe0cb); //phi
                usg(0x3b3, 0xe0b9); 
                usg(0x3b7, 0xe0bd); 
                usg(0x3b9, 0xe0bf); //iota
                usg(0x3c6, 0xe0d4); //varphi
                usg(0x3ba, 0xe0c0);
                usg(0x3bb, 0xe0c1);
                usg(0x3bc, 0xe0c2);
                usg(0x3bd, 0xe0c3);
                usg(0x3bf, 0xe0c5);
                usg(0x3c0, 0xe0c6);
                usg(0x3c7, 0xe0cc); 
                usg(0x3c1, 0xe0c7);
                usg(0x3c3, 0xe0c8);
                usg(0x3c4, 0xe0c9);
                usg(0x3c5, 0xe0ca);
                usg(0x3c9, 0xe0ce); 
                usg(0x3be, 0xe0c4);
                usg(0x3b8, 0xe0be); 
                //usg(0x3b6, 0xe0d3);
		usg(0x3b6, 0xe0bc);

                usA(0x3b1, 'A');
                usA(0x3b2, 'B');
                usG(0x3c8, 0xe0b5); //psi
                usG(0x3b4, 0xe0ad); //delta
                usA(0x3b5, 'E'); 
		usG(0x3d5, 0xe0b4); //phi
                usG(0x3b3, 0xe0ac); //gamma
                usA(0x3b7, 'H');
                usA(0x3b9, 'I');
                usG(0x3c6, 0xe0b4); //varphi
                usA(0x3ba, 'K'); 
                usG(0x3bb, 0xe0af); //lambda
                usA(0x3bc, 'M');
                usA(0x3bd, 'N');
                usA(0x3bf, 'O');
                usG(0x3c0, 0xe0b1); //pi
                usA(0x3c7,  'X');
                usA(0x3c1, 'P');
                usG(0x3c3, 0xe0b2); //sigma
                usA(0x3c4, 'T');
                usG(0x3c5, 0xe0b3); //upsilon
                usG(0x3c9, 0xe0b6); //omega
                usG(0x3be, 0xe0b0); //xi
                usG(0x3b8, 0xe0ae); //theta
                usA(0x3b6, 'Z'); 
            
        

                usp(0x222b, 0x222b);
                usp(0x2211, 0x2211);
                usp(0x220f, 0x220f);
                usp(0x22c5, 0x22c5);
                usp(0x00b1, 0x00b1);
                usp(0x2208, 0x2208);
                usp(0x220b, 0x220b);
                usp(0x2209, 0x2209);
                usp(0x2264, 0x2264);
                usp(0x2265, 0x2265);
                usp(0x2260, 0x2260);
                usp(0x2282, 0x2282);
                usp(0x2286, 0x2286);
                usp(0x2283, 0x2283);
                usp(0x2287, 0x2287);
                usp(0x2229, 0x2229);
                usp(0x222a, 0x222a);
                usp(0x2192, 0x2192);
                usp(0x2190, 0x2190);
                usp(0x2207, 0x2207);
                usp(0x2202, 0x2202);
                usp(0x221e, 0x221e);
                usp(0x2205, 0x2205);
                usp(0x211c, 0x211c);
                usp(0x2026, 0x2026);
                usp(0x221d, 0x221d);
                usp(0x2261, 0x2261);
                usp(0x2248, 0x2248);
                usp(0x00b7, 0x2022);
		usp(0x00d7, 0x00d7);
		usp(0x2112, 0x2112); //upper script L
		usp(0x2113, 0x2113); //lower script L
		usp(0xe090, 0xe090); //norm

	    jbinding = euclid;
	    editFace = "Euclid";
	    printFace = "Euclid";
                        
        }

	public int unichar;
	public int aschar;
	public int style;
	public int binary = 1;
	public String face;
	public float aftr;
	public float rescale = 1.0f;
        public String texString;
	public String MMLString;
	public double bpad = 0;
        private double lpad = 0;
        public double rpad = 0;
        public double hpad = 0;
	public double vpad = 0;
	public float fixangle = 0;
	private Jexbinding d;

	public void setSPad(){
        	//lpad = d.slpad;
		bpad = d.slpad;
        	rpad = d.srpad;
        	hpad = d.shpad;
		vpad = d.svpad;
		fixangle = d.sfixangle;
		rescale = d.srescale;
	}

	public void setBinary(int b) {
		binary = b;
	}
	public double getLpad(){
		double x = lpad;
		if(binary == 1) x = x + bpad;
		return x;
	}
        
	public Jexfont(int unichar){
		this.unichar = unichar;
		aschar = unichar;
		texString = "";
		char y = (char) aschar;
		Character z = new Character(y);
                if(unichar >= 32 & unichar <= 126) {
			texString = z.toString();
			MMLString = "<mi>" + z.toString() + "</mi>";
		}
		else MMLString = "<mi>" + "&#" + unichar + ";</mi>";
                if(aschar == '{') texString = "\\{";
                if(aschar == '}') texString = "\\}";
                if(aschar == '^') texString = "\\^";
                if(aschar == '_') texString = "\\_";
                if(aschar == '~') texString = "\\~";
                if(aschar == '&') texString = "\\&";
                if(aschar == '\\') texString = "\\backslash";
                if(aschar == '%') texString = "\\%";
                if(aschar == '$') texString = "\\$";
                if(aschar == '#') texString = "\\#";
		if(aschar == '<') MMLString = "<mo>&lt;</mo>";
		if(aschar == '>') MMLString = "<mo>&gt;</mo>";		
		setBinding("");
	}

	public void setBinding(String binding){
		if(binding.equals(""))      d = Jexfont.jbinding;
            	if(binding.equals("esstix"))d = esstix;
            	if(binding.equals("symbol"))d = symbol;
	    	if(binding.equals("euclid"))d = euclid;
	    	if(binding.equals("ooeuclid"))d = ooeuclid;
		if(binding.equals("opensymbol"))d = opensymbol;
		if(binding.equals("jopensymbol"))d = jopensymbol;
		aschar = unichar;
                face = d.dface;
                style = Font.PLAIN;
                aftr = 0;
		if((aschar == '-') | (aschar == '+') | (aschar == '/')) bpad = d.binpad;
                if(!JexStyle("math") & (aschar < 128)) {
			if((aschar == '=') | (aschar == '<') | (aschar == '>')) setSPad();		
		}
                else {
                    Integer u = new Integer(unichar);
                    Integer a = (Integer) d.unigrk.get(u);
                    if(a != null){
			if(greekItalic) style = Font.ITALIC;
                        aschar = a.intValue();
                        face = d.gface;
			vpad = d.gvpad;
			fixangle = d.gfixangle;
			rescale = d.grescale;
                    }
                    else{
                        int v = u.intValue() + 0x3b1 - 0x391;
                        a = (Integer) d.uniGrk.get(new Integer(v));
                        if(a != null){
                            aschar = a.intValue();
                            face = d.Gface;
			    vpad = d.Gvpad;
			    fixangle = d.Gfixangle;
			    rescale = d.Grescale;
                        }
                        else{
                            a = (Integer) d.uniArk.get(new Integer(v));
                            if(a != null){
                                aschar = a.intValue();
                                char y = (char) aschar;
                                Character z = new Character(y);
                                texString = z.toString();
                                this.unichar = a.intValue();
                                face = d.dface;
                            }
                            else {
                                a = (Integer) d.uniSym.get(u);
                                if(a != null) {
                                    Integer b = (Integer) d.uniFac.get(u);
                                    if(b == null) face = d.sface[0];
                                    else face = d.sface[b.intValue()];
                                    aschar = a.intValue();
				    setSPad();
                                }
                            }
                        }
                    }
                }
	}
            
        public boolean JexStyle(String nstyle){
            if((unichar >= 'a' & unichar <= 'z') | (unichar >= 'A' & unichar <= 'Z') | (unichar == 32)); 
	    else return false;

            char y = (char) aschar;
            Character z = new Character(y);
            face = d.dface;
            if(nstyle.equals("math")){
                style = Font.ITALIC;
		if(!mathItalic) style = Font.PLAIN;
                //aftr = (float) 0.2;
		aftr = d.aftr;
                texString = z.toString();
            }
            if(nstyle.equals("mathrm")){
                style = Font.PLAIN;
                aftr = 0;
                texString = "\\mathrm{" + z.toString() + "}";
		MMLString = "<mo>" + z.toString() + "</mo>";
            }
            return true;    
        }
        
        public String texString(){
            String z = texString;
            if(z.equals("")) z = Jextex.texMacro("\\unicode{" + Integer.toHexString(unichar) + "}");
            return z;
        }

	public String MMLString(){
		String z = MMLString;
		return z;
	}
        
        public String styleName(){
            if (style == Font.ITALIC) return "Italic";
            if (style == Font.BOLD) return "Bold";
            return "Plain";
            //bold italic??
        }

	public float getCenter(){
		if(unichar == '~') return 0.8f;
		if(unichar == '^') return 0.7f;
		if(unichar == '.') return -0.6f;
		return 1.0f;
	}

	public float getPrintWidth(){
		if(unichar == '-') return 1.62f; 
		return 1.0f;
	}

	public float getStretchScale(){
                if((unichar == 0x2192) || (unichar == 0x2190)) return 0.58f; //arrows
		//if(unichar == 126) return 1.2f; //tilda
		return 1.0f;
	}
}

// * This code is licensed under:
// * jeHEP License, Version 1.0
// * - for license details see http://jehep.sourceforge.net/license.html 
// *
// * Copyright (c) 2005 by S.Chekanov (chekanov@mail.desy.de). 
// * All rights reserved.
package jehep.ui;

import javax.swing.*;

import java.awt.Color;
import java.awt.Font;
import java.io.*;
import java.util.*;
import java.util.zip.ZipFile;
import java.lang.*;

   public class  SetEnv {
     

// localisation
     public static Locale   locale;
     public static String   LANGU;
     public static String   OSsys;
     public static String   DirPath;
     public static final String  fSep = System.getProperty("file.separator"); 
     public static final String  lSep= System.getProperty("line.separator");
     public static String   curDir;
     public static String   DicDir,ProjDir;

     public static boolean FirstTime=false;

// file with the settings 
    public static String INIFILE="";
    public static String INIRUN="";
    public static List<String>  RunComm;
    public static String RunCommText;


    public static String LookAndFeel; 
    public static int SelectedDic;    
    public static String SelectedDicName;
    public static JLabel MessageBar;
    public static Color FontColor;
    public static Font FontBold;
    public static Font FontPlain;
    public static String  INIDIR;
    public static String ClassPath;

//    public static URL[] cjars;


    public static void init() { 


      String OS = System.getProperty("os.name").toLowerCase();
// determine directories 
      RunComm=new ArrayList<String>();
      curDir = System.getProperty("user.dir");

      DirPath = System.getProperty("jehep.home");
      ClassPath= System.getProperty("java.class.path");
     LookAndFeel="javax.swing.plaf.metal.MetalLookAndFeel"; 
     LANGU="en";
     SelectedDic=0; 
     locale = new Locale(LANGU);
     Locale.setDefault(locale);
     MessageBar = new JLabel("jeHEP is initialized");

     
  INIDIR=System.getProperty("user.home")+File.separator+".jehep";
  OSsys="linux";  
  if (OS.indexOf("windows") > -1 || OS.indexOf("nt") > -1) {
     OSsys="windows";
     INIDIR=System.getProperty("user.home")+File.separator+"jehep";
    }  

     INIFILE=INIDIR+File.separator+"jehep.pref";
     INIRUN=INIDIR+File.separator+"jehep.run";

     FontColor=Color.black;
     FontBold= new Font("Lucida Sans", Font.BOLD, 14); 
     FontPlain= new Font("Lucida Sans", 0, 14);
     
     }
    


// read command
        public static String readFile(String file) {

            String tmp="";
            try {
                   FileReader inF = new FileReader(file);
                   BufferedReader br = new BufferedReader(inF);
                   String s;
                   while (( s=br.readLine())!= null) {
                   tmp=tmp+s+lSep;
                   }
                   inF.close();

            } catch (IOException e)
           {
               System.err.println ("Unable to read :"+file);
           }

          return tmp;

    }









   }
